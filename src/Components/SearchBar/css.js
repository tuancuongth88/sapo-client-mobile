import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({ 
    container: {},
    borderWrapper: {
        // width: width/1.1,
        height: 40,
        borderRadius: 5,
        borderWidth: 0.8,
        borderColor: 'lightgray',
        alignItems: 'center',
        backgroundColor: '#fff',
        flexDirection: 'row',
    },
    textInput: {
        // width: width/1.1 - 70,
    },
    clearInput: {
        position: 'absolute',
        // left: width/1.15 - 15,
        top: 8
    },
    iconSearch: {
        marginLeft: 10,
        marginRight: 5,
    }
})
