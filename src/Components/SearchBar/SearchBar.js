import React, { useState } from 'react';
import { View, TextInput, TouchableWithoutFeedback } from 'react-native';
import { styles } from './css'
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

export default function SearchBar(props) {

    const [searchText, setSearchText] = useState('');
    const [focus, setFocus] = useState(false);

    const onChangeText = (text) => {
        setSearchText(text)
        props.setKeyword(text)
    } 

    const clearInput = () => {
        setSearchText('')
        props.setKeyword('')
    }

    const handleFocus = () => {
        setFocus(true)
    }

    const handleBlur = () => {
        setFocus(false)
    }

    return (
        <View style={[styles.borderWrapper, {width: props.width, backgroundColor: props.backgroundColor}]}>
            <Feather
                name='search'
                size={20}
                color='gray'
                style={styles.iconSearch}
            />
            <TextInput 
                style={[styles.textInput, {width: props.width - 70}]}
                placeholder={props.placeholder}
                onChangeText={text => onChangeText(text)}
                value={searchText}
                underlineColorAndroid="transparent"
                onFocus={() => handleFocus()}
                onBlur={() => handleBlur()}
            />
            {searchText ? 
                <TouchableWithoutFeedback
                    onPress={() => clearInput()}
                >
                    <Icon
                        name='close-circle'
                        size={20}
                        color='gray'
                        style={[styles.clearInput, {left: props.left}]}
                    />
                </TouchableWithoutFeedback> : null   
            }
        </View>
        
    );
};

