import { StyleSheet } from 'react-native';

const SIZE = 65;

export const styles = StyleSheet.create({
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        width: SIZE,
        height: SIZE,
        borderRadius: SIZE / 2,
        backgroundColor: '#1C3FAA',
        opacity: 0.9,
    }
})