import React, { Component } from 'react';
import { View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { styles } from './css';

export default function MainButton(props) {

    return (
        <View
            underlayColor="#2882D8"
            style={styles.btn}                   
        >
            <Feather 
                name="youtube" 
                size={30} 
                color={props.focused ? 'tomato' : "#fff"}
            />
        </View> 
    );
};