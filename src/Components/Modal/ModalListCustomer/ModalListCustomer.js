import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, FlatList, TouchableWithoutFeedback, Modal } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import ModalCreateCustomer from '../ModalCreateCustomer/ModalCreateCustomer';
import SearchBar from '../../SearchBar/SearchBar';
import { width, height } from '../../Dimensions/Dimensions';

export default function ModalListCustomer(props) {
    const [refresh, setRefresh] = useState(false)
    const [modalCreateNewCustomerVisible, setModalCreateNewCustomerVisible] = useState(false);
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        
    }, []);

    const openModalCreateNewCustomer = () => {
        setModalCreateNewCustomerVisible(true)
    }

    const closeModalListProduct = () => {
        props.setModalListCustomerVisible(false)
    }

    const renderItem = ({item}) => {
        return (
            <View></View>
        )
    }

    const onRefresh = () => {
        
    }

    const data = [

    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalListCustomerVisible}
                onRequestClose={() => {
                    props.setModalListCustomerVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableWithoutFeedback onPress={() => closeModalListProduct()}>   
                            <View style={styles.headerLeft}>
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={styles.headerCenter}>
                            <SearchBar
                                placeholder='Tìm kiếm'
                                backgroundColor='#EAEAF7'
                                width={width/1.08 - 80}
                                left={width/1.08 - 110}
                                setKeyword={setKeyword}
                            />
                        </View>
                        <TouchableWithoutFeedback onPress={() => openModalCreateNewCustomer()}>   
                            <View style={styles.headerRight}>
                                <Feather
                                    name='plus'
                                    size={30}
                                    color='#404040'
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    {/* MODAL CREATE CUSTOMER */}
                        <ModalCreateCustomer
                            modalCreateNewCustomerVisible={modalCreateNewCustomerVisible}
                            setModalCreateNewCustomerVisible={setModalCreateNewCustomerVisible}
                        />
                    {/* MODAL CREATE CUSTOMER */}
                        
                    <View style={styles.body}>
                        <FlatList
                            style={styles.listCustomer}
                            refreshing={refresh}
                            onRefresh={onRefresh}
                            data={data}
                            renderItem={renderItem}
                            keyExtractor={(item, index) => index.toString()}
                            showsVerticalScrollIndicator ={false}
                        />
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    );
};