import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Image, TouchableWithoutFeedback, Modal, TouchableOpacity, Switch, FlatList } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import ModalCreateProductFast from '../ModalCreateProductFast/ModalCreateProductFast';
import SearchBar from '../../SearchBar/SearchBar';
import { width, height } from '../../Dimensions/Dimensions';

export default function ModalListProduct(props) {

    const [refresh, setRefresh] = useState(false)
    const [modalFilterListProductVisible, setModalFilterListProductVisible] = useState(false);
    const [modalCreateNewProductFastVisible, setModalCreateNewProductFastVisible] = useState(false)
    const [keyword, setKeyword] = useState('');
    const [manyChooseActive, setManyChooseActive] = useState(false);
    const toggleSwitch = () => setManyChooseActive(previousState => !previousState);

    useEffect(() => {
        
    }, []);

    const closeModalListProduct = () => {
        props.setModalListProductVisible(false)
    }

    const openModalCreateNewProductFast = () => {
        setModalCreateNewProductFastVisible(true)
    }

    const openModalFilterListProduct = () => {
        setModalFilterListProductVisible(true)
    }

    const renderItem = ({item}) => {
        return(
            <TouchableOpacity 
                style={styles.element}
                onPress={() => count()}
            >   
                <View style={styles.image}>
                    <Image
                        style={styles.productImage}
                        source={require('../../../Assets/no_image.png')}
                    />
                </View>
                <View style={styles.productInfo}>
                    <Text 
                        style={styles.productName}
                        numberOfLines={2}
                    >
                        {item.name}
                    </Text>
                    <Text style={styles.productCode}>{item.code}</Text>
                </View>
                <View style={styles.amountGroup}>
                    <Text style={styles.inventoryAmount}>{item.inventoryAmount}</Text>
                </View>
            </TouchableOpacity>
        )  
    }

    const count = () => {

    }

    const onRefresh = () => {
        
    }

    const chooseAgain = () => {

    }

    const finish = () => {

    }

    const data = [
        {name: 'Quần sks - 34 - xanh đen', code: 'SKU: PVN15', inventoryAmount: 0},
        {name: 'Quần sks - 34 - xanh', code: 'SKU: PVN14', inventoryAmount: 0},
        {name: 'Quần sks - 34 - đen', code: 'SKU: PVN13', inventoryAmount: 0},
        {name: 'Quần sks - 33 - xanh đen', code: 'SKU: PVN12', inventoryAmount: 0},
        {name: 'Quần sks - 33 - xanh', code: 'SKU: PVN11', inventoryAmount: 0},
        {name: 'Quần sks - 33 - đen', code: 'SKU: PVN10', inventoryAmount: 0},
        {name: 'Quần sks - 32 - xanh đen', code: 'SKU: PVN09', inventoryAmount: 0},
        {name: 'Quần sks - 32 - xanh', code: 'SKU: PVN08', inventoryAmount: 0},
        {name: 'Quần sks - 32 - đen', code: 'SKU: PVN07', inventoryAmount: 0},
    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalListProductVisible}
                onRequestClose={() => {
                    props.setModalListProductVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableWithoutFeedback onPress={() => closeModalListProduct()}>   
                            <View style={styles.headerLeft}>
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={styles.headerCenter}>
                            <SearchBar
                                placeholder='Tìm kiếm'
                                backgroundColor='#EAEAF7'
                                width={width/1.08 - 80}
                                left={width/1.08 - 110}
                                setKeyword={setKeyword}
                            />
                        </View>
                        <TouchableWithoutFeedback onPress={() => openModalCreateNewProductFast()}>   
                            <View style={styles.headerRight}>
                                <Feather
                                    name='plus'
                                    size={30}
                                    color='#404040'
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    {/* MODAL CREATE PRODUCT FAST */}
                        <ModalCreateProductFast
                            modalCreateNewProductFastVisible={modalCreateNewProductFastVisible}
                            setModalCreateNewProductFastVisible={setModalCreateNewProductFastVisible}
                        />
                    {/* MODAL CREATE PRODUCT FAST */}
                    
                    <View style={styles.body}>
                        <View style={styles.filterGroup}>
                            <View style={styles.filterGroupInside}>
                                <View style={styles.filterGroupLeft}>
                                    <TouchableOpacity
                                        style={styles.openFilterListWrapper}
                                        onPress={() => openModalFilterListProduct()}
                                    >
                                        <Text style={styles.openFilterList}>Tất cả sản phẩm</Text>
                                        <Feather
                                            name='chevrons-down'
                                            size={20}
                                            style={styles.iconDown}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.filterGroupRight}>
                                    <Text style={styles.chooseMany}>Chọn nhiều</Text>
                                    <Switch
                                        onValueChange={toggleSwitch}
                                        value={manyChooseActive}
                                        style={styles.switch}
                                    />
                                </View>
                            </View>
                        </View>

                        <FlatList
                            contentContainerStyle={[styles.productList, manyChooseActive ? {paddingBottom: 20} : {paddingBottom: 70}]}
                            // ref={(ref) => { this.flatListRef = ref; }}
                            refreshing={refresh}
                            onRefresh={onRefresh}
                            data={data}
                            renderItem={renderItem}
                            keyExtractor={(item, index) => index.toString()}
                            showsVerticalScrollIndicator ={false}
                        />

                        {manyChooseActive ? 
                            <View style={styles.bottom}>
                                <TouchableOpacity 
                                    style={styles.chooseAgainBtn}
                                    onPress={() => chooseAgain()}
                                >
                                    <Text style={styles.chooseAgainTitle}>Chọn lại</Text>
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={styles.finishBtn}
                                    onPress={() => finish()}
                                >
                                    <Text style={styles.finishTitle}>Xong</Text>
                                </TouchableOpacity>
                            </View> : null
                        }
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    );
};