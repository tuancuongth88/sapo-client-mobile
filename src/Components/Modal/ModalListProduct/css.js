import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 10,
    },
    headerCenter: {
        width: width/1.05 - 80,
        alignItems: 'center',
    },
    headerLeft: {
        width: 40,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        width: 40,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center',
    },
    filterGroup: {
        backgroundColor: '#EAEAF7',
        width: width,
        alignItems: 'center'
    },
    filterGroupInside: {
        width: width/1.08,
        flexDirection: 'row',
    },
    filterGroupLeft: {
        width: (width/1.08)/2,
        marginVertical: 10
    },
    filterGroupRight: {
        width: (width/1.08)/2,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        justifyContent: 'flex-end'
    },
    iconDown: {
        marginLeft: 3
    },
    openFilterListWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    switch: {
        marginLeft: 5
    },
    productList: {
        width: width,
    },
    element: {
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    productImage: {
        width: 70,
        height: 70,
        borderRadius: 5,
    },
    productInfo: {
        width: width - 150,
        paddingLeft: 15,
        paddingRight: 10
    },
    amountGroup: {
        width: 50,
        alignItems: 'flex-end',
    },
    inventoryAmount: {
        fontWeight: 'bold',
        fontSize: 15
    },
    productName: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    productCode: {
        marginTop: 5,
        color: 'gray',
        fontSize: 13
    },
    bottom: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 3,
        paddingBottom: 80,
    },
    chooseAgainBtn: {
        width: width/2.2,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'blue',
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        borderLeftWidth: 0.5
    },
    finishBtn: {
        width: width/2.2,
        height: 51,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9999FF',
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5
    },
    chooseAgainTitle: {
        fontSize: 18
    },
    finishTitle: {
        fontSize: 18,
        color: '#fff'
    },
    switch: {
        transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }]
    }
})