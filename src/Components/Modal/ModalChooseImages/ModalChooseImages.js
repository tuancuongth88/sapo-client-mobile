import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableHighlight, TouchableWithoutFeedback, Alert } from 'react-native';
import { styles } from './css'
import ImagePicker from 'react-native-image-crop-picker';

export default function ModalChooseImages(props) {
    
    useEffect(() => {
       
    }, []);
    
    const closeModalChooseImages = () => {
        props.setModalChooseImagesVisible(false)
    }

    const takePhoto = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            // compressImageQuality: 0.3
        }).then(image => {
            let path = "~" + image.path.substring(image.path.indexOf("/react-native-image-crop-picker"));
            image.filename = path.split("/").pop();
            if(props.setAvatar) {
                props.setAvatar(image)
            }
            else {
                props.setImgArr([...props.imgArr, image])
                if(props.setImgChosen) {
                    props.setImgChosen(image)
                    props.setModalListProductImgVisible(false)
                    props.setModalOptionImgOfVersionVisible(false)
                }
            }
            props.setModalChooseImagesVisible(false)
        }).catch((err) => { console.log("openCamera catch" + err.toString()) });
    }

    const pickPhotoFromLibrary = () => {
        if(props.setImgChosen) {
            ImagePicker.openPicker({
                mediaType: 'photo'
            }).then(image => {
                if(!image.mime) {
                    Alert.alert('Error !!!', 'Ảnh vừa chọn không đúng định dạng. Vui lòng chọn ảnh khác !!!')
                }
                else {
                    let path = "~" + image.path.substring(image.path.indexOf("/react-native-image-crop-picker"));
                    image.filename = path.split("/").pop();
                    props.setImgArr([...props.imgArr, image])
                    props.setImgChosen(image)
                    props.setModalChooseImagesVisible(false)
                    props.setModalListProductImgVisible(false)
                    props.setModalOptionImgOfVersionVisible(false)
                }
            }).catch((err) => { console.log("openLibrary catch" + err.toString()) });
        }
        else if(props.setImgArr && !props.setImgChosen) {
            ImagePicker.openPicker({
                mediaType: 'photo',
                multiple: true,
                // compressImageQuality: 0.3
            }).then(images => {
                var checkType = true;
                images.map((value, key) => { 
                    if(!value.mime) {
                        checkType = false;
                    }
                });
                if(!checkType) {
                    Alert.alert('Error !!!', 'Ảnh vừa chọn không đúng định dạng. Vui lòng chọn ảnh khác !!!')
                } 
                else {
                    images.map((image, key) => { 
                        let path = "~" + image.path.substring(image.path.indexOf("/react-native-image-crop-picker"));
                        image.filename = path.split("/").pop();
                    })
                    props.setImgArr([...props.imgArr,...images])
                    props.setModalChooseImagesVisible(false)
                }8
            }).catch((err) => { console.log("openLibrary catch" + err.toString()) });
        }
        else if(props.setAvatar) {
            ImagePicker.openPicker({
                mediaType: 'photo'
            }).then(image => {
                if(!image.mime) {
                    Alert.alert('Error !!!', 'Ảnh vừa chọn không đúng định dạng. Vui lòng chọn ảnh khác !!!')
                }
                else {
                    let path = "~" + image.path.substring(image.path.indexOf("/react-native-image-crop-picker"));
                    image.filename = path.split("/").pop();
                    props.setAvatar(image)
                    props.setModalChooseImagesVisible(false)
                }
            }).catch((err) => { console.log("openLibrary catch" + err.toString()) });
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalChooseImagesVisible}
                onRequestClose={() => {
                    props.setModalChooseImagesVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalChooseImagesVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.topContent}>
                                    <View style={[styles.row, {paddingVertical: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10}]}>
                                        <Text style={styles.borderTitle}>Chọn nguồn ảnh</Text>
                                    </View>
                                    <TouchableHighlight
                                        onPress={() => takePhoto()}
                                        underlayColor='whitesmoke'
                                    >
                                        <View style={[styles.row, {paddingVertical: 15}]}>
                                            <Text style={styles.optionTitle}>Camera</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        onPress={() => pickPhotoFromLibrary()}
                                        underlayColor='whitesmoke'
                                        style={{borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}
                                    >
                                        <View style={[styles.row, {paddingVertical: 15, borderBottomLeftRadius: 10, borderBottomRightRadius: 10}]}>
                                            <Text style={styles.optionTitle}>Thư viện ảnh</Text>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                                <TouchableHighlight 
                                    onPress={() => closeModalChooseImages()}
                                    underlayColor='silver'
                                    style={{borderRadius: 10}}
                                >
                                    <View style={styles.bottomContent}>
                                        <Text style={styles.cancel}>Hủy</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    )
}