import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    modalContent: {
        width: width,
        paddingBottom: 10,
        alignItems: 'center'
    },
    topContent: {
        width: width/1.05,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginBottom: 10,
    },
    bottomContent: {
        width: width/1.05,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        paddingVertical: 15
    },
    row: {
        width: width/1.05,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        alignItems: 'center',
    },
    borderTitle: {
        color: 'gray',
    },
    optionTitle: {
        fontSize: 20,
        color: '#1C3FAA'
    },
    cancel: {
        fontSize: 20,
        color: '#1C3FAA',
        fontWeight: 'bold'
    }
})