import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Input from '../../Input/Input';
import Feather from 'react-native-vector-icons/Feather';
import ModalScrollList from '../ModalScrollList/ModalScrollList';
import { width, height } from '../../Dimensions/Dimensions';

export default function ModalPlaceOfReceipt(props) {

    const [locationList, setLocationList] = useState([]);
    const [subDistrictList, setSubDistrictList] = useState([]);
    const [modalLocationVisible, setModalLocationVisible] = useState(false);
    const [modalSubDistrictVisible, setModalSubDistrictVisible] = useState(false);

    useEffect(() => {
        
    }, []);

    const openModalChooseLocation = () => {
        setModalLocationVisible(true)
    }

    const openModalChooseSubDistrict = () => {
        setModalSubDistrictVisible(true)
    }

    const getValue = (name, value) => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalPlaceOfReceiptVisible}
                onRequestClose={() => {
                    props.setModalPlaceOfReceiptVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalPlaceOfReceiptVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.headerWrapper}>
                                    <Text style={styles.header}>Kho lấy hàng</Text>
                                </View>
                                
                                <View style={styles.body}>
                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Tên chi nhánh</Text>
                                        <Input
                                            placeholder='Tên chi nhánh'
                                            name='placeName'
                                            value='Chi nhánh mặc định'
                                            borderWidth={width/1.1}
                                            inputWidth={width/1.1 - 30}
                                            iconDeleteLeft={width/1.15 - 25}
                                            editable={true}
                                        />
                                    </View>

                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Số điện thoại</Text>
                                        <Input
                                            placeholder='Số điện thoại'
                                            name='phone'
                                            value='0981963960'
                                            borderWidth={width/1.1}
                                            inputWidth={width/1.1 - 30}
                                            iconDeleteLeft={width/1.15 - 25}
                                            editable={true}
                                            keyboardType='phone-pad'
                                        />
                                    </View>

                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Địa chỉ lấy hàng</Text>
                                        <Input
                                            placeholder='Địa chỉ lấy hàng (số nhà, ngõ ngách)'
                                            name='address'
                                            borderWidth={width/1.1}
                                            inputWidth={width/1.1 - 30}
                                            iconDeleteLeft={width/1.15 - 25}
                                            editable={true}
                                        />
                                    </View>

                                    <TouchableHighlight
                                        onPress={() => openModalChooseLocation()}
                                        underlayColor='lavender'
                                    >
                                        <View style={styles.row}>
                                            <View style={styles.left}>
                                                <Text style={styles.leftText}>Khu vực</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                            {/* MODAL LOCATION */}
                                                <ModalScrollList
                                                    modalScrollListVisible={modalLocationVisible}
                                                    setModalScrollListVisible={setModalLocationVisible}
                                                    list={locationList}
                                                    listTitle='Chọn khu vực'
                                                />
                                            {/* MODAL LOCATION */}
                                        </View>
                                    </TouchableHighlight>

                                    <TouchableHighlight
                                        onPress={() => openModalChooseSubDistrict()}
                                        underlayColor='lavender'
                                    >
                                        <View style={styles.row}>
                                            <View style={styles.left}>
                                                <Text style={styles.leftText}>Phường/Xã</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                            {/* MODAL SUB-DISTRICT */}
                                                <ModalScrollList
                                                    modalScrollListVisible={modalSubDistrictVisible}
                                                    setModalScrollListVisible={setModalSubDistrictVisible}
                                                    list={subDistrictList}
                                                    listTitle='Chọn phường/xã'
                                                />
                                            {/* MODAL SUB-DISTRICT */}
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};