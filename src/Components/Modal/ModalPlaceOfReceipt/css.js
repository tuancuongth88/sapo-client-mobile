import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    modalContent: {
        width: width/1.05,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
    },
    header: {
        fontSize: 20,
        marginVertical: 10,
        color: '#404040'
    },
    headerWrapper: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        width: width/1.05,
        alignItems: 'center'
    },
    input: {
        marginBottom: 10
    },
    body: {
        paddingTop: 20,
        paddingBottom: 30
    },
    inputTitle: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 3
    },
    row: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
    },
    left: {
        width: width/2.2,
        paddingLeft: 5,
    },
    right: {
        width: width/2.2,
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center'
    },
    leftText: {
        // fontSize: 16,
    },
})