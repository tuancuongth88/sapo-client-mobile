import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal } from 'react-native';
import { styles } from './css'

export default function ModalCODDefine(props) {

    useEffect(() => {
       
    }, []);

    const closeModalCODDefine = () => {
        props.setModalCODDefineVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalCODDefineVisible}
                onRequestClose={() => {
                    props.setModalCODDefineVisible(false);
                }}
            >
                <TouchableWithoutFeedback onPress={() => closeModalCODDefine()}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Thu hộ COD</Text>
                                </View>
                                <View style={styles.body}>
                                    <Text style={styles.define}>Là tiền thu hộ mà cửa hàng của bạn cần đối soát với bên đối tác vận chuyển</Text>
                                    <Text style={styles.note}>Bạn cần tạo và cập nhật trạng thái phiếu đối soát để thông tin đối soát chính xác</Text>
                                    <Text style={styles.title}>Trong đó</Text>
                                    <Text style={styles.index}>- Đang thu hộ: gồm các vận đơn đang giao hàng chưa thành công</Text>
                                    <Text style={styles.index}>- Chờ đối soát: gồm các vận đơn đã giao thành công nhưng chưa được đối soát</Text>
                                    <Text style={styles.index}>- Đã đối soát: gồm các vận đơn đã giao thành công và đã được đối soát</Text>
                                </View>
                                <View style={styles.bottom}>
                                    <TouchableWithoutFeedback onPress={() => closeModalCODDefine()}>
                                        <Text style={styles.understood}>Tôi đã hiểu!</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};