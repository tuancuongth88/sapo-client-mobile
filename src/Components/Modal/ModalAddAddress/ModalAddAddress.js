import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import ModalScrollList from '../ModalScrollList/ModalScrollList';
import { width, height } from '../../Dimensions/Dimensions';

export default function ModalAddAddress(props) {

    const [locationList, setLocationList] = useState([]);
    const [subDistrictList, setSubDistrictList] = useState([]);
    const [modalLocationVisible, setModalLocationVisible] = useState(false);
    const [modalSubDistrictVisible, setModalSubDistrictVisible] = useState(false);

    useEffect(() => {
        
    }, []);

    const closeModalAddAddress = () => {
        props.setModalAddAddressVisible(false)
    }

    const openModalChooseLocation = () => {
        setModalLocationVisible(true)
    }

    const openModalChooseSubDistrict = () => {
        setModalSubDistrictVisible(true)
    }

    const addAddress = () => {

    }

    const getValue = (name, value) => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalAddAddressVisible}
                onRequestClose={() => {
                    props.setModalAddAddressVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalAddAddress()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Thêm địa chỉ</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity
                                onPress={() => addAddress()}
                            >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
    
                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Tên khách hàng</Text>
                            <Input
                                placeholder='Tên khách hàng'
                                name='customerName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Số điện thoại</Text>
                            <Input
                                placeholder='Số điện thoại'
                                name='phone'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                keyboardType='phone-pad'
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Địa chỉ</Text>
                            <Input
                                placeholder='Địa chỉ'
                                name='address'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Khu vực</Text>
                            <TouchableWithoutFeedback onPress={() => openModalChooseLocation()}>
                                <View style={styles.picker}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftText}>Chọn khu vực</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={20}
                                            color='gray'
                                        />
                                    </View>
                                    {/* MODAL LOCATION */}
                                        <ModalScrollList
                                            modalScrollListVisible={modalLocationVisible}
                                            setModalScrollListVisible={setModalLocationVisible}
                                            list={locationList}
                                            listTitle='Chọn khu vực'
                                        />
                                    {/* MODAL LOCATION */}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                                
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Phường/Xã</Text>
                            <TouchableWithoutFeedback onPress={() => openModalChooseSubDistrict()}>
                                <View style={styles.picker}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftText}>Chọn phường/xã</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={20}
                                            color='gray'
                                        />
                                    </View>
                                    {/* MODAL SUB-DISTRICT */}
                                        <ModalScrollList
                                            modalScrollListVisible={modalSubDistrictVisible}
                                            setModalScrollListVisible={setModalSubDistrictVisible}
                                            list={subDistrictList}
                                            listTitle='Chọn phường/xã'
                                        />
                                    {/* MODAL SUB-DISTRICT */}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};