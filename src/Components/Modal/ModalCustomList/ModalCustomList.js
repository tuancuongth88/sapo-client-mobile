import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import { width, height } from '../../Dimensions/Dimensions';
import SearchBar from '../../SearchBar/SearchBar';

export default function ModalCustomList(props) {

    const [modalAddElementVisible, setModalAddElementVisible] = useState(false);
    const [switchSearch, setSwitchSearch] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [arr, setArr] = useState([]);

    useEffect(() => {
       
    }, []);

    const closeModalCustomList = () => {
        props.setModalCustomListVisible(false)
    }

    const swtichToSearch = () => {
        setSwitchSearch(true)
    }

    const switchToShowHeader = () => {
        setSwitchSearch(false)
    }

    const openModalAddElement = () => {
        setModalAddElementVisible(true)
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalCustomListVisible}
                onRequestClose={() => {
                    props.setModalCustomListVisible(false)
                }}
            >
                <View style={styles.container}>
                    {switchSearch ? 
                        <View style={styles.header}>  
                            <View style={styles.headerSearchLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => closeModalCustomList()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerSearchCenter}>
                                <SearchBar
                                    placeholder='Tìm kiếm'
                                    backgroundColor='#EAEAF7'
                                    width={width/1.08 - 80}
                                    left={width/1.08 - 110}
                                    setKeyword={setKeyword}
                                />
                            </View>
                            <View style={styles.headerSearchRight}>
                                <TouchableWithoutFeedback onPress={() => switchToShowHeader()}>
                                    <Text style={{fontSize: 16}}>Hủy</Text>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                            :
                        <View style={styles.header}>  
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback onPress={() => closeModalCustomList()}>
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>{props.headerTitle}</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <View style={styles.iconPlusWrapper}>
                                    <TouchableOpacity onPress={() => openModalAddElement()}>
                                        <Feather
                                            name='plus'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.iconSearchWrapper}>
                                    <TouchableWithoutFeedback onPress={() => swtichToSearch()}>
                                        <Feather
                                            name='search'
                                            size={25}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </View>
                    }
                    
                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        
                    </ScrollView>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

