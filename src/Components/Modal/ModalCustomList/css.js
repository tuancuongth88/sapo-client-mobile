import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        paddingRight: 10,
        width: 70,
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        alignItems: 'center',
        width: 70,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 140,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    iconPlusWrapper: {
        width: 35,
        alignItems: 'center'
    },
    iconSearchWrapper: {
        width: 35,
        alignItems: 'flex-end'
    },
    headerSearchLeft: {
        width: 40,
    },
    headerSearchCenter: {
        width: width/1.08 - 70
    },
    headerSearchRight: {
        width: 30,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchBar: {
        width: width/1.08 - 80,
        height: 40,
        borderRadius: 5,
        backgroundColor: '#EAEAF7',
        paddingLeft: 10
    },
})