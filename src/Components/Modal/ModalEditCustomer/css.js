import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

const radioSize = 20;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        width: width,
        alignItems: 'center',
        paddingTop: 20
    },
    input: {
        marginBottom: 10,
        width: width/1.1,
    },
    inputTitle: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 3
    },
    picker: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderColor: 'gray',
        borderWidth: 0.5
    },
    left: {
        width: width/1.1 - 40,
        paddingLeft: 10,
    },
    right: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    birthdayGroup: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    birthdayShow: {
        width: width/1.1 - 60,
        paddingLeft: 10,
        paddingVertical: 15,
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 10,
        backgroundColor: '#fff'
    },
    calendar: {
        width: 50,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    bottom: {
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20,
        width: width,
        borderColor: 'gray',
        borderTopWidth: 0.5,
    },
    saveBtn: {
        width: width/1.1,
        borderRadius: 10,
        backgroundColor: '#1C3FAA',
        alignItems: 'center',
        paddingVertical: 10
    },
    save: {
        color: '#fff',
        fontSize: 18
    },
    genderGroup: {
        width: width/1.1,
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 5
    },
    maleGroupWrapper: {
        width: 100,
    },
    maleGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5
    },
    femaleGroupWrapper: {
        width: width/1.1 - 100,
    },
    femaleGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5
    },
    radioBtn: {
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: radioSize,
        height: radioSize,
        borderRadius: radioSize,
        backgroundColor: '#fff'
    },
    radioChecked: {
        width: radioSize - 8,
        height: radioSize - 8,
        borderRadius: radioSize - 8
    },
    radioTitle: {
        marginLeft: 10,
        fontSize: 16
    }
})