import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import { width, height } from '../../Dimensions/Dimensions';
import ModalDateTimePicker from '../ModalDateTimePicker/ModalDateTimePicker';
import ModalScrollList from '../ModalScrollList/ModalScrollList';

export default function ModalEditCustomer(props) {

    const [datePickerVisible, setDatePickerVisible] = useState(false);
    const [birthday, setBirthday] = useState(new Date());
    const [birthdayShow, setBirthdayShow] = useState('');
    const [male, setMale] = useState(false);
    const [female, setFemale] = useState(false);
    const [customerGroupList, setCustomerGroupList] = useState([]);
    const [modalCustomerGroupVisible, setModalCustomerGroupVisible] = useState(false);

    useEffect(() => {
       
    }, []);

    const closeModalEditCustomer = () => {
        props.setModalEditCustomerVisible(false)
    }

    const openDatePicker = () => {
        setDatePickerVisible(true)
    }

    const openModalChooseCustomerGroup = () => {
        setModalCustomerGroupVisible(true)
    }

    const chooseMale = () => {
        setMale(true)
        setFemale(false)
    }

    const chooseFemale = () => {
        setMale(false)
        setFemale(true)
    }

    const save = () => {

    }
    
    const getValue = (name, value) => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalEditCustomerVisible}
                onRequestClose={() => {
                    props.setModalEditCustomerVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalEditCustomer()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Chỉnh sửa thông tin</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity
                                onPress={() => save()}
                            >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Tên khách hàng</Text>
                            <Input
                                placeholder='Tên khách hàng'
                                name='customerName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Số điện thoại</Text>
                            <Input
                                placeholder='Số điện thoại'
                                name='phone'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                                keyboardType='phone-pad'
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Email</Text>
                            <Input
                                placeholder='Email'
                                name='email'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Ngày sinh</Text>
                            <View style={styles.birthdayGroup}>
                                <View style={styles.birthdayShow}>
                                    <Text style={[styles.birthday, birthdayShow == '' ? {color: 'gray'} : {color: '#000'}]}>
                                        {birthdayShow == '' ? 'Ngày sinh' : birthdayShow}
                                    </Text>
                                </View>
                                <View style={styles.calendar}>
                                    <TouchableOpacity onPress={() => openDatePicker()}>
                                        <Feather
                                            name='calendar'
                                            size={35}
                                            color='gray'
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {datePickerVisible ? 
                                <ModalDateTimePicker
                                    datePickerVisible={datePickerVisible}
                                    setDatePickerVisible={setDatePickerVisible}
                                    birthday={birthday}
                                    setBirthday={setBirthday}
                                    birthdayShow={birthdayShow}
                                    setBirthdayShow={setBirthdayShow}
                                /> : null
                            }
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Mã khách hàng</Text>
                            <Input
                                placeholder='Mã khách hàng'
                                name='customerCode'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Nhóm khách hàng</Text>
                            <TouchableWithoutFeedback onPress={() => openModalChooseCustomerGroup()}>
                                <View style={styles.picker}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftText}>Chọn nhóm khách</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={20}
                                            color='gray'
                                        />
                                    </View>
                                    {/* MODAL CUSTOMER GROUP */}
                                        <ModalScrollList
                                            modalScrollListVisible={modalCustomerGroupVisible}
                                            setModalScrollListVisible={setModalCustomerGroupVisible}
                                            list={customerGroupList}
                                            listTitle='Nhóm khách hàng'
                                        />
                                    {/* MODAL CUSTOMER GROUP */}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>

                        <View style={[styles.input, {marginTop: 10, marginBottom: 50}]}>
                            <Text style={styles.inputTitle}>Giới tính</Text>
                            <View style={styles.genderGroup}>
                                <View style={styles.maleGroupWrapper}>
                                    <TouchableWithoutFeedback onPress={() => chooseMale()}>
                                        <View style={styles.maleGroup}>
                                            <View style={[styles.radioBtn, male ? {borderColor: '#1C3FAA'} : {borderColor: 'gray'}]}>
                                                <View style={[styles.radioChecked, male ? {backgroundColor: '#1C3FAA'} : null]}></View>
                                            </View>
                                        <Text style={styles.radioTitle}>Nam</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.femaleGroupWrapper}>
                                    <TouchableWithoutFeedback onPress={() => chooseFemale()}>
                                        <View style={styles.femaleGroup}>
                                            <View style={[styles.radioBtn, female ? {borderColor: '#1C3FAA'} : {borderColor: 'gray'}]}>
                                                <View style={[styles.radioChecked, female ? {backgroundColor: '#1C3FAA'} : null]}></View>
                                            </View>
                                            <Text style={styles.radioTitle}>Nữ</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </View>
                    </ScrollView>

                    <View style={styles.bottom}>
                        <TouchableOpacity
                            style={styles.saveBtn}
                            onPress={() => createNewCustomerByBtn()}
                        >
                            <Text style={styles.save}>Lưu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

