import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NumberFormat from 'react-number-format';
import ModalListProduct from '../ModalListProduct/ModalListProduct';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';
import ModalDeliveryCost from '../ModalDeliveryCost/ModalDeliveryCost';
import ModalListCustomer from '../ModalListCustomer/ModalListCustomer';
import ModalPriceOption from '../ModalPriceOption/ModalPriceOption';
import ModalPaymentMethod from '../ModalPaymentMethod/ModalPaymentMethod';
import ModalAddNote from '../ModalAddNote/ModalAddNote';
import ModalOrderMethod from '../ModalOrderMethod/ModalOrderMethod';

export default function ModalCreateOrder(props) {

    const [modalListProductVisible, setModalListProductVisible] = useState(false);
    const [modalNumberVisible, setModalNumberVisible] = useState(false);
    const [modalDeliveryVisible, setModalDeliveryVisible] = useState(false);
    const [modalListCustomerVisible, setModalListCustomerVisible] = useState(false);
    const [modalPriceOptionVisible, setModalPriceOptionVisible] = useState(false);
    const [modalPaymentMethodsVisible, setModalPaymentMethodsVisible] = useState(false);
    const [modalAddNoteVisible, setModalAddNoteVisible] = useState(false);
    const [modalOrderMethodsVisible, setModalOrderMethodsVisible] = useState(false);
    const [discount, setDiscount] = useState('0');
    const [discountMethod, setDiscountMethod] = useState('');
    const [deliveryCost, setDeliveryCost] = useState('0');
    const [wholesale, setWholesale] = useState(false);
    const [chosenProducts, setChosenProduct] = useState([]);
    const [note, setNote] = useState('');
    const [paymentMethod, setPaymentMethod] = useState('');
    const [orderOption, setOrderOption] = useState('');

    useEffect(() => {
        
    }, []);

    const closeModalCreateOrder = () => {
        props.setModalCreateNewOrderVisible(false)
    }

    const chooseProduct = () => {
        setModalListProductVisible(true)
    }

    const openModalNumber = () => {
        setModalNumberVisible(true)
    }

    const openModalDelivery = () => {
        setModalDeliveryVisible(true)
    }

    const openModalListCustomer = () => {
        setModalListCustomerVisible(true)
    }

    const openModalPriceOption = () => {
        setModalPriceOptionVisible(true)
    }

    const openModalPaymentMethods = () => {
        // if(chosenProducts.length == 0) {
        //     Alert.alert('Đã có lỗi xảy ra', 'Giá trị cần thanh toán phải lớn hơn 0 !!!')
        // }
        // else {
        //     setModalPaymentMethodsVisible(true)
        // }
        setModalPaymentMethodsVisible(true)
    }

    const openModalOrderMethods = () => {
        setModalOrderMethodsVisible(true)
    }

    const openModalAddNote = () => {
        setModalAddNoteVisible(true)
    }

    const deletePaymentMethod = () => {
        setPaymentMethod('')
    }

    const createNewOrderByIcon = () => {
        props.setModalCreateNewOrderVisible(false)
    }

    const createNewOrderByBtn = () => {
        
    }
 
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                 animationType="slide"
                transparent={true}
                visible={props.modalCreateNewOrderVisible}
                onRequestClose={() => {
                    props.setModalCreateNewOrderVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalCreateOrder()}>
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Tạo đơn hàng</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity onPress={() => createNewOrderByIcon()}>
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <TouchableOpacity 
                            style={styles.chooseProduct}
                            onPress={() => chooseProduct()}
                        >   
                            <Feather
                                name='plus-circle'
                                size={35}
                                color='#1C3FAA'
                                style={styles.addProduct}
                            />
                            <Text style={styles.noProduct}>Đơn hàng của bạn chưa có sản phẩm nào</Text>
                            {/* MODAL PRODUCT LIST */}
                                <ModalListProduct
                                    modalListProductVisible={modalListProductVisible}
                                    setModalListProductVisible={setModalListProductVisible}
                                    chosenProducts={chosenProducts}
                                    setChosenProduct={setChosenProduct}
                                />
                            {/* MODAL PRODUCT LIST */}
                        </TouchableOpacity>

                        <View style={styles.caculate}>
                            <View style={styles.row}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.title}>Tổng số lượng</Text>
                                </View>
                                <View style={styles.rowRight}>
                                    <Text style={styles.amount}>0</Text>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.title}>Tổng tiền hàng</Text>
                                </View>
                                <View style={styles.rowRight}>
                                    <Text style={styles.amount}>0</Text>
                                </View>
                            </View>
                            <TouchableOpacity 
                                style={styles.row}
                                onPress={() => openModalNumber()}
                            >
                                <View style={styles.rowLeft}>
                                    <Text style={styles.titleBlue}>Chiết khấu</Text>
                                </View>
                                <View style={styles.rowRight}>
                                    {discountMethod == 'đ' ? 
                                        <NumberFormat
                                            value={discount}
                                            thousandSeparator={','}
                                            prefix={discount != '0' ? 'đ ' : ''}
                                            displayType={'text'}
                                            renderText={value => <Text style={styles.amountBlue}>{value}</Text>}
                                        />
                                        :
                                        <Text style={styles.amountBlue}>{discount}{discount != '0' ? ' ' + discountMethod : null}</Text>
                                    }
                                </View>
                            </TouchableOpacity>
                            {/* MODAL DISCOUNT */}
                                <ModalEvenNumber
                                    modalTitle='Chiết khấu'
                                    modalEvenNumberVisible={modalNumberVisible}
                                    setModalEvenNumberVisible={setModalNumberVisible}
                                    evenNumber={discount}
                                    setEvenNumber={setDiscount}
                                    setDiscountMethod={setDiscountMethod}
                                />
                            {/* MODAL DISCOUNT */}
                            <TouchableOpacity 
                                style={styles.row}
                                onPress={() => openModalDelivery()}
                            >
                                <View style={styles.rowLeft}>
                                    <Text style={styles.titleBlue}>Phí giao hàng</Text>
                                </View>
                                <View style={styles.rowRight}>
                                    <NumberFormat
                                        value={Number(deliveryCost)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.amountBlue}>{value}</Text>}
                                    />  
                                </View>
                            </TouchableOpacity>
                            {/* MODAL DELIVERY COST */}
                                <ModalDeliveryCost
                                    modalDeliveryVisible={modalDeliveryVisible}
                                    setModalDeliveryVisible={setModalDeliveryVisible}
                                    deliveryCost={deliveryCost}
                                    setDeliveryCost={setDeliveryCost}
                                />
                            {/* MODAL DELIVERY COST */}
                        </View>

                        <TouchableOpacity 
                            style={styles.customerWrapper}
                            onPress={() => openModalListCustomer()}    
                        >
                            <View style={styles.customerInside}>
                                <Text style={styles.customerTitle}>Khách hàng</Text>
                                <View style={styles.addCustomer}>
                                    <Icon
                                        name='person-add'
                                        size={20}
                                        color='gray'
                                    />
                                    <Text style={styles.addCustomerTitle}>Thêm khách hàng</Text>
                                </View>
                                <View style={styles.line}></View>
                                {/* MODAL CUSTOMER */}
                                    <ModalListCustomer
                                        modalListCustomerVisible={modalListCustomerVisible}
                                        setModalListCustomerVisible={setModalListCustomerVisible}
                                    />
                                {/* MODAL CUSTOMER */}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.choosePrice}
                            onPress={() => openModalPriceOption()}
                        >
                            <View style={styles.choosePriceInside}>
                                <View style={styles.choosePriceInsideLeft}>
                                    <Icon
                                        name='pricetags'
                                        size={20}
                                        color='gray'
                                    />
                                    <Text style={styles.choosePriceTitle}>
                                        {wholesale ? 'Giá bán buôn' : 'Giá bán lẻ'}
                                    </Text>
                                </View>
                                <Feather
                                    name='chevron-right'
                                    size={20}
                                    color='gray'
                                />
                                {/* MODAL PRICE OPTION */}
                                    <ModalPriceOption
                                        modalPriceOptionVisible={modalPriceOptionVisible}
                                        setModalPriceOptionVisible={setModalPriceOptionVisible}
                                        wholesale={wholesale}
                                        setWholesale={setWholesale}
                                    />
                                {/* MODAL PRICE OPTION */}
                            </View>
                        </TouchableOpacity>

                        {paymentMethod == '' ?
                            <TouchableOpacity
                                style={styles.paymentMethods}
                                onPress={() => openModalPaymentMethods()}
                            >    
                                <View style={styles.paymentMethodsInside}>
                                    <View style={styles.paymentMethodsInsideLeft}>
                                        <Icon
                                            name='card'
                                            size={20}
                                            color='gray'
                                        />
                                        <Text style={styles.paymentMethodsTitle}>Chọn phương thức thanh toán</Text>
                                    </View>
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                            </TouchableOpacity> 
                                :
                            <View style={styles.paymentMethods}>
                                <View style={styles.paymentMethodsInside}>
                                    <View style={styles.paymentMethodsInsideLeft}>
                                        <Icon
                                            name='card'
                                            size={20}
                                            color='gray'
                                        />
                                        <Text style={styles.paymentMethodsTitle}>Thanh toán</Text>
                                    </View>
                                </View>
                                <TouchableHighlight
                                    onPress={() => openModalPaymentMethods()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.paymentDetail}>
                                        <View style={styles.paymentDetailLeft}>
                                            <Text style={styles.paymentMethodTitle}>{paymentMethod}</Text>
                                        </View>
                                        <View style={styles.paymentDetailRight}>
                                            <Text style={styles.paymentFee}>100,000</Text>
                                            <TouchableWithoutFeedback onPress={() => deletePaymentMethod()}>
                                                <Feather
                                                    name='trash-2'
                                                    size={20}
                                                    color='red'
                                                />
                                            </TouchableWithoutFeedback>
                                        </View>
                                    </View> 
                                </TouchableHighlight>
                            </View>   
                        }
                               
                        {/* MODAL PAYMENT METHOD */}
                            <ModalPaymentMethod
                                modalPaymentMethodsVisible={modalPaymentMethodsVisible}
                                setModalPaymentMethodsVisible={setModalPaymentMethodsVisible}
                                setPaymentMethod={setPaymentMethod}
                                payment='100,000'
                            />
                        {/* MODAL PAYMENT METHOD */}

                        <TouchableOpacity
                            style={styles.addNote}
                            onPress={() => openModalAddNote()}
                        >
                            <View style={styles.addNoteInside}>
                                <MaterialCommunityIcons
                                    name='pencil-outline'
                                    size={20}
                                    color='blue'
                                />
                                <Text style={styles.addNoteTitle}>Thêm ghi chú</Text>
                                {/* MODAL ADD NOTE */}
                                    <ModalAddNote
                                        modalAddNoteVisible={modalAddNoteVisible}
                                        setModalAddNoteVisible={setModalAddNoteVisible}
                                        note={note}
                                        setNote={setNote}
                                    />
                                {/* MODAL ADD NOTE */}
                            </View>
                        </TouchableOpacity>
                    </ScrollView>

                    <View style={styles.createBtnWrapper}>
                        <View style={styles.provisional}>
                            <View style={styles.provisionalTitleWrapper}>
                                <Text style={styles.provisionalTitle}>Tạm tính</Text>
                            </View>
                            <View style={styles.totalWrapper}>
                                <Text style={styles.total}>0</Text>
                            </View>
                        </View> 
                        <View style={styles.createOrderBtnGroup}>
                            <TouchableOpacity
                                style={styles.createOrderBtn}
                                onPress={() => createNewOrderByBtn()}
                            >
                                <Text style={styles.createOrderBtnTitle}>
                                    {orderOption == '' ? 'Tạo đơn và giao hàng' : orderOption} 
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.optionCreateBtn}
                                onPress={() => openModalOrderMethods()}
                            >
                                <Feather
                                    name='more-horizontal'
                                    size={25}
                                    color='gray'
                                />
                            </TouchableOpacity>
                            {/* MODAL ORDER METHOD */}
                                <ModalOrderMethod
                                    modalOrderMethodsVisible={modalOrderMethodsVisible}
                                    setModalOrderMethodsVisible={setModalOrderMethodsVisible}
                                    setOrderOption={setOrderOption}
                                />
                            {/* MODAL ORDER METHOD */}
                        </View>
                    </View>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

