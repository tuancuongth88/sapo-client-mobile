import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
    },
    headerRight: {
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    chooseProduct: {
        marginBottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        width: width,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
    },
    addProduct: {
        marginTop: 20,
        marginBottom: 5
    },
    noProduct: {
        marginBottom: 20,
        color: 'gray'
    },
    caculate: {
        borderBottomColor: 'gray',
        borderTopColor: 'gray',
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        paddingTop: 10,
        backgroundColor: '#fff',
        marginBottom: 15
    },
    row: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rowLeft: {
        width: 120,
        justifyContent: 'center'
    },
    rowRight: {
        width: width/1.1 - 120,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    title: {
        fontSize: 15
    },
    amount: {
        fontSize: 15
    },
    titleBlue: {
        color: 'blue',
        fontSize: 15
    },
    amountBlue: {
        color: 'blue',
        fontSize: 15
    },
    customerWrapper: {
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    customerInside: {
        width: width/1.1,
    },
    customerTitle: {
        fontSize: 15,
        marginVertical: 10,
    },
    addCustomer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    addCustomerTitle: {
        marginLeft: 15,
        fontSize: 15,
        color:'blue'
    },
    line: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
    },
    choosePrice: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginBottom: 15
    },
    choosePriceInside: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    choosePriceTitle: {
        marginLeft: 15,
        fontSize: 15,
        color:'blue'
    },
    choosePriceInsideLeft: {
        flexDirection: 'row',
        width: width/1.1 - 15,
        alignItems: 'center',
    },
    paymentMethods: {
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginBottom: 15
    },
    paymentMethodsInside: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    paymentMethodsInsideLeft: {
        flexDirection: 'row',
        width: width/1.1 - 15,
        alignItems: 'center',
    },
    paymentMethodsTitle: {
        fontSize: 15,
        marginLeft: 15
    },
    addNote: {
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginBottom: 30
    },
    addNoteInside: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    addNoteTitle: {
        fontSize: 15,
        marginLeft: 15,
        color: 'blue',
    },
    createBtnWrapper: {
        width: width,
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    provisional: {
        width: width/1.1,
        marginVertical: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    provisionalTitleWrapper: {
        width: 100
    },
    totalWrapper: {
        width: width/1.1 - 100,
        alignItems: 'flex-end'
    },
    provisionalTitle: {
        fontSize: 15
    },
    total: {
        fontSize: 15
    },
    createOrderBtnGroup: {
        width: width/1.1,
        marginBottom: 10,
        flexDirection: 'row'
    },
    createOrderBtn: {
        borderRadius: 10,
        backgroundColor: '#1C3FAA',
        justifyContent: 'center',
        alignItems: 'center',
        width: width/1.1 - 60,
        height: 50
    },
    createOrderBtnTitle: {
        fontSize: 17,
        color: '#fff'
    },
    optionCreateBtn: {
        width: 50,
        borderRadius: 10,
        backgroundColor: 'whitesmoke',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        height: 50
    },
    paymentDetail: {
        width: width/1.1,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderTopColor: 'gray',
        borderTopWidth: 0.5
    },
    paymentDetailLeft: {
        width: width/2.2
    },
    paymentDetailRight: {
        width: width/2.2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    paymentFee: {
        marginRight: 10,
        fontSize: 15
    },
    paymentMethodTitle: {
        fontSize: 15
    }
})