import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    modalContent: {
        width: width/1.3,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        height: height/1.7
    },
    header: {
        fontSize: 20,
        marginVertical: 10,
        color: '#404040'
    },
    headerWrapper: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        width: width/1.3,
        alignItems: 'center'
    },
    textInput: {
        width: width/1.3,
        paddingLeft: 10,
        backgroundColor: 'lavender'
    },
    searchBar: {
        marginBottom: 10
    },
    itemRow: {
        width: width/1.3,
        padding: 10,
    },
    itemTitle: {
        fontSize: 15
    },
})