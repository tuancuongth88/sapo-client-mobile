import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TextInput, Modal, FlatList, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import { styles } from './css';
import Feather from 'react-native-vector-icons/Feather';

export default function ModalScrollList(props) {

    const [searchText, setSearchText] = useState('');
    const [refresh, setRefresh] = useState(false)

    useEffect(() => {
        
    }, []);

    const onChangeText = (text) => {
        setSearchText(text)
    }

    const renderItem = ({item}) => {
        return (
            <TouchableHighlight
                onPress={() => chooseItem(item)}
                underlayColor='lavender'
            >
                <View style={styles.itemRow}>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                </View>
            </TouchableHighlight>
        )
    }

    const chooseItem = (item) => {
        props.setChooseItem(item)
        props.setModalScrollListVisible(false)
    }

    const onRefresh = () => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalScrollListVisible}
                onRequestClose={() => {
                    props.setModalScrollListVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalScrollListVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.headerWrapper}>
                                    <Text style={styles.header}>{props.listTitle}</Text>
                                </View>
                                    
                                <View style={styles.body}>
                                    <View style={styles.searchBar}>
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder='Tìm kiếm'
                                            onChangeText={text => onChangeText(text)}
                                            value={searchText}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>

                                    <FlatList
                                        contentContainerStyle={styles.scrollList}
                                        // ref={(ref) => { this.flatListRef = ref; }}
                                        refreshing={refresh}
                                        onRefresh={onRefresh}
                                        data={props.list}
                                        renderItem={renderItem}
                                        keyExtractor={(item, index) => index.toString()}
                                        showsVerticalScrollIndicator ={false}
                                    />
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};