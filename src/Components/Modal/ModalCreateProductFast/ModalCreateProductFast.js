import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import { width, height } from '../../Dimensions/Dimensions';
import Input from '../../Input/Input';

export default function ModalCreateProductFast(props) {

    useEffect(() => {
        
    }, []);

    const closeModal = () => {
        props.setModalCreateNewProductFastVisible(false)
    }

    const createNewProductFast = () => {
        props.setModalCreateNewProductFastVisible(false)
    }

    const getValue = (name, value) => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalCreateNewProductFastVisible}
                onRequestClose={() => {}}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModal()}>
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Thêm nhanh sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity onPress={() => createNewProductFast()} >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <ScrollView
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.input}>
                            <Input
                                placeholder='Tên sản phẩm'
                                name='productName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>
                        <View style={styles.input}>
                            <Input
                                placeholder='Mã sản phẩm'
                                name='productCode'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>
                        <View style={styles.input}>
                            <Input
                                placeholder='Tồn kho'
                                name='inventoryAmount'
                                keyboardType='numeric'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>
                        <View style={styles.row}>
                            <View style={styles.left}>
                                <Input
                                    placeholder='Giá bán lẻ (vnđ)'
                                    name='retailPrice'
                                    keyboardType='numeric'
                                    borderWidth={width/2.2 - 5}
                                    inputWidth={width/2.2 - 30}
                                    iconDeleteLeft={width/2.3 - 25}
                                    editable={true}
                                />
                            </View>
                            <View style={styles.right}>
                                <Input
                                    placeholder='Giá vốn (vnđ)'
                                    name='costPrice'
                                    keyboardType='numeric'
                                    borderWidth={width/2.2 - 5}
                                    inputWidth={width/2.2 - 30}
                                    iconDeleteLeft={width/2.3 - 25}
                                    editable={true}
                                />
                            </View>
                        </View>
                        <View style={styles.input}>
                            <Input
                                placeholder='Khối lượng (kg/dùng để tính phí vận chuyển)'
                                name='weight'
                                keyboardType='numeric'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.note}>
                            <Text style={styles.noteTitle}>Lưu ý: <Text style={styles.noteContent}>Đây là màn hình hỗ trợ tạo nhanh sản phẩm nên chỉ hiển thị các thông tin cơ bản.
                                                Để quản lý nhiều thông tin hơn vui lòng tạo tại menu sản phẩm !!!</Text></Text>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};