import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center'
    },
    modalContent: {
        width: width/1.5,
        borderRadius: 10,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    header: {
        width: width/1.5,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
    },
    headerTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        width: width/1.5 - 10,
    },
    bottom: {
        paddingVertical: 10
    },
    define: {
        textAlign: 'center',
        fontSize: 16,
        marginBottom: 10,
        lineHeight: 21
    },
    understood: {
        color: 'blue',
        fontSize: 16
    }
})