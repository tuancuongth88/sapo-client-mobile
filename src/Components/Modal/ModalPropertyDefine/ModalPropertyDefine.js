import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal } from 'react-native';
import { styles } from './css'

export default function ModalPropertyDefine(props) {

    useEffect(() => {
       
    }, []);

    const closeModalPropertyDefine = () => {
        props.setModalPropertyDefineVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalPropertyDefineVisible}
                onRequestClose={() => {
                    props.setModalPropertyDefineVisible(false);
                }}
            >
                <TouchableWithoutFeedback onPress={() => closeModalPropertyDefine()}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Thuộc tính</Text>
                                </View>
                                <View style={styles.body}>
                                    <Text style={styles.define}>Thêm thuộc tính để tạo ra sản phẩm khi nhiều phiên bản với các sự lựa chọn như màu sắc, kích thước, chất liệu,...</Text>
                                </View>
                                <View style={styles.bottom}>
                                    <TouchableWithoutFeedback onPress={() => closeModalPropertyDefine()}>
                                        <Text style={styles.understood}>Tôi đã hiểu!</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};