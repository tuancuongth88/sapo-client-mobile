import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

const SIZE = 15;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'flex-end'
    },
    modalContent: {
        width: width,
        borderRadius: 10,
        backgroundColor: '#fff'
    },
    header: {
        
    },
    headerTitle: {
        
    },
    body: {
        alignItems: 'center',
        width: width
    },
    row: {
        flexDirection: 'row',
        width: width/1.06,
    },
    rowElement: {
        width: (width/1.06)/5,
        paddingVertical: 5,
    },
    orderOptionGroup: {
        width: width,
        borderColor: 'gray',
        marginBottom: 15
    },
    optionElement: {
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        paddingVertical: 15,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15
    },
    option: {
        fontSize: 16,
        marginLeft: 15
    }
})