import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Icon from 'react-native-vector-icons/Ionicons';

export default function ModalOrderOption(props) {
    
    const [cancelOrder, setCancelOrder] = useState(true);
    const [cancelPack, setCancelPack] = useState(true);
    const [printOrder, setPrintOrder] = useState(true);

    useEffect(() => {
        
    }, []);

    const cancelOrderFunc = () => {
        setCancelOrder(true)
        setCancelPack(false)
        setPrintOrder(false)
        props.setOption('')     
        props.setModalOrderOptionVisible(false)      
    }

    const cancelPackFunc = () => {
        setCancelOrder(true)
        setCancelPack(true)
        setPrintOrder(false)
        props.setOption('Đóng gói và giao hàng') 
        props.setModalOrderOptionVisible(false)      
    }

    const printOrderFunc = () => {
        setCancelOrder(true)
        setCancelPack(true)
        setPrintOrder(true)
        props.setOption('')  
        props.setModalOrderOptionVisible(false)      
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalOrderOptionVisible}
                onRequestClose={() => {
                    props.setModalOrderOptionVisible(false)            
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalOrderOptionVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                   
                                </View>
                                    
                                <View style={styles.body}>
                                    <View style={styles.orderOptionGroup}>
                                        <TouchableHighlight
                                            onPress={() => cancelOrderFunc()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Icon
                                                    name='close-circle'
                                                    size={20}
                                                    color='gray'
                                                />
                                                <Text style={styles.option}>Hủy đơn</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => cancelPackFunc()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Icon
                                                    name='logo-dropbox'
                                                    size={20}
                                                    color='gray'
                                                />
                                                <Text style={styles.option}>Hủy đóng gói</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => printOrderFunc()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Icon
                                                    name='print'
                                                    size={20}
                                                    color='gray'
                                                />
                                                <Text style={styles.option}>In đơn hàng</Text>
                                            </View>
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};