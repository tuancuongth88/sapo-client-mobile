import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({ 
    modalWrapper: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContent: {
        backgroundColor: '#fff',
        width: width/1.1,
        borderRadius: 10,
        alignItems: 'center'
    },
    modalTitle: {
        fontSize: 18,
        marginBottom: 15,
        marginTop: 10,
        fontWeight: 'bold'
    },
    textInput: {
        width: width/1.1 - 40
    },
    propertyNameWrapper: {
        marginBottom: 15,
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray'
    },
    propertyNameTitle: {
        color: 'gray',
        fontSize: 13
    },
    propertyValueWrapper: {
        marginBottom: 15,
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray'
    },
    propertyValueTitle: {
        color: 'gray',
        fontSize: 13
    },
    bodyModalContent: {
        alignItems: 'center'
    },
    bottomModalContent: {
        height: 50,
        width: width/1.1,
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        flexDirection: 'row'
    },
    closeModal: {
        width: width/2.2,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightColor: 'gray',
        borderRightWidth: 0.5
    },
    addNewPropertyBtn: {
        width: width/2.2,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeModalTitle: {
        color: 'blue'
    },
    addNewPropertyTitle: {
        color: 'blue'
    },
    tagsInput: {
        width: width/1.1 - 40
    },
    tag: { 
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 3,
        marginTop: 10
    },
    tagText: {
        marginVertical: 3,
        marginHorizontal: 10,
        fontSize: 13
    },
    noteWrapper: {
        width: width/1.1 - 40,
        marginBottom: 10
    },
    note: {
        color: 'gray',
        fontSize: 13
    },
})