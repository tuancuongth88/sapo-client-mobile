import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableOpacity, TextInput, TouchableWithoutFeedback } from 'react-native';
import { styles } from './css'
import Tags from "react-native-tags";

export default function ModalAddProperties(props) {

    useEffect(() => {
        
    }, []);

    const onChangePropertyName = (text) => {
        props.setPropertyName(text)
    } 

    const onChangePropertyValue = (tags) => {
        props.setPropertyValue(tags)
    } 

    const closeModalAddNewProperties = () => {
        props.setPropertyName('')
        props.setModalAddPropertiesVisible(false)
    }

    const addNewPropertyToArr = (arr) => {
        var arrTmp = arr;
        arrTmp.push({
            propertyName: props.propertyName,
            propertyValue: props.propertyValue
        })
        props.setPropertiesArr([...arrTmp])
        props.setModalAddPropertiesVisible(false)
        props.setPropertyName('')
        props.setPropertyValue([])
        props.setHasAttributes(true)
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalAddPropertiesVisible}
                onRequestClose={() => {
                    props.setModalAddPropertiesVisible(false)            
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalAddPropertiesVisible(false)}>
                    <View style={styles.modalWrapper}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.bodyModalContent}>
                                    <Text style={styles.modalTitle}>Thêm thuộc tính</Text>
                                    <View style={styles.propertyNameWrapper}>
                                        <Text style={styles.propertyNameTitle}>Tên thuộc tính</Text>
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder='Kích thước, màu sắc, dung tích, chất liệu,...'
                                            onChangeText={text => onChangePropertyName(text)}
                                            value={props.propertyName}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>
                                    <View style={styles.propertyValueWrapper}>
                                        <Text style={styles.propertyValueTitle}>Giá trị thuộc tính</Text>
                                        <Tags
                                            textInputProps={{
                                                placeholder: "Giá trị",
                                            }}
                                            onChangeTags={tags => onChangePropertyValue(tags)}
                                            onTagPress={(index, tagLabel, event, deleted) =>
                                                console.log(index, tagLabel, event, deleted ? "deleted" : "not deleted")
                                            }
                                            containerStyle={styles.tagsInput}
                                            inputStyle={{ backgroundColor: "white" }}
                                            renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                                                <TouchableOpacity 
                                                    key={`${tag}-${index}`} 
                                                    onPress={onPress}
                                                    style={styles.tag}
                                                >
                                                    <Text style={styles.tagText}>{tag}</Text>
                                                </TouchableOpacity>
                                            )}
                                        />
                                    </View>
                                    <View style={styles.noteWrapper}>
                                        <Text style={styles.note}>Giá trị được cách nhau bằng dấu cách hoặc dấu phẩy</Text>
                                    </View>
                                </View>
                                <View style={styles.bottomModalContent}>
                                    <TouchableOpacity 
                                        style={styles.closeModal}
                                        onPress={() => closeModalAddNewProperties()}
                                    >
                                        <Text style={styles.closeModalTitle}>Thoát</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity 
                                        style={styles.addNewPropertyBtn}
                                        onPress={() => addNewPropertyToArr(props.propertiesArr)}
                                    >
                                        <Text style={styles.addNewPropertyTitle}>Thêm</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};

