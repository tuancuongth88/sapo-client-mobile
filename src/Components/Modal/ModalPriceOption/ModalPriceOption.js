import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';

export default function ModalPriceOption(props) {

    useEffect(() => {
        
    }, []);

    const chooseRetail = () => {
        props.setWholesale(false)
        props.setModalPriceOptionVisible(false)
    }

    const chooseWholesale = () => {
        props.setWholesale(true)
        props.setModalPriceOptionVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalPriceOptionVisible}
                onRequestClose={() => {
                    props.setModalPriceOptionVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalPriceOptionVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}> 
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Chọn chính sách giá</Text>
                                </View>
                                <View style={styles.body}>
                                    <TouchableHighlight
                                        onPress={() => chooseRetail()}
                                        underlayColor='lavender'
                                    >
                                        <View style={styles.row}>
                                            <View style={styles.left}>
                                                <Text style={styles.rowTitle}>Giá bán lẻ</Text>
                                            </View>
                                            <View style={styles.right}>
                                                {props.wholesale ? 
                                                    null    
                                                      :
                                                    <Feather
                                                        name='check'
                                                        size={20}
                                                        color='#1C3FAA'
                                                    />
                                                }   
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                    <View style={styles.separate}></View>
                                    <TouchableHighlight
                                        onPress={() => chooseWholesale()}
                                        underlayColor='lavender'
                                        style={{borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}
                                    >
                                        <View style={styles.row}>
                                            <View style={styles.left}>
                                                <Text style={styles.rowTitle}>Giá bán buôn</Text>
                                            </View>
                                            <View style={styles.right}>
                                                {props.wholesale ? 
                                                    <Feather
                                                        name='check'
                                                        size={20}
                                                        color='#1C3FAA'
                                                    /> : null
                                                }   
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};