import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center'
    },
    modalContent: {
        width: width/1.3,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center'
    },
    header: {
        width: width/1.3,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 15
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#404040'
    },
    body: {
        width: width/1.3,
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width/1.3,
        justifyContent: 'center',
        paddingVertical: 15,
        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
    },
    left: {
        width: width/1.3 - 60
    },
    right: {
        width: 30,
        alignItems: 'flex-end'
    },
    rowTitle: {
        fontSize: 16
    },
    separate: {
        borderTopWidth: 0.5,
        borderTopColor: 'gray',
        width: width/1.3 - 30
    }
})