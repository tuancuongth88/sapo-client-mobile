import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { styles } from './css'
import DatePicker from 'react-native-date-picker';
import moment from 'moment';

export default function ModalDateTimePicker(props) {

    useEffect(() => {
        
    }, []);

    const clearBirthday = () => {
        props.setBirthday(new Date())
        props.setBirthdayShow('')
        props.setDatePickerVisible(false)
    }

    const confirm = () => {
        props.setBirthdayShow(moment(props.birthday).format('DD/MM/YYYY'))
        props.setDatePickerVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.datePickerVisible}
                onRequestClose={() => {
                    props.setDatePickerVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setDatePickerVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Ngày sinh</Text>
                                </View>
                                <DatePicker
                                    date={props.birthday}
                                    onDateChange={props.setBirthday}
                                    mode='date'
                                />
                                <View style={styles.bottom}>
                                    <TouchableOpacity
                                        style={styles.cancelBtn}
                                        onPress={() => clearBirthday()}
                                    >
                                        <Text style={styles.btnTitle}>Xóa</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.confirmBtn}
                                        onPress={() => confirm()}
                                    >
                                        <Text style={styles.btnTitle}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};