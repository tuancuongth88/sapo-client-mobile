import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContent: {
        backgroundColor: '#fff',
        borderRadius: 10,
        width: width/1.3
    },
    header: {
        width: width/1.3,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 15
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#404040'
    },
    bottom: {
        flexDirection: 'row',
        borderTopColor: 'gray',
        borderTopWidth: 0.5
    },
    cancelBtn: {
        width: width/2.6,
        borderRightColor: 'gray',
        borderRightWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    confirmBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnTitle: {
        color: 'blue',
        fontSize: 18
    }
})