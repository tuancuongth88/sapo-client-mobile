import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import NumberFormat from 'react-number-format';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';
import ModalDeliveryCostEstimate from '../ModalDeliveryCostEstimate/ModalDeliveryCostEstimate';

export default function ModalDeliveryCost(props) {

    const [modalNumberVisible, setModalNumberVisible] = useState(false);
    const [chooseOtherDeliveryCost, setChooseOtherDeliveryCost] = useState(false);
    const [modalDeliveryCostEstimateVisible, setModalDeliveryCostEstimateVisible] = useState(false);

    useEffect(() => {
        
    }, []);

    const openModalNumber = () => {
        setModalNumberVisible(true)
    }

    const closeModalDeliveryCost = () => {
        props.setModalDeliveryVisible(false)
    }

    const chooseFreeCost = () => {
        setChooseOtherDeliveryCost(false)
        props.setDeliveryCost('0')
        props.setModalDeliveryVisible(false)
    }

    const openModalDeliveryCostEstimate = () => {
        setModalDeliveryCostEstimateVisible(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalDeliveryVisible}
                onRequestClose={() => {
                    props.setModalDeliveryVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalDeliveryCost()}>
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Phí giao hàng</Text>
                        </View>
                        <View style={styles.headerRight}>
                                
                        </View>    
                    </View>

                    <ScrollView
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >   
                        <View style={styles.headerNoteWrapper}>
                            <Text style={styles.headerNote}>Đây là phí giao hàng shop thu của khách</Text>
                        </View>
                        <TouchableHighlight 
                            onPress={() => chooseFreeCost()}
                            underlayColor='lavender'
                        >
                            <View style={styles.rowWrapper}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={[styles.leftText, chooseOtherDeliveryCost ? {color: '#000'} : {color: 'blue'}]}>Miễn phí giao hàng</Text>
                                    </View>
                                    <View style={styles.right}>
                                        {chooseOtherDeliveryCost ?
                                            null
                                              :
                                            <Feather
                                                name='check'
                                                size={20}
                                                color='blue'    
                                            />
                                        }
                                    </View>
                                </View>
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight 
                            onPress={() => openModalNumber()}
                            underlayColor='lavender'
                        >
                            <View style={styles.rowWrapper}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <NumberFormat
                                            value={Number(props.deliveryCost)}
                                            thousandSeparator={','}
                                            displayType={'text'}
                                            renderText={value => <Text style={[styles.leftText, chooseOtherDeliveryCost && props.deliveryCost != '0' ? {color: 'blue'} : {color: '#000'}]}>
                                                                    Khác {props.deliveryCost != '0' ? '(' + value + ')' : null}
                                                                </Text>}
                                        />
                                    </View>
                                    <View style={styles.right}>
                                        {chooseOtherDeliveryCost ?
                                            <Feather
                                                name='check'
                                                size={20}
                                                color='blue'    
                                            /> : null
                                        }
                                    </View>
                                </View>
                                {/* MODAL NUMBER */}
                                    <ModalEvenNumber
                                        modalTitle='Nhập phí giao hàng'
                                        modalEvenNumberVisible={modalNumberVisible}
                                        setModalEvenNumberVisible={setModalNumberVisible}
                                        evenNumber={props.deliveryCost}
                                        setEvenNumber={props.setDeliveryCost}
                                        setChooseOtherDeliveryCost={setChooseOtherDeliveryCost}
                                        setModalDeliveryVisible={props.setModalDeliveryVisible}
                                    />
                                {/* MODAL NUMBER */}
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={() => openModalDeliveryCostEstimate()}
                            underlayColor='lavender'
                        >
                            <View style={styles.costEstimateWrapper}>
                                <View style={styles.costEstimate}>
                                    <Feather
                                        name='truck'
                                        size={25}
                                        color='blue'
                                    />
                                    <Text style={styles.costEstimateTitle}>Phí dự kiến của đối tác vận chuyển</Text>
                                </View>
                                {/* MODAL COST ESTIMATE */}
                                    <ModalDeliveryCostEstimate
                                        modalDeliveryCostEstimateVisible={modalDeliveryCostEstimateVisible}
                                        setModalDeliveryCostEstimateVisible={setModalDeliveryCostEstimateVisible}
                                    />
                                {/* MODAL COST ESTIMATE */}
                            </View>
                        </TouchableHighlight>               
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};