import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        width: width,
        alignItems: 'center'
    },
    headerNoteWrapper: {
        width: width,
        alignItems: 'center',
        backgroundColor: 'lavender'
    },
    headerNote: {
        color: 'gray',
        marginVertical: 5,
        fontSize: 14.5
    },
    row: {
        width: width/1.1,
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray',
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 10
    },
    left: {
        width: width/1.1 - 30
    },
    right: {
        width: 30,
        alignItems: 'flex-end'
    },
    leftText: {
        fontSize: 15,
    },
    rowWrapper: {
        width: width,
        alignItems: 'center'
    },
    costEstimateWrapper: {
        alignItems: 'center',
        width: width,
        paddingVertical: 15
    },
    costEstimate: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    costEstimateTitle: {
        fontSize: 17,
        color: 'blue',
        marginLeft: 10
    }
})