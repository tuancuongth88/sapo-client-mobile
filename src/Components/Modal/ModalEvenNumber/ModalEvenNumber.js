import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';

export default function ModalEvenNumber(props) {

    const [number, setNumber] = useState(props.evenNumber);
    const [percentActive, setPercentActive] = useState(false);
    const [discountMethod, setDiscountMethod] = useState('đ');

    useEffect(() => {
        
    }, []);

    const deleteNumber = () => {
        setNumber('0')
    }

    const chooseVND = () => {
        setPercentActive(false)
        setDiscountMethod('đ')
    }

    const choosePercent = () => {
        setPercentActive(true)
        setDiscountMethod('%')
        if(Number(number) > 100) {
            setNumber('100')
        }
    }

    const sendNumber = (num) => {
        if(number == '0') {
            setNumber(num)
        }
        else {
            if(props.setDiscount && props.setDiscountMethod) {
                var discount = number + num;
                if(Number(discount) > 100 && discountMethod == '%') {
                    setNumber('100')
                }
                else {
                    setNumber(number + num)
                }
            }
            else {
                setNumber(number + num)
            }
        }
    }

    const closeModalNumber = () => {
        setNumber(props.evenNumber)
        props.setModalEvenNumberVisible(false)
    }

    const confirm = () => {
        if(props.setDiscount && props.setDiscountMethod) {
            props.setDiscountMethod(discountMethod);
            props.setEvenNumber(number)
        }
        else if(props.setDeliveryCost) {
            if(number != '0') {
                props.setChooseOtherDeliveryCost(true)
                props.setEvenNumber(number)
            }
            else {
                props.setChooseOtherDeliveryCost(false)
            }
            props.setModalDeliveryVisible(false)
        }
        else {
            props.setEvenNumber(number)
        }
        props.setModalEvenNumberVisible(false)
    }

    const deleteNumberFromRight = () => {
        var newNum = number.slice(0, number.length - 1)
        if(newNum == '') {
            setNumber('0')
        }
        else {
            setNumber(newNum)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalEvenNumberVisible}
                onRequestClose={() => {
                    props.setModalEvenNumberVisible(false)
                }}
            >   
                <TouchableWithoutFeedback onPress={() => closeModalNumber()}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>{props.modalTitle}</Text>
                                </View>
                                <View style={styles.body}>
                                    <View style={styles.numberShow}>
                                        <View style={styles.empty}></View>
                                        <View style={styles.center}>
                                            <NumberFormat
                                                value={Number(number)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.mainNumber}>{value}</Text>}
                                            />
                                        </View>
                                        <View style={styles.empty}>
                                            <TouchableOpacity
                                                onPress={() => deleteNumber()}
                                            >
                                                <Icon
                                                    name='close-circle'
                                                    size={20}
                                                    color='silver'
                                                />   
                                            </TouchableOpacity>                                 
                                        </View>
                                    </View>

                                    {props.setDiscount && props.setDiscountMethod ?
                                        <View style={styles.discountMethods}>
                                            <TouchableWithoutFeedback
                                                onPress={() => chooseVND()}
                                            >
                                                <View style={percentActive ? styles.vndWrapperDeactive : styles.vndWrapperActive}>
                                                    <Text style={percentActive ? styles.vndTitleDeactive : styles.vndTitleActive}>VNĐ</Text>
                                                </View>
                                            </TouchableWithoutFeedback>
                                            <TouchableWithoutFeedback
                                                onPress={() => choosePercent()}
                                            >
                                                <View style={percentActive ? styles.percentWrapperActive : styles.percentWrapperDeactive}>
                                                    <Text style={percentActive ? styles.percentTitleActive : styles.percentTitleDeactive}>%</Text>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        </View> : null
                                    }
                                    
                                    <View style={styles.numberBoard}>
                                        <View style={styles.row}>
                                            <TouchableOpacity 
                                                style={styles.leftNumber}
                                                onPress={() => sendNumber('1')}
                                            >
                                                <Text style={styles.number}>1</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.centerNumber}
                                                onPress={() => sendNumber('2')}
                                            >
                                                <Text style={styles.number}>2</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.rightNumber}
                                                onPress={() => sendNumber('3')}
                                            >
                                                <Text style={styles.number}>3</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.row}>
                                            <TouchableOpacity 
                                                style={styles.leftNumber}
                                                onPress={() => sendNumber('4')}
                                            >
                                                <Text style={styles.number}>4</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.centerNumber}
                                                onPress={() => sendNumber('5')}
                                            >
                                                <Text style={styles.number}>5</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.rightNumber}
                                                onPress={() => sendNumber('6')}
                                            >
                                                <Text style={styles.number}>6</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.row}>
                                            <TouchableOpacity 
                                                style={styles.leftNumber}
                                                onPress={() => sendNumber('7')}
                                            >
                                                <Text style={styles.number}>7</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.centerNumber}
                                                onPress={() => sendNumber('8')}
                                            >
                                                <Text style={styles.number}>8</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.rightNumber}
                                                onPress={() => sendNumber('9')}
                                            >
                                                <Text style={styles.number}>9</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.row}>
                                            <TouchableOpacity 
                                                style={styles.leftNumber}
                                                onPress={() => sendNumber('000')}
                                            >
                                                <Text style={styles.number}>000</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.centerNumber}
                                                onPress={() => sendNumber('0')}
                                            >
                                                <Text style={styles.number}>0</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity 
                                                style={styles.rightNumber}
                                                onPress={() => deleteNumberFromRight()}
                                            >
                                                <Feather
                                                    name='delete'
                                                    size={30}
                                                    color='gray'
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={styles.bottom}>
                                        <TouchableOpacity
                                            style={styles.cancelBtn}
                                            onPress={() => closeModalNumber()}
                                        >
                                            <Text style={styles.btnTitle}>Hủy</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={styles.confirmBtn}
                                            onPress={() => confirm()}
                                        >
                                            <Text style={styles.btnTitle}>Xác nhận</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
            
        </SafeAreaView>
    );
};