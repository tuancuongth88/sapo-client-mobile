import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center'
    },
    modalContent: {
        width: width/1.3,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center'
    },
    header: {
        width: width/1.3,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 15,
        paddingBottom: 20
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 17,
        color: '#404040'
    },
    body: {
        width: width/1.3,
        alignItems: 'center',
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5
    },
    noteInput: {
        width: width/1.3 - 20,
        minHeight: 100,
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 5,
        marginBottom: 20,
        paddingHorizontal: 10
    },
    bottom: {
        flexDirection: 'row',
    },
    cancelBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightWidth: 0.5,
        borderRightColor: 'gray',
    },
    confirmBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnTitle: {
        color: 'blue',
        fontSize: 18,
        marginVertical: 10
    }
})