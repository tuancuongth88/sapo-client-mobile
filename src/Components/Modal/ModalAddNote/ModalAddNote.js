import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableOpacity, TextInput } from 'react-native';
import { styles } from './css'

export default function ModalAddNote(props) {
    const [inputValue, setInputValue] = useState(props.note)

    useEffect(() => {
        
    }, []);

    const onChangeText = (text) => { console.log(text)
        setInputValue(text)
    }

    const closeModalAddNote = () => {
        if(props.note == '') {
            setInputValue('')
        }
        props.setModalAddNoteVisible(false)
    }

    const confirm = () => {
        props.setNote(inputValue)
        props.setModalAddNoteVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalAddNoteVisible}
                onRequestClose={() => {
                    props.setModalAddNoteVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalAddNoteVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}> 
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Ghi chú đơn hàng</Text>
                                </View>
                                <View style={styles.body}>
                                    <TextInput
                                        style={styles.noteInput}
                                        onChangeText={text => onChangeText(text)}
                                        value={inputValue}
                                        underlineColorAndroid="transparent"
                                        multiline={true}
                                        textAlignVertical='top'
                                    />
                                </View>
                                <View style={styles.bottom}>
                                    <TouchableOpacity
                                        style={styles.cancelBtn}
                                        onPress={() => closeModalAddNote()}
                                    >
                                        <Text style={styles.btnTitle}>Thoát</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.confirmBtn}
                                        onPress={() => confirm()}
                                    >
                                        <Text style={styles.btnTitle}>Xác nhận</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};