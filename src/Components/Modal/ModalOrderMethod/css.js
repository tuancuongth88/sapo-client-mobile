import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

const SIZE = 15;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'flex-end'
    },
    modalContent: {
        width: width,
        borderRadius: 10,
        backgroundColor: '#fff'
    },
    header: {
        width: width,
        alignItems: 'center',
        paddingVertical: 15
    },
    headerTitle: {
        fontSize: 17,
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        width: width
    },
    row: {
        flexDirection: 'row',
        width: width/1.06,
        alignItems: 'center'
    },
    rowElement: {
        width: (width/1.06)/5,
        paddingVertical: 5,
        alignItems: 'center'
    },
    circleActive: {
        width: SIZE ,
        height: SIZE,
        borderRadius: SIZE,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1C3FAA',
    },
    circleDeactive: {
        width: SIZE,
        height: SIZE,
        borderRadius: SIZE,
        borderWidth: 1.5,
        borderColor: 'gray'
    },
    firstCircle: {
        marginLeft: (width/1.06)/20
    },
    orderOptionGroup: {
        width: width,
        borderTopWidth: 0.5,
        borderColor: 'gray',
        marginVertical: 20
    },
    optionElement: {
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        paddingVertical: 15,
        alignItems: 'center'
    },
    option: {
        fontSize: 16
    }
})