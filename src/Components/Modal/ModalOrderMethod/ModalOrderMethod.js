import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';

export default function ModalOrderMethod(props) {

    const [option1, setOption1] = useState(true);
    const [option2, setOption2] = useState(true);
    const [option3, setOption3] = useState(true);

    useEffect(() => {
        
    }, []);

    const chooseOption1 = () => {
        setOption1(true)
        setOption2(false)
        setOption3(false)
        props.setOrderOption('Tạo đơn hàng')     
        props.setModalOrderMethodsVisible(false)      
    }

    const chooseOption2 = () => {
        setOption1(true)
        setOption2(true)
        setOption3(false)
        props.setOrderOption('Tạo đơn và duyệt') 
        props.setModalOrderMethodsVisible(false)      
    }

    const chooseOption3 = () => {
        setOption1(true)
        setOption2(true)
        setOption3(true)
        props.setOrderOption('Tạo đơn và giao hàng') 
        props.setModalOrderMethodsVisible(false)      
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalOrderMethodsVisible}
                onRequestClose={() => {
                    props.setModalOrderMethodsVisible(false)            
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalOrderMethodsVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.header}>
                                    <Text style={styles.headerTitle}>Thay đổi phương thức đặt hàng</Text>
                                </View>
                                <View style={styles.body}>
                                    <View style={styles.row}>
                                        <View style={styles.rowElement}>
                                            <Text style={styles.title}>Đặt hàng</Text>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <Text style={styles.title}>Duyệt</Text>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <Text style={styles.title}>Đóng gói</Text>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <Text style={styles.title}>Xuất kho</Text>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <Text style={styles.title}>Hoàn thành</Text>
                                        </View>
                                    </View>

                                    <View style={styles.row}>
                                        <View style={styles.rowElement}>
                                            <View style={styles.circleActive}>
                                                <Feather
                                                    name='check'
                                                    size={10}
                                                    color='#fff'
                                                />
                                            </View>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <View style={option2 || option3 ? styles.circleActive : styles.circleDeactive}>
                                                {option2 || option3 ?
                                                    <Feather
                                                        name='check'
                                                        size={10}
                                                        color='#fff'
                                                    /> : null
                                                }
                                                    
                                            </View>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <View style={option3 ? styles.circleActive : styles.circleDeactive}>
                                                {option3 ?
                                                    <Feather
                                                        name='check'
                                                        size={10}
                                                        color='#fff'
                                                    /> : null
                                                }
                                            </View>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <View style={styles.circleDeactive}>
                                                {/* <Feather
                                                    name='check'
                                                    size={10}
                                                    color='#fff'
                                                /> */}
                                            </View>
                                        </View>
                                        <View style={styles.rowElement}>
                                            <View style={styles.circleDeactive}>
                                                {/* <Feather
                                                    name='check'
                                                    size={10}
                                                    color='#fff'
                                                /> */}
                                            </View>
                                        </View>
                                    </View>

                                    <View style={styles.orderOptionGroup}>
                                        <TouchableHighlight
                                            onPress={() => chooseOption1()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Text style={styles.option}>Tạo đơn hàng</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => chooseOption2()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Text style={styles.option}>Tạo đơn và duyệt</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => chooseOption3()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.optionElement}>
                                                <Text style={styles.option}>Tạo đơn và giao hàng</Text>
                                            </View>
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};