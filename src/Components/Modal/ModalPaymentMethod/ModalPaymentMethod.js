import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';

export default function ModalPaymentMethod(props) {

    useEffect(() => {
        
    }, []);

    const closeModalPaymentMethod = () => {
        props.setModalPaymentMethodsVisible(false)
    }

    const choosePaymentByCard = () => {
        props.setPaymentMethod('Quẹt thẻ')
        props.setModalPaymentMethodsVisible(false)
    }

    const choosePaymentByCOD = () => {
        props.setPaymentMethod('COD')
        props.setModalPaymentMethodsVisible(false)
    }

    const choosePaymentByTransfer = () => {
        props.setPaymentMethod('Chuyển khoản')
        props.setModalPaymentMethodsVisible(false)
    }

    const choosePaymentByCash = () => {
        props.setPaymentMethod('Tiền mặt')
        props.setModalPaymentMethodsVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalPaymentMethodsVisible}
                onRequestClose={() => {
                    props.setModalPaymentMethodsVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalPaymentMethod()}>
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Phương thức thanh toán</Text>
                        </View>
                        <View style={styles.headerRight}>
                                
                        </View>        
                    </View>
                    <ScrollView
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <Text style={styles.totalPaymentText}>Tổng khách cần trả</Text>
                        <Text style={styles.totalPaymentNumber}>{props.payment}</Text>

                        <View style={styles.paymentMethodGroup}>
                            <View style={styles.row}>
                                <TouchableWithoutFeedback onPress={() => choosePaymentByCard()}>
                                    <View style={styles.left}>
                                        <Text style={styles.paymentMethodTitle}>Quẹt thẻ</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                    
                                <TouchableWithoutFeedback onPress={() => choosePaymentByCOD()}>
                                    <View style={styles.right}>
                                        <Text style={styles.paymentMethodTitle}>COD</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <View style={styles.row}>
                                <TouchableWithoutFeedback onPress={() => choosePaymentByTransfer()}>
                                    <View style={styles.left}>
                                        <Text style={styles.paymentMethodTitle}>Chuyển khoản</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                
                                <TouchableWithoutFeedback onPress={() => choosePaymentByCash()}>
                                    <View style={styles.right}>
                                        <Text style={styles.paymentMethodTitle}>Tiền mặt</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};