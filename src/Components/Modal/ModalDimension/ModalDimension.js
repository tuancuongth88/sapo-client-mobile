import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TextInput, Modal, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { styles } from './css'

export default function ModalDimension(props) {
    

    useEffect(() => {
        
    }, []);

    const closeModalDimension = () => {
        props.setModalDimensionVisible(false)
    }

    const saveDimension = () => {
        props.setModalDimensionVisible(false)
    }

    const onChangeLong = (long) => {
        if(Number(long) > 9999) {
            props.setLong('9999')
        }
        else {
            props.setLong(long)
        } 
    }

    const onChangeWide = (wide) => {
        if(Number(wide) > 9999) {
            props.setWide('9999')
        }
        else {
            props.setWide(wide)
        } 
    }

    const onChangeHigh = (high) => {
        if(Number(high) > 9999) {
            props.setHigh('9999')
        }
        else {
            props.setHigh(high)
        } 
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalDimensionVisible}
                onRequestClose={() => {
                    props.setModalDimensionVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalDimensionVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.headerWrapper}>
                                    <Text style={styles.header}>Kích thước gói hàng</Text>
                                </View>

                                <View style={styles.body}>
                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Text style={styles.inputTitle}>Chiều dài (cm)</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textInput}
                                                onChangeText={long => onChangeLong(long)}
                                                value={props.long}
                                                underlineColorAndroid="transparent"
                                                keyboardType='numeric'
                                                textAlign='right'
                                            />
                                        </View>
                                    </View>

                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Text style={styles.inputTitle}>Chiều rộng (cm)</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textInput}
                                                onChangeText={wide => onChangeWide(wide)}
                                                value={props.wide}
                                                underlineColorAndroid="transparent"
                                                keyboardType='numeric'
                                                textAlign='right'
                                            />
                                        </View>
                                    </View>

                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Text style={styles.inputTitle}>Chiều cao (cm)</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <TextInput
                                                style={styles.textInput}
                                                onChangeText={high => onChangeHigh(high)}
                                                value={props.high}
                                                underlineColorAndroid="transparent"
                                                keyboardType='numeric'
                                                textAlign='right'
                                            />
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.bottom}>
                                    <TouchableOpacity
                                        style={styles.cancelBtn}
                                        onPress={() => closeModalDimension()}
                                    >
                                        <Text style={styles.btnTitle}>Hủy</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.saveBtn}
                                        onPress={() => saveDimension()}
                                    >
                                        <Text style={styles.btnTitle}>Lưu</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    );
};