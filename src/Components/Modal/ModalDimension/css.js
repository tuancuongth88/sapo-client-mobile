import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    modalContent: {
        width: width/1.3,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center'
    },
    header: {
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10,
        color: '#404040'
    },
    bottom: {
        flexDirection: 'row',
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        marginTop: 10
    },
    cancelBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightWidth: 0.5,
        borderRightColor: 'gray'
    },
    saveBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnTitle: {
        color: 'blue',
        fontSize: 18,
        marginVertical: 10
    },
    row: {
        flexDirection: 'row',
        width: width/1.3 - 30,
        marginBottom: 20
    },
    left: {
        width: (width/1.3 - 30)/2,
        justifyContent: 'center'
    },
    right: {
        width: (width/1.3 - 30)/2,  
    },
    inputTitle: {
        color: 'gray',
        fontSize: 15,
        
    },
    textInput: {
        height: 40,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
    }
})