import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import { width, height } from '../../Dimensions/Dimensions';

export default function ModalAddContactPeople(props) {

    useEffect(() => {
        
    }, []);

    const closeModalAddContactPeople = () => {
        props.setModalAddContactPeopleVisible(false)
    }

    const addContactInfo = () => {

    }

    const getValue = (name, value) => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalAddContactPeopleVisible}
                onRequestClose={() => {
                    props.setModalAddContactPeopleVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalAddContactPeople()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Chỉnh sửa liên hệ</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity
                                onPress={() => addContactInfo()}
                            >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
    
                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Họ tên</Text>
                            <Input
                                placeholder='Họ tên'
                                name='contactPeopleName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Chức vụ</Text>
                            <Input
                                placeholder='Chức vụ'
                                name='position'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Bộ phận</Text>
                            <Input
                                placeholder='Bộ phận'
                                name='department'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Số điện thoại</Text>
                            <Input
                                placeholder='Số điện thoại'
                                name='phone'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                                keyboardType='phone-pad'
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Địa chỉ</Text>
                            <Input
                                placeholder='Địa chỉ'
                                name='address'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};