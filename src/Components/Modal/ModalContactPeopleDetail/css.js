import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

const radioSize = 20;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
    },
    headerRight: {
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    input: {
        marginBottom: 10,
        width: width/1.1,
    },
    inputTitle: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 3
    },
    body: {
        paddingTop: 20,
        alignItems: 'center'
    },
})