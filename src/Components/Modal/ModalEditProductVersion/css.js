import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 30,
        paddingTop: 10
    },
    borderWrapper: {
        width: width/1.05,
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 15,
        paddingTop: 10,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    row: {
        width: width/1.1,
        marginBottom: 10,
    },
    title: {
        fontWeight: 'bold',
        marginBottom: 10,
        fontSize: 15
    },
    numberBorder: {
        height: 45,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
        backgroundColor: '#fff',
        width: width/2.2 - 5,
        justifyContent: 'center'
    },
    number: {
        marginLeft: 15,
        color: 'gray'
    },
    left: {
        width: width/2.2 - 5
    },
    right: {
        marginLeft: 10,
        width: width/2.2 - 5
    },
    switchTitle: {
        fontSize: 16,
        marginLeft: 5
    },
    switch: {
        transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }]
    },
    editBtn: {
        width: width/1.05,
        borderRadius: 10,
        backgroundColor: '#1C3FAA',
        alignItems: 'center',
        paddingVertical: 10
    },
    editBtnTitle: {
        fontSize: 18,
        color: '#fff'
    }
})