import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, Switch, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Input from '../../Input/Input';
import { width, height } from '../../Dimensions/Dimensions';
import Feather from 'react-native-vector-icons/Feather';
import NumberFormat from 'react-number-format';
import ModalRealsNumber from '../ModalRealsNumber/ModalRealsNumber';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';

export default function ModalEditProductVersion(props) {
    
    const [weight, setWeight] = useState('0');
    const [modalWeightVisible, setModalWeightVisible] = useState(false);
    const [modalInventoryAmountVisible, setModalInventoryAmountVisible] = useState(false);
    const [inventoryAmount, setInventoryAmount] = useState('0');
    const [modalImportPriceVisible, setModalImportPriceVisible] = useState(false);
    const [importPrice, setImportPrice] = useState('0');
    const [modalRetailPriceVisible, setModalRetailPriceVisible] = useState(false);
    const [retailPrice, setRetailPrice] = useState('0');
    const [modalWholesalePriceVisible, setModalWholesalePriceVisible] = useState(false);
    const [wholesalePrice, setWholesalePrice] = useState('0');
    const [canSell, setCanSell] = useState(false);
    const toggleSwitch = () => setCanSell(previousState => !previousState);

    useEffect(() => {
       
    }, []);

    const closeModalEditProductVersion = () => {
        props.setModalEditProductVersionVisible(false)
    }

    const openModalWeight = () => {
        setModalWeightVisible(true)
    }

    const openModalInventoryAmount = () => {
        setModalInventoryAmountVisible(true)
    }

    const openModalImportPrice = () => {
        setModalImportPriceVisible(true)
    }
    
    const openModalRetailPrice = () => {
        setModalRetailPriceVisible(true)
    }

    const openModalWholesalePrice = () => {
        setModalWholesalePriceVisible(true)
    }

    const editVersion = () => {

    }
    
    const getValue = (name, value) => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalEditProductVersionVisible}
                onRequestClose={() => {
                    props.setModalEditProductVersionVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalEditProductVersion()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Sửa phiên bản</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity
                                onPress={() => editVersion()}
                            >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.borderWrapper}>
                            <View style={styles.row}>
                                <Text style={styles.title}>Tên phiên bản</Text>
                                <Input
                                    placeholder='Tên phiên bản'
                                    name='versionName'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='Quần sks - 30 - Đen'
                                    editable={true}
                                />
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.title}>Mã sản phẩm / SKU</Text>
                                <Input
                                    placeholder='Mã sản phẩm'
                                    name='productCode'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='PVN01'
                                    editable={true}
                                />
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.title}>Kích thước</Text>
                                <Input
                                    placeholder='Kích thước'
                                    name='demension'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='30'
                                    editable={true}
                                />
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.title}>Màu sắc</Text>
                                <Input
                                    placeholder='Màu sắc'
                                    name='color'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='Đen'
                                    editable={true}
                                />
                            </View>
                            <View style={styles.row}>
                                <Text style={styles.title}>Chất liệu</Text>
                                <Input
                                    placeholder='Chất liệu'
                                    name='fabric'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='Da'
                                    editable={true}
                                />
                            </View>
                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Khối lượng (g)</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalWeight()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(weight)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL WEIGHT */}
                                        <ModalRealsNumber
                                            modalTitle='Khối lượng (g)'
                                            modalRealsNumberVisible={modalWeightVisible}
                                            setModalRealsNumberVisible={setModalWeightVisible}
                                            realsNumber={weight}
                                            setRealsNumber={setWeight}
                                        />
                                    {/* MODAL WEIGHT */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Đơn vị tính</Text>
                                    <Input
                                        placeholder='Đơn vị tính'
                                        name='unit'
                                        borderWidth={width/2.2 - 5}
                                        inputWidth={width/2.2 - 30}
                                        iconDeleteLeft={width/2.3 - 25}
                                        editable={true}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={[styles.borderWrapper, {marginBottom: 20}]}>
                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Tồn kho ban đầu</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalInventoryAmount()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(inventoryAmount)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL INVENTORY AMOUNT */}
                                        <ModalRealsNumber
                                            modalTitle='Tồn kho ban đầu'
                                            modalRealsNumberVisible={modalInventoryAmountVisible}
                                            setModalRealsNumberVisible={setModalInventoryAmountVisible}
                                            realsNumber={inventoryAmount}
                                            setRealsNumber={setInventoryAmount}
                                        />
                                    {/* MODAL INVENTORY AMOUNT */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Giá nhập</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalImportPrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(importPrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL IMPORT PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá nhập'
                                            modalEvenNumberVisible={modalImportPriceVisible}
                                            setModalEvenNumberVisible={setModalImportPriceVisible}
                                            evenNumber={importPrice}
                                            setEvenNumber={setImportPrice}
                                        />
                                    {/* MODAL IMPORT PRICE */}
                                </View>
                            </View>

                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Giá bán lẻ</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalRetailPrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(retailPrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL RETAIL PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá bán lẻ'
                                            modalEvenNumberVisible={modalRetailPriceVisible}
                                            setModalEvenNumberVisible={setModalRetailPriceVisible}
                                            evenNumber={retailPrice}
                                            setEvenNumber={setRetailPrice}
                                        />
                                    {/* MODAL RETAIL PRICE */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Giá bán buôn</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalWholesalePrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(wholesalePrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL WHOLESALE PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá bán buôn'
                                            modalEvenNumberVisible={modalWholesalePriceVisible}
                                            setModalEvenNumberVisible={setModalWholesalePriceVisible}
                                            evenNumber={wholesalePrice}
                                            setEvenNumber={setWholesalePrice}
                                        />
                                    {/* MODAL WHOLESALE PRICE */}
                                </View>
                            </View>
                        </View>

                        <View style={[styles.borderWrapper, {paddingBottom: 10, marginBottom: 20}]}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.left}>
                                    <Text style={styles.switchTitle}>Cho phép bán</Text>
                                </View>
                                <View style={[styles.right, {alignItems: 'flex-end'}]}>
                                    <Switch
                                        onValueChange={toggleSwitch}
                                        value={canSell}
                                        style={styles.switch}
                                    />
                                </View>
                            </View>
                        </View>

                        <TouchableOpacity 
                            style={styles.editBtn}
                            onPress={() => editVersion()}
                        >
                            <Text style={styles.editBtnTitle}>Lưu</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

