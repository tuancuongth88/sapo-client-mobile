import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import ModalScrollList from '../ModalScrollList/ModalScrollList';
import { width, height } from '../../Dimensions/Dimensions';
import ModalDateTimePicker from '../ModalDateTimePicker/ModalDateTimePicker';

export default function ModalCreateCustomer(props) {

    const [locationList, setLocationList] = useState([]);
    const [subDistrictList, setSubDistrictList] = useState([]);
    const [customerGroupList, setCustomerGroupList] = useState([]);
    const [modalLocationVisible, setModalLocationVisible] = useState(false);
    const [modalSubDistrictVisible, setModalSubDistrictVisible] = useState(false);
    const [modalCustomerGroupVisible, setModalCustomerGroupVisible] = useState(false);
    const [showMoreInfo, setShowMoreInfo] = useState(false);
    const [birthday, setBirthday] = useState(new Date());
    const [birthdayShow, setBirthdayShow] = useState('');
    const [datePickerVisible, setDatePickerVisible] = useState(false);
    const [male, setMale] = useState(false);
    const [female, setFemale] = useState(false);

    useEffect(() => {
        
    }, []);

    const closeModalCreateCustomer = () => {
        props.setModalCreateNewCustomerVisible(false)
    }

    const openModalChooseLocation = () => {
        setModalLocationVisible(true)
    }

    const openModalChooseSubDistrict = () => {
        setModalSubDistrictVisible(true)
    }

    const openModalChooseCustomerGroup = () => {
        setModalCustomerGroupVisible(true)
    }

    const openMoreInfo = () => {
        setShowMoreInfo(!showMoreInfo)
    }

    const openDatePicker = () => {
        setDatePickerVisible(true)
    }

    const chooseMale = () => {
        setMale(true)
        setFemale(false)
    }

    const chooseFemale = () => {
        setMale(false)
        setFemale(true)
    }

    const createNewCustomerByIcon = () => {

    }

    const getValue = (name, value) => {
        
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalCreateNewCustomerVisible}
                onRequestClose={() => {
                    props.setModalCreateNewCustomerVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalCreateCustomer()}>
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Thêm khách hàng</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity onPress={() => createNewCustomerByIcon()}>
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                        
                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Tên khách hàng</Text>
                            <Input
                                placeholder='Tên khách hàng'
                                name='customerName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Số điện thoại</Text>
                            <Input
                                placeholder='Số điện thoại'
                                name='phone'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                                keyboardType='phone-pad'
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Email</Text>
                            <Input
                                placeholder='Email'
                                name='email'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Địa chỉ</Text>
                            <Input
                                placeholder='Địa chỉ'
                                name='address'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Khu vực</Text>
                            <TouchableWithoutFeedback onPress={() => openModalChooseLocation()}>
                                <View style={styles.picker}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftText}>Chọn khu vực</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={20}
                                            color='gray'
                                        />
                                    </View>
                                    {/* MODAL LOCATION */}
                                        <ModalScrollList
                                            modalScrollListVisible={modalLocationVisible}
                                            setModalScrollListVisible={setModalLocationVisible}
                                            list={locationList}
                                            listTitle='Chọn khu vực'
                                        />
                                    {/* MODAL LOCATION */}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                                
                        <View style={styles.input}>
                            <Text style={styles.inputTitle}>Phường/Xã</Text>
                            <TouchableWithoutFeedback onPress={() => openModalChooseSubDistrict()}>
                                <View style={styles.picker}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftText}>Chọn phường/xã</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={20}
                                            color='gray'
                                        />
                                    </View>
                                    {/* MODAL SUB-DISTRICT */}
                                        <ModalScrollList
                                            modalScrollListVisible={modalSubDistrictVisible}
                                            setModalScrollListVisible={setModalSubDistrictVisible}
                                            list={subDistrictList}
                                            listTitle='Chọn phường/xã'
                                        />
                                    {/* MODAL SUB-DISTRICT */}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>

                        <View style={styles.moreInfoGroup}>
                            <TouchableOpacity 
                                style={[styles.dropdowWrapper, showMoreInfo ? {borderBottomColor: 'gray', borderBottomWidth: 0.5, marginBottom: 10} : null]}
                                onPress={() => openMoreInfo()}
                            >
                                <View style={styles.leftDropdown}>
                                    <Text style={styles.moreInfoText}>Thông tin thêm</Text>
                                    <Text style={styles.moreInfoNote}>Bổ sung mã khách hàng, nhóm, giới tính</Text>
                                </View>
                                <View style={styles.rightDropdown}>
                                    <Feather
                                        name={showMoreInfo ? 'chevron-up' : 'chevron-down'}
                                        size={25}
                                        color='gray'
                                    />
                                </View>
                            </TouchableOpacity>

                            {showMoreInfo ? 
                                <View>
                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Ngày sinh</Text>
                                        <View style={styles.birthdayGroup}>
                                            <View style={styles.birthdayShow}>
                                                <Text style={[styles.birthday, birthdayShow == '' ? {color: 'gray'} : {color: '#000'}]}>
                                                    {birthdayShow == '' ? 'Ngày sinh' : birthdayShow}
                                                </Text>
                                            </View>
                                            <View style={styles.calendar}>
                                                <TouchableOpacity onPress={() => openDatePicker()}>
                                                    <Feather
                                                        name='calendar'
                                                        size={35}
                                                        color='gray'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        {datePickerVisible ? 
                                            <ModalDateTimePicker
                                                datePickerVisible={datePickerVisible}
                                                setDatePickerVisible={setDatePickerVisible}
                                                birthday={birthday}
                                                setBirthday={setBirthday}
                                                birthdayShow={birthdayShow}
                                                setBirthdayShow={setBirthdayShow}
                                            /> : null
                                        }
                                    </View>

                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Mã khách hàng</Text>
                                        <Input
                                            placeholder='Mã khách hàng'
                                            name='customerCode'
                                            borderWidth={width/1.1}
                                            inputWidth={width/1.1 - 30}
                                            iconDeleteLeft={width/1.15 - 25}
                                            editable={true}
                                        />
                                    </View>

                                    <View style={styles.input}>
                                        <Text style={styles.inputTitle}>Nhóm khách hàng</Text>
                                        <TouchableWithoutFeedback onPress={() => openModalChooseCustomerGroup()}>
                                            <View style={styles.picker}>
                                                <View style={styles.left}>
                                                    <Text style={styles.leftText}>Chọn nhóm khách</Text>
                                                </View>
                                                <View style={styles.right}>
                                                    <Feather
                                                        name='chevron-right'
                                                        size={20}
                                                        color='gray'
                                                    />
                                                </View>
                                                {/* MODAL CUSTOMER GROUP */}
                                                    <ModalScrollList
                                                        modalScrollListVisible={modalCustomerGroupVisible}
                                                        setModalScrollListVisible={setModalCustomerGroupVisible}
                                                        list={customerGroupList}
                                                        listTitle='Nhóm khách hàng'
                                                    />
                                                {/* MODAL CUSTOMER GROUP */}
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>

                                    <View style={[styles.input, {marginTop: 10, marginBottom: 50}]}>
                                        <Text style={styles.inputTitle}>Giới tính</Text>
                                        <View style={styles.genderGroup}>
                                            <View style={styles.maleGroupWrapper}>
                                                <TouchableWithoutFeedback onPress={() => chooseMale()}>
                                                    <View style={styles.maleGroup}>
                                                        <View style={[styles.radioBtn, male ? {borderColor: '#1C3FAA'} : {borderColor: 'gray'}]}>
                                                            <View style={[styles.radioChecked, male ? {backgroundColor: '#1C3FAA'} : null]}></View>
                                                        </View>
                                                        <Text style={styles.radioTitle}>Nam</Text>
                                                    </View>
                                                </TouchableWithoutFeedback>
                                            </View>
                                            <View style={styles.femaleGroupWrapper}>
                                                <TouchableWithoutFeedback onPress={() => chooseFemale()}>
                                                    <View style={styles.femaleGroup}>
                                                        <View style={[styles.radioBtn, female ? {borderColor: '#1C3FAA'} : {borderColor: 'gray'}]}>
                                                            <View style={[styles.radioChecked, female ? {backgroundColor: '#1C3FAA'} : null]}></View>
                                                        </View>
                                                        <Text style={styles.radioTitle}>Nữ</Text>
                                                    </View>
                                                </TouchableWithoutFeedback>
                                            </View>
                                        </View>
                                    </View>
                                </View> : null
                            } 
                        </View>
                    </ScrollView>

                    <View style={styles.bottom}>
                        <TouchableOpacity
                            style={styles.saveBtn}
                            onPress={() => createNewCustomerByBtn()}
                        >
                            <Text style={styles.save}>Lưu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    );
};