import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 30
    },
    borderWrapper: {
        width: width/1.05,
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    productImageWrapper: {
        width: width,
        padding: 15,
    },
    addImage: {
        width: 60,
        height: 60,
        borderRadius: 5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'silver',
        borderWidth: 0.5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    productImageScroll: {
        width: width - 90,
    },
    productImage: {
        width: width/1.05,
        height: 120,
        borderRadius: 5,
    },
    borderTitle: {
        fontSize: 16
    },
    verionInfoRow: {
        paddingVertical: 5,
        flexDirection: 'row'
    },
    infoRowLeft: {
        width: 100
    },
    infoRowRight: {
        width: width/1.05 - 110,
        paddingLeft: 15
    },
    textRowLeft: {
        color: 'gray',
        fontSize: 15
    },
    textRowRight: {
        fontSize: 15
    },
    inventoryAndPriceRow: {
        width: width/1.05 - 20,
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingVertical: 10,
        borderColor: 'gray',
        borderBottomWidth: 0.5
    },
    rowLeft: {
        width: (width/1.05 - 20)/2,
        paddingRight: 15
    },
    rowRight: {
        width: (width/1.05 - 20)/2,
        paddingRight: 10
    },
    note: {
        color: 'gray'
    },
    amount: {
        fontSize: 17
    },
    rowTitle: {
        color: 'gray',
        fontSize: 13,
        marginBottom: 5
    },
    deleteBtn: {
        width: width/2.5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'tomato',
        paddingVertical: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    deleteTitle: {
        color: 'tomato',
        fontSize: 16
    }
})