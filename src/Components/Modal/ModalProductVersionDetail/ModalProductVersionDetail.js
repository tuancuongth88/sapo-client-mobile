import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableHighlight, Image } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ModalEditProductVersion from '../ModalEditProductVersion/ModalEditProductVersion';
import ModalListProductImg from '../ModalListProductImg/ModalListProductImg';
import ModalOptionImgOfVersion from '../ModalOptionImgOfVersion/ModalOptionImgOfVersion';

export default function ModalProductVersionDetail(props) {

    const [modalEditProductVersionVisible, setModalEditProductVersionVisible] = useState(false);
    const [modalListProductImgVisible, setModalListProductImgVisible] = useState(false);
    const [modalOptionImgOfVersionVisible, setModalOptionImgOfVersionVisible] = useState(false);
    const [imgChosen, setImgChosen] = useState(null);
    
    useEffect(() => {
       
    }, []);

    const closeModalProductVersionDetail = () => {
        props.setModalProductVersionDetailVisible(false)
    }

    const openModalEditProductVersion = () => {
        setModalEditProductVersionVisible(true)
    }

    const openModalListProductImg = () => {
        setModalListProductImgVisible(true)
    }

    const openModalOptionImgOfVersion = () => {
        setModalOptionImgOfVersionVisible(true)
    }

    const deleteProductVersion = () => {

    }
    
    const getValue = (name, value) => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalProductVersionDetailVisible}
                onRequestClose={() => {
                    props.setModalProductVersionDetailVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalProductVersionDetail()}
                            >
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Chi tiết phiên bản</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableWithoutFeedback
                                onPress={() => openModalEditProductVersion()}
                            >
                                <MaterialCommunityIcon
                                    name='pencil-outline'
                                    size={25}
                                    color='#808080'
                                    style={styles.iconEdit}
                                />
                            </TouchableWithoutFeedback>
                            {/* MODAL EDIT PRODUCT VERSION */}
                                <ModalEditProductVersion
                                    modalEditProductVersionVisible={modalEditProductVersionVisible}
                                    setModalEditProductVersionVisible={setModalEditProductVersionVisible}
                                />
                            {/* MODAL EDIT PRODUCT VERSION */}
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={[styles.productImageWrapper, imgChosen != null && !props.arrImgEmpty ? {alignItems: 'center'} : null]}>
                            {imgChosen == null || props.arrImgEmpty ? 
                                <TouchableWithoutFeedback onPress={() => openModalListProductImg()}>
                                    <View style={styles.addImage}>
                                        <MaterialIcon
                                            name='add-photo-alternate'
                                            size={30}
                                            color='gray'
                                        />
                                    </View>
                                </TouchableWithoutFeedback> : null
                            }
                            {/* MODAL LIST PRODUCT IMAGE */}
                                <ModalListProductImg
                                    modalListProductImgVisible={modalListProductImgVisible}
                                    setModalListProductImgVisible={setModalListProductImgVisible}
                                    imgArr={props.imgArr}
                                    setImgArr={props.setImgArr}
                                    imgChosen={imgChosen}
                                    setImgChosen={setImgChosen}
                                />
                            {/* MODAL LIST PRODUCT IMAGE */}
                            {imgChosen != null && !props.arrImgEmpty ?
                                <TouchableWithoutFeedback onPress={() => openModalOptionImgOfVersion()}>
                                    <Image
                                        style={styles.productImage}
                                        source={{uri: imgChosen.path}}
                                    />
                                </TouchableWithoutFeedback> : null
                            }
                            {/* MODAL OPTION IMAGE OF VERSION */}
                                <ModalOptionImgOfVersion
                                    modalOptionImgOfVersionVisible={modalOptionImgOfVersionVisible}
                                    setModalOptionImgOfVersionVisible={setModalOptionImgOfVersionVisible}
                                    imgArr={props.imgArr}
                                    setImgArr={props.setImgArr}
                                    imgChosen={imgChosen}
                                    setImgChosen={setImgChosen}
                                />
                            {/* MODAL OPTION IMAGE OF VERSION */}
                        </View>

                        <View style={[styles.borderWrapper, {paddingTop: 10, paddingLeft: 10, paddingBottom: 5}]}>
                            <Text style={[styles.borderTitle, {marginBottom: 10}]}>Quần sks - 30 - Đen</Text>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>SKU</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : PVN01
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Barcode</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : PVN01
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Khối lượng</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : 100g
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Đơn vị tính</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}    
                                    >
                                        : Chiếc
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Kích thước</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : 30
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Màu săc</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : Đen
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.verionInfoRow}>
                                <View style={styles.infoRowLeft}>
                                    <Text style={styles.textRowLeft}>Chất liệu</Text>
                                </View>
                                <View style={styles.infoRowRight}>
                                    <Text 
                                        style={styles.textRowRight}
                                        numberOfLines={1}
                                    >
                                        : Da
                                    </Text>
                                </View>
                            </View>
                        </View>  

                        <View style={[styles.borderWrapper, {alignItems: 'center'}]}>
                            <View style={styles.inventoryAndPriceRow}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.borderTitle}>Tồn kho</Text>
                                    <Text style={styles.note}>Vị trí lưu kho: ---</Text>
                                </View>
                                <View style={[styles.rowRight, {alignItems: 'flex-end', justifyContent: 'flex-end'}]}>
                                    <Text 
                                        style={styles.amount}
                                        numberOfLines={1}
                                    >
                                        100
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.inventoryAndPriceRow}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.rowTitle}>Giá bán lẻ</Text>
                                    <Text 
                                        style={styles.amount}
                                        numberOfLines={1}
                                    >
                                        100,000
                                    </Text>
                                </View>
                                <View style={styles.rowRight}>
                                    <Text style={styles.rowTitle}>Giá bán buôn</Text>
                                    <Text 
                                        style={styles.amount}
                                        numberOfLines={1}
                                    >
                                        130,000
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.inventoryAndPriceRow}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.rowTitle}>Giá nhập</Text>
                                    <Text 
                                        style={styles.amount}
                                        numberOfLines={1}
                                    >
                                        70,000
                                    </Text>
                                </View>
                                <View style={styles.rowRight}>
                                    
                                </View>
                            </View>
                            <View style={[styles.inventoryAndPriceRow, {paddingVertical: 15}]}>
                                <Feather
                                    name='check-circle'
                                    size={20}
                                    color='green'
                                />
                                <Text style={[styles.borderTitle, {marginLeft: 10}]}>Có áp dụng thuế</Text>
                            </View>
                        </View>

                        <View style={[styles.borderWrapper, {alignItems: 'center', paddingVertical: 15, paddingLeft: 10, flexDirection: 'row'}]}>
                            <Feather
                                name='check-circle'
                                size={20}
                                color='green'
                            />
                            <Text style={[styles.borderTitle, {marginLeft: 10}]}>Cho phép bán</Text>
                        </View>                     

                        <TouchableHighlight                              
                            onPress={() => deleteProductVersion()}
                            underlayColor='gray'
                        >
                            <View style={styles.deleteBtn}>
                                <Text style={styles.deleteTitle}>Xóa phiên bản</Text>
                            </View>
                        </TouchableHighlight>
                    </ScrollView>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

