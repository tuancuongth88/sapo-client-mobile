import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 30,
        paddingTop: 10
    },
    borderWrapper: {
        width: width/1.05,
        borderRadius: 5,
        backgroundColor: '#fff',
        paddingTop: 10,
        alignItems: 'center',
        marginBottom: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    row: {
        width: width/1.1,
        marginBottom: 10,
    },
    title: {
        fontWeight: 'bold',
        marginBottom: 10,
        fontSize: 15,
    },
    borderTitle: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    rowTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    left: {
        flexDirection: 'row',
        width: width/2.2,
        alignItems: 'center'
    },
    right: {
        width: width/2.2,
        alignItems: 'flex-end',
        paddingRight: 5
    },
    propertiesShow: {
        marginBottom: 15,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5
    },
    propertiesWrapper: {
        marginBottom: 30
    },
    propertyNameShow: {
        marginBottom: 5,
        fontWeight: 'bold'
    },
    propertyValueShow: {
        marginBottom: 5,
        color: 'gray'
    },
    rowProperty: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    rowLeft: {
        width: width/1.1 - 30,
    },
    deletePropertyIcon: {
        marginBottom: 5
    },
    moreInfoWrapper: {
        paddingBottom: 10
    },
    moreInfoTitleRow: {
        width: width/1.1,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    moreInfoTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 3
    },
    moreInfoTitleNote: {
        fontSize: 13,
        color: 'gray'
    },
    moreInfoTitleRowLeft: {
        width: width/1.1 - 30
    },
    moreInfoTitleRowRight: {
        width: 30,
        alignItems: 'center'
    },
    moreInfoElementRow: {
        width: width/1.1,
        paddingVertical: 10,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    moreInfoElementRowLeft: {
        width: width/1.1 - 30,
        paddingRight: 15
    },
    moreInfoElementRowRight: {
        width: 30,
        alignItems: 'center'
    },
    moreInfoElementTitle: {
        fontSize: 13,
        color: 'gray',
        marginBottom: 5
    },
    moreInfoElementText: {
        fontSize: 16
    },
    switch: {
        transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }],
    },
    editBtn: {
        width: width/1.05,
        borderRadius: 10,
        backgroundColor: '#1C3FAA',
        alignItems: 'center',
        paddingVertical: 10
    },
    editBtnTitle: {
        fontSize: 18,
        color: '#fff'
    }
})