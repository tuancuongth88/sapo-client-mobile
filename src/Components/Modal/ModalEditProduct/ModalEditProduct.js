import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity, TouchableHighlight, Switch } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import { width, height } from '../../Dimensions/Dimensions';
import Icon from 'react-native-vector-icons/Ionicons';
import ModalPropertyDefine from '../ModalPropertyDefine/ModalPropertyDefine';
import ModalAddProperties from '../ModalAddProperties/ModalAddProperties';
import ModalDescribeProduct from '../ModalDescribeProduct/ModalDescribeProduct';
import ModalCustomList from '../ModalCustomList/ModalCustomList'

export default function ModalEditProduct(props) {

    const [modalPropertyDefineVisible, setModalPropertyDefineVisible] = useState(false);
    const [modalAddPropertiesVisible, setModalAddPropertiesVisible] = useState(false);
    const [modalDescribeProductVisible, setModalDescribeProductVisible] = useState(false);
    const [modalProductTypeVisible, setModalProductTypeVisible] = useState(false);
    const [modalProductBrandVisible, setModalProductBrandVisible] = useState(false);
    const [switchShowMoreInfo, setSwitchShowMoreInfo] = useState(true);
    const [propertyName, setPropertyName] = useState('');
    const [propertyValue, setPropertyValue] = useState([]);
    const [propertiesArr, setPropertiesArr] = useState([]);
    const [trading, setTrading] = useState(true);
    const toggleSwitch = () => setTrading(previousState => !previousState);

    useEffect(() => {
       
    }, []);

    const closeModalEditProduct = () => {
        props.setModalEditProductVisible(false)
    }

    const openModalAddProperties = () => {
        setModalAddPropertiesVisible(true)
    }

    const openModalPropertyDefine = () => {
        setModalPropertyDefineVisible(true)
    }

    const openModalProductType = () => {
        setModalProductTypeVisible(true)
    }

    const openModalProductBrand = () => {
        setModalProductBrandVisible(true)
    }

    const openModalDescribeProduct = () => {
        setModalDescribeProductVisible(true)
    }

    const deleteProperty = (keyOfProperty) => {
        var arrTmp = propertiesArr;
        arrTmp.splice(keyOfProperty, 1)
        setPropertiesArr([...arrTmp])
    }

    const showOnOffMoreInfo = () => {
        setSwitchShowMoreInfo(!switchShowMoreInfo);
    }

    const save = () => {

    }
    
    const getValue = (name, value) => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalEditProductVisible}
                onRequestClose={() => {
                    props.setModalEditProductVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalEditProduct()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Sửa sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableOpacity
                                onPress={() => save()}
                            >
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.borderWrapper}>
                            <View style={styles.row}>
                                <Text style={styles.title}>Tên phiên bản</Text>
                                <Input
                                    placeholder='Tên phiên bản'
                                    name='versionName'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value='Quần sks'
                                    editable={true}
                                />
                            </View>
                        </View>   

                        <View style={styles.borderWrapper}>
                            <View style={styles.rowTitle}>
                                <View style={styles.left}>
                                    <Text style={styles.borderTitle}>Thuộc tính</Text>
                                    <TouchableOpacity 
                                        onPress={() => openModalPropertyDefine()}
                                        style={{marginLeft: 10}}
                                    >
                                        <Icon
                                            name='information-circle'
                                            size={20}
                                            color='#1C3FAA' 
                                        />
                                    </TouchableOpacity>
                                    {/* MODAL DEFINE PROPERTY */}
                                        <ModalPropertyDefine
                                            modalPropertyDefineVisible={modalPropertyDefineVisible}
                                            setModalPropertyDefineVisible={setModalPropertyDefineVisible}
                                        />
                                    {/* MODAL DEFINE PROPERTY */}
                                </View>
                                <View style={styles.right}>
                                    <TouchableOpacity onPress={() => openModalAddProperties()}>
                                        <Feather
                                            name='plus-circle'
                                            size={22}
                                            color='#1C3FAA'
                                        />
                                    </TouchableOpacity>
                                    {/* MODAL ADD PROPERTIES */}
                                    <ModalAddProperties
                                        modalAddPropertiesVisible={modalAddPropertiesVisible}
                                        setModalAddPropertiesVisible={setModalAddPropertiesVisible}
                                        propertyName={propertyName}
                                        setPropertyName={setPropertyName}
                                        propertyValue={propertyValue}
                                        setPropertyValue={setPropertyValue}
                                        propertiesArr={propertiesArr}
                                        setPropertiesArr={setPropertiesArr}
                                    /> 
                                    {/* MODAL ADD PROPERTIES */} 
                                </View>
                            </View>
                            {propertiesArr.map((property, keyOfProperty) => {
                                return (
                                    <View style={styles.propertiesShow} key={keyOfProperty}>
                                        <Text style={styles.propertyNameShow}>{property.propertyName}</Text>
                                        <View style={styles.rowProperty}>
                                            <View style={styles.rowLeft}>
                                                <Text style={styles.propertyValueShow}>
                                                    {property.propertyValue.map((value, keyOfValue) => {
                                                        if(keyOfValue == property.propertyValue.length - 1) {
                                                            return value
                                                        }
                                                        else {
                                                            return value + ', '
                                                        }
                                                    })}
                                                </Text>
                                            </View>
                                            <TouchableWithoutFeedback
                                                onPress={() => deleteProperty(keyOfProperty)}
                                            >
                                                <Feather
                                                    name='trash-2'
                                                    size={20}
                                                    color='#000'
                                                    style={styles.deletePropertyIcon}
                                                />
                                            </TouchableWithoutFeedback>
                                        </View>
                                    </View>
                                )
                            })}
                        </View>

                        <View style={styles.borderWrapper}>
                            <View style={styles.moreInfoWrapper}>
                                <View style={styles.moreInfoTitleRow}>
                                    <View style={styles.moreInfoTitleRowLeft}>
                                        <Text style={styles.moreInfoTitle}>Thông tin thêm</Text>
                                        <Text style={styles.moreInfoTitleNote}>Mô tả, phân loại sản phẩm</Text>
                                    </View>
                                    <TouchableWithoutFeedback onPress={() => showOnOffMoreInfo()}>
                                        <View style={styles.moreInfoTitleRowRight}>
                                            <Feather
                                                name={switchShowMoreInfo ? 'chevron-up' : 'chevron-down'}
                                                size={20}
                                                color='#808080'
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                {switchShowMoreInfo ? 
                                    <View>
                                         <TouchableHighlight
                                            onPress={() => openModalProductType()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.moreInfoElementRow}>
                                                <View style={styles.moreInfoElementRowLeft}>
                                                    <Text style={styles.moreInfoElementTitle}>Loại sản phẩm</Text>
                                                    <Text 
                                                        style={styles.moreInfoElementText}
                                                        numberOfLines={1}
                                                    >
                                                        Quần
                                                    </Text>
                                                </View>
                                                <View style={styles.moreInfoElementRowRight}>
                                                    <Feather
                                                        name='chevron-right'
                                                        size={20}
                                                        color='#808080'
                                                    />
                                                </View>
                                                {/* MODAL PRODUCT TYPE */}
                                                    <ModalCustomList
                                                        headerTitle='Loại sản phẩm'
                                                        modalCustomListVisible={modalProductTypeVisible}
                                                        setModalCustomListVisible={setModalProductTypeVisible}
                                                    />
                                                {/* MODAL PRODUCT TYPE */}
                                            </View>
                                        </TouchableHighlight>                                       

                                        <TouchableHighlight
                                            onPress={() => openModalProductBrand()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.moreInfoElementRow}>
                                                <View style={styles.moreInfoElementRowLeft}>
                                                    <Text style={styles.moreInfoElementTitle}>Nhãn hiệu</Text>
                                                    <Text 
                                                        style={styles.moreInfoElementText}
                                                        numberOfLines={1}
                                                    >
                                                        --
                                                    </Text>
                                                </View>
                                                <View style={styles.moreInfoElementRowRight}>
                                                    <Feather
                                                        name='chevron-right'
                                                        size={20}
                                                        color='#808080'
                                                    />
                                                </View>
                                                {/* MODAL PRODUCT BRAND */}
                                                    <ModalCustomList
                                                        headerTitle='Nhãn hiệu'
                                                        modalCustomListVisible={modalProductBrandVisible}
                                                        setModalCustomListVisible={setModalProductBrandVisible}
                                                    />
                                                {/* MODAL PRODUCT BRAND */}
                                            </View>
                                        </TouchableHighlight>
                                        
                                        <TouchableHighlight
                                            onPress={() => openModalDescribeProduct()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.moreInfoElementRow}>
                                                <View style={styles.moreInfoElementRowLeft}>
                                                    <Text style={styles.moreInfoElementTitle}>Mô tả</Text>
                                                    <Text 
                                                        style={styles.moreInfoElementText}
                                                        numberOfLines={1}
                                                    >
                                                        --
                                                    </Text>
                                                </View>
                                                <View style={styles.moreInfoElementRowRight}>
                                                    <Feather
                                                        name='chevron-right'
                                                        size={20}
                                                        color='#808080'
                                                    />
                                                </View>
                                                {/* MODAL DESCRIBE PRODUCT */}
                                                    <ModalDescribeProduct
                                                        modalDescribeProductVisible={modalDescribeProductVisible}
                                                        setModalDescribeProductVisible={setModalDescribeProductVisible}
                                                    />
                                                {/* MODAL DESCRIBE PRODUCT */}
                                            </View>
                                        </TouchableHighlight>
                                    </View> : null
                                }
                            </View>    
                        </View>

                        <View style={[styles.borderWrapper, {paddingBottom: 10, marginBottom: 20}]}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.left}>
                                    <Text style={styles.switchTitle}>Đang giao dịch</Text>
                                </View>
                                <View style={[styles.right, {alignItems: 'flex-end'}]}>
                                    <Switch
                                        onValueChange={toggleSwitch}
                                        value={trading}
                                        style={styles.switch}
                                    />
                                </View>
                            </View>
                        </View>                   

                        <TouchableOpacity 
                            style={styles.editBtn}
                            onPress={() => save()}
                        >
                            <Text style={styles.editBtnTitle}>Lưu</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

