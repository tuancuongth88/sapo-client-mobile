import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, ScrollView, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import { width, height } from '../../Dimensions/Dimensions';
import NumberFormat from 'react-number-format';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';

export default function ModalEditProductVersionFast(props) {

    const [weight, setWeight] = useState('0');
    const [modalWeightVisible, setModalWeightVisible] = useState(false);

    useEffect(() => {

    }, []);

    const closeModalEditProductVersionFastVersion = () => {
        props.setModalEditProductVersionFastVisible(false)
    }

    const saveEditVersion = () => {
       
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalEditProductVersionFastVisible}
                onRequestClose={() => {
                    props.setModalEditProductVersionFastVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalEditProductVersionFastVersion()}>
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>{props.version.versionName}</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableWithoutFeedback onPress={() => saveEditVersion()}>
                                <Feather
                                    name='check'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.borderWrapper}>
                            <View style={styles.row}>
                                <Text style={styles.title}>Mã sản phẩm / SKU</Text>
                                <Input
                                    placeholder='Mã sản phẩm'
                                    name='productCode'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    value={props.version.sku + ' - ' + props.version.versonName}
                                    editable={false}
                                />
                            </View>

                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Khối lượng (g)</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalWeight()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(weight)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL WEIGHT */}
                                        <ModalEvenNumber
                                            modalTitle='Khối lượng (g)'
                                            modalEvenNumberVisible={modalWeightVisible}
                                            setModalEvenNumberVisible={setModalWeightVisible}
                                            evenNumber={weight}
                                            setEvenNumber={setWeight}
                                        />
                                    {/* MODAL WEIGHT */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Đơn vị tính</Text>
                                    <Input
                                        placeholder='Đơn vị tính'
                                        name='unit'
                                        borderWidth={width/2.2 - 5}
                                        inputWidth={width/2.2 - 30}
                                        iconDeleteLeft={width/2.3 - 25}
                                        editable={true}
                                    />
                                </View>
                            </View>
                        </View>
                    </ScrollView>     
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

