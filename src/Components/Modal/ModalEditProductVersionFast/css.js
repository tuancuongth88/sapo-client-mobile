import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 40
    },
    headerRight: {
        alignItems: 'center',
        width: 40
    },
    headerCenter: {
        width: width/1.08 - 80,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 40,
        width: width
    },
    borderWrapper: {
        width: width/1.05,
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 15,
        paddingTop: 10,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    row: {
        marginBottom: 15
    },
    left: {
        width: width/2.2 - 5
    },
    right: {
        marginLeft: 10,
        width: width/2.2 - 5
    },
    title: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
    },
})