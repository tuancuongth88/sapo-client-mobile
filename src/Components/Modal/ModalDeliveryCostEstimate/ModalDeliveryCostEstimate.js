import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import NumberFormat from 'react-number-format';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';
import ModalDimension from '../ModalDimension/ModalDimension';
import ModalRealsNumber from '../ModalRealsNumber/ModalRealsNumber';
import ModalScrollList from '../ModalScrollList/ModalScrollList';
import ModalPlaceOfReceipt from '../ModalPlaceOfReceipt/ModalPlaceOfReceipt';

export default function ModalDeliveryCostEstimate(props) {
    
    const [modalNumberVisible, setModalNumberVisible] = useState(false);
    const [modalWeightVisible, setModalWeightVisible] = useState(false);
    const [modalDimensionVisible, setModalDimensionVisible] = useState(false);
    const [modalPlaceOfReceiptVisible, setModalPlaceOfReceiptVisible] = useState(false);
    const [codCost, setCodCost] = useState('0');
    const [weight, setWeight] = useState('100');
    const [long, setLong] = useState('10');
    const [wide, setWide] = useState('10');
    const [high, setHigh] = useState('10');
    const [locationList, setLocationList] = useState([]);
    const [subDistrictList, setSubDistrictList] = useState([]);
    const [modalLocationVisible, setModalLocationVisible] = useState(false);
    const [modalSubDistrictVisible, setModalSubDistrictVisible] = useState(false);

    useEffect(() => {
        
    }, []);

    const closeModalDeliveryCostEstimate = () => {
        props.setModalDeliveryCostEstimateVisible(false)
    }

    const openModalChooseLocation = () => {
        setModalLocationVisible(true)
    }

    const openModalPlaceOfReceipt = () => {
        setModalPlaceOfReceiptVisible(true)
    }
    
    const openModalCOD = () => {
        setModalNumberVisible(true)
    }

    const openModalWeight = () => {
        setModalWeightVisible(true)
    }

    const openModalDimension = () => {
        setModalDimensionVisible(true)
    }

    return ( 
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalDeliveryCostEstimateVisible}
                onRequestClose={() => {
                    props.setModalDeliveryCostEstimateVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalDeliveryCostEstimate()}
                            >
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Phí dự kiến</Text>
                        </View>
                        <View style={styles.headerRight}>
                            
                        </View> 
                    </View>
                    <ScrollView
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.topTitleWrapper}>
                            <Text style={styles.topTitle}>Thông tin giao hàng</Text>
                        </View>
                        
                        <TouchableHighlight
                            onPress={() => openModalPlaceOfReceipt()}
                            underlayColor='#E0E0E0'
                        >
                            <View style={styles.row}>
                                <View style={styles.left}>
                                    <Text style={styles.leftText}>Địa chỉ lấy hàng</Text>
                                </View>
                                <View style={styles.right}>
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                               {/* MODAL PLACE OF RECEIPT */}
                                    <ModalPlaceOfReceipt
                                        modalPlaceOfReceiptVisible={modalPlaceOfReceiptVisible}
                                        setModalPlaceOfReceiptVisible={setModalPlaceOfReceiptVisible}
                                    />
                               {/* MODAL PLACE OF RECEIPT */}
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={() => openModalChooseLocation()}
                            underlayColor='#E0E0E0'
                        >
                            <View style={styles.row}>
                                <View style={styles.left}>
                                    <Text style={styles.leftText}>Địa chỉ giao hàng</Text>
                                </View>
                                <View style={styles.right}>
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                                 {/* MODAL LOCATION */}
                                 <ModalScrollList
                                        modalScrollListVisible={modalLocationVisible}
                                        setModalScrollListVisible={setModalLocationVisible}
                                        list={locationList}
                                        listTitle='Chọn khu vực'
                                    />
                                {/* MODAL LOCATION */}

                                {/* MODAL SUB-DISTRICT */}
                                    <ModalScrollList
                                        modalScrollListVisible={modalSubDistrictVisible}
                                        setModalScrollListVisible={setModalSubDistrictVisible}
                                        list={subDistrictList}
                                        listTitle='Chọn phường/xã'
                                    />
                                {/* MODAL SUB-DISTRICT */}
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={() => openModalCOD()}
                            underlayColor='#E0E0E0'
                        >
                            <View style={styles.row}>
                                <View style={styles.left}>
                                    <Text style={styles.leftText}>Thu hộ COD</Text>
                                </View>
                                <View style={styles.right}>
                                    <NumberFormat
                                        value={Number(codCost)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.codCost}>{value}</Text>}
                                    />
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                                {/* MODAL NUMBER */}
                                    <ModalEvenNumber
                                        modalTitle='Thu hộ COD'
                                        modalEvenNumberVisible={modalNumberVisible}
                                        setModalEvenNumberVisible={setModalNumberVisible}
                                        evenNumber={codCost}
                                        setEvenNumber={setCodCost}
                                    />
                                {/* MODAL NUMBER */}
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={() => openModalWeight()}
                            underlayColor='#E0E0E0'
                        >
                            <View style={styles.row}>
                                <View style={styles.left}>
                                    <Text style={styles.leftText}>Khối lượng (g)</Text>
                                </View>
                                <View style={styles.right}>
                                    <NumberFormat
                                        value={Number(weight)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.weight}>{value}</Text>}
                                    />
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                                {/* MODAL WEIGHT */}
                                    <ModalRealsNumber
                                        modalTitle='Khối lượng (g)'
                                        modalRealsNumberVisible={modalWeightVisible}
                                        setModalRealsNumberVisible={setModalWeightVisible}
                                        realsNumber={weight}
                                        setRealsNumber={setWeight}
                                    />
                                {/* MODAL NUMBER */}
                            </View>
                        </TouchableHighlight>

                        <TouchableHighlight
                            onPress={() => openModalDimension()}
                            underlayColor='#E0E0E0'
                        >
                            <View style={styles.row}>
                                <View style={styles.left}>
                                    <Text style={styles.leftText}>Kích thước (cm)</Text>
                                    <Text style={styles.dimensionNote}>(dài x rộng x cao)</Text>
                                </View>
                                <View style={styles.right}>
                                    <NumberFormat
                                        value={Number(long)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.dimension}>{value}</Text>}
                                    />
                                    <Text style={styles.multiplied}>x</Text>
                                    <NumberFormat
                                        value={Number(wide)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.dimension}>{value}</Text>}
                                    />
                                    <Text style={styles.multiplied}>x</Text>
                                    <NumberFormat
                                        value={Number(high)}
                                        thousandSeparator={','}
                                        displayType={'text'}
                                        renderText={value => <Text style={styles.dimension}>{value}</Text>}
                                    />
                                    <Feather
                                        name='chevron-right'
                                        size={20}
                                        color='gray'
                                    />
                                </View>
                                {/* MODAL DIMENSION */}
                                    <ModalDimension
                                        modalDimensionVisible={modalDimensionVisible}
                                        setModalDimensionVisible={setModalDimensionVisible}
                                        long={long}
                                        wide={wide}
                                        high={high}
                                        setLong={setLong}
                                        setWide={setWide}
                                        setHigh={setHigh}
                                    />
                                {/* MODAL DIMENSION */}
                            </View>
                        </TouchableHighlight>

                        <View style={styles.noteWrapper}>
                            <View style={styles.note}>
                                <Feather
                                    name='alert-circle'
                                    size={25}
                                    color='red'
                                />
                                <Text style={styles.noteText}>Vui lòng nhập đầy đủ thông tin địa chỉ giao hàng để lựa chọn đơn vị vận chuyển</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};