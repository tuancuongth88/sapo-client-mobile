import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        width: width,
        alignItems: 'center'
    },
    topTitleWrapper: {
        width: width/1.1,
        marginTop: 15,
        marginBottom: 10
    },
    topTitle: {
        fontWeight: 'bold',
        fontSize: 17
    },
    row: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5
    },
    left: {
        width: width/2.2,
        paddingLeft: 5,
    },
    right: {
        width: width/2.2,
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center'
    },
    leftText: {
        fontSize: 16,
    },
    codCost: {
        fontWeight: 'bold',
        fontSize: 18,
        marginRight: 5
    },
    weight: {
        fontSize: 16,
        marginRight: 5
    },
    dimension: {
        fontSize: 16,
        marginRight: 5
    },
    multiplied: {
        fontSize: 16,
        marginRight: 5
    },
    dimensionNote: {
        color: 'gray'
    }, 
    noteWrapper: {
        width: width,
        paddingVertical: 15,
        backgroundColor: '#FFF5EB',
        alignItems: 'center',
        marginTop: 10, 
    },
    note: {
        width: width/1.1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    noteText: {
        marginLeft: 5,
    }
})