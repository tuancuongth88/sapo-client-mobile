import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center'
    },
    modalContent: {
        width: width/1.3,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center'
    },
    header: {
        marginVertical: 10,
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        color: '#404040'
    },
    numberShow: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 10
    },
    empty: {
        width: 50,
        justifyContent: 'center',
    },
    center: {
        width: width/1.3 - 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainNumber: {
        fontSize: 30,
    },
    body: {
        alignItems: 'center'
    },
    numberBoard: {
        borderTopColor: 'gray',
        borderTopWidth: 0.5
    },
    row: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row'
    },
    leftNumber: {
        width: width/3.9,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerNumber: {
        width: width/3.9,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightColor: 'gray',
        borderLeftColor: 'gray',
        borderRightWidth: 0.5,
        borderLeftWidth: 0.5
    },
    rightNumber: {
        width: width/3.9,
        justifyContent: 'center',
        alignItems: 'center'
    },
    number: {
        marginVertical: 10,
        color: 'gray',
        fontSize: 17
    },
    bottom: {
        flexDirection: 'row'
    },
    cancelBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
        borderRightWidth: 0.5,
        borderRightColor: 'gray'
    },
    confirmBtn: {
        width: width/2.6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnTitle: {
        color: 'blue',
        fontSize: 18,
        marginVertical: 10
    }
})