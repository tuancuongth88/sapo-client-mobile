import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, ImageBackground, TouchableOpacity } from 'react-native';
import { styles } from './css'
import Icon from 'react-native-vector-icons/Ionicons';

export default function ModalShowFullImage(props) {

    useEffect(() => {
       
    }, []);

    const closeModalShowFullImage = () => {
        props.setModalShowFullImageVisible(false)
        if(props.setModalOptionImgOfVersionVisible) {
            props.setModalOptionImgOfVersionVisible(false)
        }
    }

    const deleteImage = () => {
        if(props.setImgChosen) {
            props.setImgChosen(null)
        }
        if(props.imgShow) {
            var arrTmp = props.imgArr;
            props.imgArr.map((value, key) => {
                if(value == props.imgShow) {
                    arrTmp.splice(key, 1)
                }
            })
            props.setImgArr([...arrTmp]);
        }
        if(props.setModalOptionImgOfVersionVisible) {
            props.setModalOptionImgOfVersionVisible(false)
        }
        props.setModalShowFullImageVisible(false)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalShowFullImageVisible}
                onRequestClose={() => {
                    props.setModalShowFullImageVisible(false)
                }}
            >
                <ImageBackground
                    style={styles.imgBackGround}
                    source={{uri: props.imgChosen ? props.imgChosen.path : props.imgShow.path}}
                >
                    <View style={styles.row}>
                        <View style={styles.left}>
                            <TouchableOpacity
                                style={styles.deleteBtn}
                                onPress={() => deleteImage()}
                            > 
                                <Icon
                                    name='trash-outline'
                                    color='tomato'
                                    size={25}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.right}>
                            <TouchableOpacity
                                style={styles.exitBtn}
                                onPress={() => closeModalShowFullImage()}
                            > 
                                <Icon
                                    name='close-circle'
                                    color='gray'
                                    size={25}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </Modal>
        </SafeAreaView>
    )
}