import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    imgBackGround: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 10
    },
    row: {
        width: width/1.05,
        flexDirection: 'row',
        alignItems: 'center'
    },
    left: {
        width: (width/1.05)/2
    },
    right: {
        width: (width/1.05)/2,
        alignItems: 'flex-end'
    },
})