import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableHighlight, TouchableWithoutFeedback } from 'react-native';
import { styles } from './css'
import ModalListProductImg from '../ModalListProductImg/ModalListProductImg';
import ModalShowFullImage from '../ModalShowFullImage/ModalShowFullImage';

export default function ModalOptionImgOfVersion(props) {
    
    const [modalListProductImgVisible, setModalListProductImgVisible] = useState(false);
    const [modalShowFullImageVisible, setModalShowFullImageVisible] = useState(false);

    useEffect(() => {
       
    }, []);

    const closeModalOptionImgOfVersion = () => {
        props.setModalOptionImgOfVersionVisible(false)
    }

    const showPhoto = () => {
        setModalShowFullImageVisible(true)
    }

    const deletePhoto = () => {
        props.setImgChosen(null)
        props.setModalOptionImgOfVersionVisible(false)
    }

    const changePhoto = () => {
        setModalListProductImgVisible(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.modalOptionImgOfVersionVisible}
                onRequestClose={() => {
                    props.setModalOptionImgOfVersionVisible(false)
                }}
            >
                <TouchableWithoutFeedback onPress={() => props.setModalOptionImgOfVersionVisible(false)}>
                    <View style={styles.container}>
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContent}>
                                <View style={styles.topContent}>
                                    <View style={[styles.row, {paddingVertical: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10}]}>
                                        <Text style={styles.borderTitle}>Tùy chọn</Text>
                                    </View>
                                    <TouchableHighlight
                                        onPress={() => showPhoto()}
                                        underlayColor='whitesmoke'
                                    >
                                        <View style={[styles.row, {paddingVertical: 15}]}>
                                            <Text style={styles.optionTitle}>Xem ảnh</Text>
                                        </View>
                                    </TouchableHighlight>
                                    {/* MODAL SHOW FULL IMAGE */}
                                        <ModalShowFullImage
                                            modalShowFullImageVisible={modalShowFullImageVisible}
                                            setModalShowFullImageVisible={setModalShowFullImageVisible}
                                            setModalOptionImgOfVersionVisible={props.setModalOptionImgOfVersionVisible}
                                            imgChosen={props.imgChosen}
                                            setImgChosen={props.setImgChosen}
                                        />
                                    {/* MODAL SHOW FULL IMAGE */}
                                    <TouchableHighlight
                                        onPress={() => deletePhoto()}
                                        underlayColor='whitesmoke'
                                    >
                                        <View style={[styles.row, {paddingVertical: 15}]}>
                                            <Text style={[styles.optionTitle, {color: 'red'}]}>Xóa ảnh</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        onPress={() => changePhoto()}
                                        underlayColor='whitesmoke'
                                        style={{borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}
                                    >
                                        <View style={[styles.row, {paddingVertical: 15, borderBottomLeftRadius: 10, borderBottomRightRadius: 10}]}>
                                            <Text style={styles.optionTitle}>Đổi ảnh</Text>
                                        </View>
                                    </TouchableHighlight>
                                    {/* MODAL LIST PRODUCT IMAGE */}
                                        <ModalListProductImg
                                            modalListProductImgVisible={modalListProductImgVisible}
                                            setModalListProductImgVisible={setModalListProductImgVisible}
                                            setModalOptionImgOfVersionVisible={props.setModalOptionImgOfVersionVisible}
                                            imgArr={props.imgArr}
                                            setImgArr={props.setImgArr}
                                            imgChosen={props.imgChosen}
                                            setImgChosen={props.setImgChosen}
                                        />
                                    {/* MODAL LIST PRODUCT IMAGE */}
                                </View>
                                <TouchableHighlight 
                                    onPress={() => closeModalOptionImgOfVersion()}
                                    underlayColor='silver'
                                    style={{borderRadius: 10}}
                                >
                                    <View style={styles.bottomContent}>
                                        <Text style={styles.cancel}>Thoát</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </SafeAreaView>
    )
}