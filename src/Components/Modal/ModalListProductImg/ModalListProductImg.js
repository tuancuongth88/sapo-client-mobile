import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, Image } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons'; 
import ModalChooseImages from '../ModalChooseImages/ModalChooseImages';

export default function ModalListProductImg(props) {
    
    const [modalChooseImagesVisible, setModalChooseImagesVisible] = useState(false);

    useEffect(() => {
       
    }, []);

    const closeModalListProductImg = () => {
        props.setModalListProductImgVisible(false)
    }

    const openModalChooseImages = () => {
        setModalChooseImagesVisible(true)
    }

    const choseImageForVersion = (image) => {
        props.setImgChosen(image)
        props.setModalListProductImgVisible(false)
        if(props.setModalOptionImgOfVersionVisible) {
            props.setModalOptionImgOfVersionVisible(false)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalListProductImgVisible}
                onRequestClose={() => {
                    props.setModalListProductImgVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalListProductImg()}
                            >
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Chọn ảnh sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>

                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.bodyContent}>
                            <TouchableWithoutFeedback onPress={() => openModalChooseImages()}>
                                <View style={styles.addImage}>
                                    <Icon
                                        name='camera'
                                        size={30}
                                        color='gray'
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                            {props.imgArr.map((value, key) => { 
                                return(
                                    <TouchableWithoutFeedback 
                                        onPress={() => choseImageForVersion(value)}
                                        key={key}
                                    >
                                        <Image
                                            style={styles.image}
                                            source={{uri: value.path}}
                                        />
                                    </TouchableWithoutFeedback>
                                )
                            })}
                        </View>
                    </ScrollView>
                    {/* MODAL CHOOSE IMAGE */}
                        <ModalChooseImages
                            modalChooseImagesVisible={modalChooseImagesVisible}
                            setModalChooseImagesVisible={setModalChooseImagesVisible}
                            setModalListProductImgVisible={props.setModalListProductImgVisible}
                            setModalOptionImgOfVersionVisible={props.setModalOptionImgOfVersionVisible}
                            imgArr={props.imgArr}
                            setImgArr={props.setImgArr}
                            imgChosen={props.imgChosen}
                            setImgChosen={props.setImgChosen}
                        />
                    {/* MODAL CHOOSE IMAGE */}
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

