import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';

export default function ModalDescribeProduct(props) {

    useEffect(() => {
       
    }, []);

    const closeModalDescribeProduct = () => {
        props.setModalDescribeProductVisible(false)
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalDescribeProductVisible}
                onRequestClose={() => {
                    props.setModalDescribeProductVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback
                                onPress={() => closeModalDescribeProduct()}
                            >
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Mô tả sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>

                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        <Text style={styles.describe}>ádjagsdjhasbdasbdjhbdjdbasdashasbdajsbdasdbahsbdhasdbajhdbhasdbashdbh</Text>
                    </ScrollView>
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

