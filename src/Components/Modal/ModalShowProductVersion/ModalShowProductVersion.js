import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Modal, ScrollView, TouchableHighlight } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import ModalEditProductVersionFast from '../ModalEditProductVersionFast/ModalEditProductVersionFast';
import  postCreateProduct from '../../../Api/postCreateProduct';
import AsyncStorage from '@react-native-community/async-storage';

export default function ModalShowProductVersion(props) {

    const [modalEditProductVersionFastVisible, setModalEditProductVersionFastVisible] = useState(false);
    const [versionKey, setVersionKey] = useState(null);
    const [arrCombinedProperties, setArrayCombineProperties] = useState(props.productObj.arrCombinedProperties);
    const [version, setVersion] = useState({});

    useEffect(() => {

    }, []);

    const closeModalShowProductVersion = () => {
        props.setModalShowProductVersionVisible(false)
    }

    const openModalEditProductVersionFast = (key, version) => {
        setVersionKey(key)
        setVersion(version)
        setModalEditProductVersionFastVisible(true)
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalShowProductVersionVisible}
                onRequestClose={() => {
                    props.setModalShowProductVersionVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalShowProductVersion()}>
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Phiên bản sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>
                            
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        { props.productObj.arrCombinedProperties ? 
                            props.productObj.arrCombinedProperties.map((value, key) => {
                                return(
                                    <TouchableHighlight
                                        onPress={() => openModalEditProductVersionFast(key, value)}
                                        underlayColor='lavender'
                                        key={key}
                                    >
                                        <View style={styles.row}>
                                            <View style={styles.left}>
                                                <Text style={styles.rowTitle}>{value.versionName}</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Text style={styles.inventory}>Tồn kho: {value.inventory}</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                )}) : null
                        }
                    </ScrollView>   
                    {/* MODAL EDIT PRODUCT VERSION FAST   */}
                        <ModalEditProductVersionFast
                            modalEditProductVersionFastVisible={modalEditProductVersionFastVisible}
                            setModalEditProductVersionFastVisible={setModalEditProductVersionFastVisible}
                            key={versionKey}
                            version={version}
                            setArrayCombineProperties={setArrayCombineProperties}
                            arrCombinedProperties={arrCombinedProperties}
                        />
                    {/* MODAL EDIT PRODUCT VERSION FAST   */}
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

