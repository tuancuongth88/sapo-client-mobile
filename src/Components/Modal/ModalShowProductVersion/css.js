import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 40
    },
    headerRight: {
        alignItems: 'center',
        width: 40
    },
    headerCenter: {
        width: width/1.08 - 80,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 40,
        width: width
    },
    row: {
        width: width,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        borderColor: 'gray',
        borderBottomWidth: 0.5
    },
    left: {
        width: width*2/3,
        paddingLeft: 15,
    },
    right: {
        width: width*1/3,
        paddingRight: 15,
        alignItems: 'flex-end'
    },
    rowTitle: {
        fontSize: 16
    },
    inventory: {
        fontSize: 13,
        color: 'gray'
    }
})