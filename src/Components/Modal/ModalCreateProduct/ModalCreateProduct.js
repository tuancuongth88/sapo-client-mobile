import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity, Image, TouchableHighlight, Switch, Alert } from 'react-native';
import { styles } from './css'
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Input/Input';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { width, height } from '../../Dimensions/Dimensions';
import NumberFormat from 'react-number-format';
import ModalAddProperties from '../ModalAddProperties/ModalAddProperties';
import ModalShowFullImage from '../ModalShowFullImage/ModalShowFullImage';
import ModalChooseImages from '../ModalChooseImages/ModalChooseImages';
import ModalEvenNumber from '../ModalEvenNumber/ModalEvenNumber';
// import ModalRealsNumber from '../ModalRealsNumber/ModalRealsNumber';
import ModalDescribeProduct from '../ModalDescribeProduct/ModalDescribeProduct';
import ModalCustomList from '../ModalCustomList/ModalCustomList';
import ModalScrollList from '../ModalScrollList/ModalScrollList';
import ModalShowProductVersion from '../ModalShowProductVersion/ModalShowProductVersion';
import  postCreateProduct from '../../../Api/postCreateProduct';
import AsyncStorage from '@react-native-community/async-storage';

export default function ModalCreateProduct(props) {

    const [imgArr, setImgArr] = useState([]);
    const [modalAddPropertiesVisible, setModalAddPropertiesVisible] = useState(false);
    const [modalShowFullImageVisible, setModalShowFullImageVisible] = useState(false);
    const [modalChooseImagesVisible, setModalChooseImagesVisible] = useState(false);
    const [modalWeightVisible, setModalWeightVisible] = useState(false);
    const [modalWeightUnitVisible, setModalWeightUnitVisible] = useState(false);
    const [modalInventoryAmountVisible, setModalInventoryAmountVisible] = useState(false);
    const [modalImportPriceVisible, setModalImportPriceVisible] = useState(false);
    const [modalRetailPriceVisible, setModalRetailPriceVisible] = useState(false);  
    const [modalWholesalePriceVisible, setModalWholesalePriceVisible] = useState(false);  
    const [modalDescribeProductVisible, setModalDescribeProductVisible] = useState(false);
    const [modalProductTypeVisible, setModalProductTypeVisible] = useState(false);
    const [modalProductBrandVisible, setModalProductBrandVisible] = useState(false);
    const [modalShowProductVersionVisible, setModalShowProductVersionVisible] = useState(false);  
    const [switchDelImg, setSwitchDelImg] = useState(false);
    const [switchShowMoreInfo, setSwitchShowMoreInfo] = useState(false);
    const [imgShow, setImgShow] = useState({});
    const [propertyName, setPropertyName] = useState('');
    const [propertyValue, setPropertyValue] = useState([]);
    const [propertiesArr, setPropertiesArr] = useState([]);
    // const [canSell, setCanSell] = useState(true);
    // const toggleSwitch = () => setCanSell(previousState => !previousState);

    const [productName, setProductName] = useState('');
    const [productCode, setProductCode] = useState('');
    const [weight, setWeight] = useState('0');
    const [weightUnit, setWeightUnit] = useState({title: 'g'});
    const [unit, setUnit] = useState('');
    const [description, setDescripion] = useState('');
    const [inventoryAmount, setInventoryAmount] = useState('0');
    const [importPrice, setImportPrice] = useState('0');
    const [retailPrice, setRetailPrice] = useState('0');
    const [wholesalePrice, setWholesalePrice] = useState('0');
    const [hasAttributes, setHasAttributes] = useState(false);
    const [categoryId, setCategoryId] = useState(0);
    const [branchId, setBranchId] = useState(0);
    const [taxable, setTaxable] = useState(true);
    const toggleSwitch = () => setTaxable(previousState => !previousState);

    const [productObj, setProductObj] = useState({});

    const weightUnitList = [
        {title: 'g'},
        {title: 'kg'}
    ]

    useEffect(() => {
       
    }, []);

    const openModalShowFullImage = (img) => {
        setImgShow(img)
        setModalShowFullImageVisible(true)
    }

    const openModalChooseImages = () => {
        setModalChooseImagesVisible(true)
    }

    const openModalWeight = () => {
        setModalWeightVisible(true)
    }

    const openModalListWeightUnit = () => {
        setModalWeightUnitVisible(true)
    }

    const openModalInventoryAmount = () => {
        setModalInventoryAmountVisible(true)
    }

    const openModalImportPrice = () => {
        setModalImportPriceVisible(true)
    }
    
    const openModalRetailPrice = () => {
        setModalRetailPriceVisible(true)
    }

    const openModalWholesalePrice = () => {
        setModalWholesalePriceVisible(true)
    }

    const openModalAddNewProperty = () => {
        setModalAddPropertiesVisible(true)
    }

    const openModalProductType = () => {
        setModalProductTypeVisible(true)
    }

    const openModalProductBrand = () => {
        setModalProductBrandVisible(true)
    }

    const openModalDescribeProduct = () => {
        setModalDescribeProductVisible(true)
    }

    const closeModalCreateProduct = () => {
        props.setModalCreateNewProductVisible(false);
        setSwitchShowMoreInfo(false)
    }

    const switchDeleteImg = () => {
        setSwitchDelImg(!switchDelImg)
    }

    const deleteImg = (imgKey) => {
        var arrTmp = imgArr;
        imgArr.map((value, key) => {
            if(key == imgKey) {
                arrTmp.splice(imgKey, 1)
            }
        });
        setImgArr([...arrTmp])
        if(arrTmp.length == 0) {
            setSwitchDelImg(false)
        }
    }

    const showOnOffMoreInfo = () => {
        setSwitchShowMoreInfo(!switchShowMoreInfo);
    }

    const deleteProperty = (keyOfProperty) => {
        var arrTmp = propertiesArr;
        arrTmp.splice(keyOfProperty, 1)
        if(arrTmp.length > 0) {
            setHasAttributes(true)
        }
        else{
            setHasAttributes(false)
        }
        setPropertiesArr([...arrTmp])
    }

    const goToNextStep = () => {
        if(productName == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập tên sản phẩm')
        }
        else {
            var arrPropertyObjs = [];
            var arrPropertyValue = [];
            var arrPropertyName = [];
            var arrCombinedProperties = [];
            propertiesArr.map((value, key) => {
                arrPropertyObjs.push({[value.propertyName]: value.propertyValue})
                arrPropertyValue.push(value.propertyValue)
                arrPropertyName.push(value.propertyName)
            }) 
            var objData = arrPropertyObjs.reduce(function(result, item) { 
                var key = Object.keys(item)[0];
                result[key] = item[key];
                return result;
            }, {});
            console.log(arrPropertyValue)
            if(arrPropertyValue.length == 1) {
                arrPropertyValue[0].map((value, key) => { 
                    let text =  value.charAt(0).toUpperCase() + value.slice(1)
                    arrCombinedProperties.push({
                        [arrPropertyName[0]] : value,
                        versionName: text,
                        sku: productCode,
                        retail_price: retailPrice,
                        whole_sale_price: wholesalePrice,
                        import_price: importPrice,
                        weight: '0',
                        weight_unit: 'g',
                        inventory: '0'
                    });
                })
            }
            else if(arrPropertyValue.length == 2) {
                for(let i=0; i<arrPropertyValue[0].length; i++) {
                    for(let j=0; j<arrPropertyValue[1].length; j++) {
                        let text =  arrPropertyValue[0][i].charAt(0).toUpperCase() + arrPropertyValue[0][i].slice(1)
                                    + ' - ' 
                                    + arrPropertyValue[1][j].charAt(0).toUpperCase() + arrPropertyValue[1][j].slice(1)
                        arrCombinedProperties.push({
                            [arrPropertyName[0]] : arrPropertyValue[0][i],
                            [arrPropertyName[1]] : arrPropertyValue[1][j],
                            versionName: text,
                            sku: productCode,
                            retail_price: retailPrice,
                            whole_sale_price: wholesalePrice,
                            import_price: importPrice,
                            weight: '0',
                            weight_unit: 'g',
                            inventory: '0'
                        });
                    }
                }
            }
            else if(arrPropertyValue.length == 3) {
                for(let i=0; i<arrPropertyValue[0].length; i++) {
                    for(let j=0; j<arrPropertyValue[1].length; j++) {
                        for(let k=0; k<arrPropertyValue[2].length; k++) {
                            let text =  arrPropertyValue[0][i].charAt(0).toUpperCase() + arrPropertyValue[0][i].slice(1)
                                        + ' - ' 
                                        + arrPropertyValue[1][j].charAt(0).toUpperCase() + arrPropertyValue[1][j].slice(1) 
                                        + ' - ' 
                                        + arrPropertyValue[2][k].charAt(0).toUpperCase() + arrPropertyValue[2][k].slice(1) 
                            arrCombinedProperties.push({
                                [arrPropertyName[0]] : arrPropertyValue[0][i],
                                [arrPropertyName[1]] : arrPropertyValue[1][j],
                                [arrPropertyName[2]] : arrPropertyValue[2][k],
                                versionName: text,
                                sku: productCode,
                                retail_price: retailPrice,
                                whole_sale_price: wholesalePrice,
                                import_price: importPrice,
                                weight: '0',
                                weight_unit: 'g',
                                inventory: '0'
                            });
                        }  
                    }
                }
            }
    
            setProductObj({
                title: productName,
                sku: productCode,
                unit: unit,
                weight: weight,
                weight_unit: weightUnit.title,
                description: description,
                image: imgArr,
                retail_price: retailPrice,
                whole_sale_price: wholesalePrice,
                import_price: importPrice,
                inventory: inventoryAmount,
                has_attributes: hasAttributes,
                category_id: categoryId,
                branch_id: branchId,
                taxable: taxable,
                data: objData,
                arrCombinedProperties: arrCombinedProperties,
            }) 
            setModalShowProductVersionVisible(true)
        }
    }

    const createProduct = async() => {
        if(productName == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập tên sản phẩm')
        }
        else {
            const productObj = {
                title: productName,
                sku: productCode,
                unit: unit,
                weight: weight,
                weight_unit: weightUnit.title,
                description: description,
                retail_price: retailPrice,
                whole_sale_price: wholesalePrice,
                import_price: importPrice,
                inventory: inventoryAmount,
                has_attributes: hasAttributes,
                category_id: categoryId,
                branch_id: branchId,
                taxable: taxable,
            }
            const res = await postCreateProduct(
                props.userToken,
                productObj,
                imgArr
            )
            if(res.status == 200) {
                const resCreateProduct = await res.json()
                if(resCreateProduct.code == 200) {
                    Alert.alert('Thông báo', 'Tạo sản phẩm thành công');
                    props.onRefresh()
                    props.setModalCreateNewProductVisible(false)
                }
                else if(resCreateProduct.code == 401) {
                    Alert.alert('Thông báo', 'Phiên làm việc của bạn đã hết hạn. Vui lòng đăng nhập lại !!!');
                    AsyncStorage
                    .clear()
                    .then(() => props.navigation.navigate('LoginNavigation'))
                }
                else if(resCreateProduct.code == 403) {
                    Alert.alert('Lỗi !!!', resCreateProduct.message);
                }
                else if(resCreateProduct.code == 422) {
                    Alert.alert('Lỗi !!!', resCreateProduct.message);
                }
                else if(resCreateProduct.code == 500) {
                    Alert.alert('Lỗi !!!', resCreateProduct.message);
                    console.log(resCreateProduct.message_content)
                }
            }
            else if(res.status == 500) {
                Alert.alert('Lỗi !!!', 'Yêu cầu không thể thực hiện. Vui lòng đăng nhập lại !!!');
            }
        }  
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.modalCreateNewProductVisible}
                onRequestClose={() => {
                    props.setModalCreateNewProductVisible(false)
                }}
            >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerLeft}>
                            <TouchableWithoutFeedback onPress={() => closeModalCreateProduct()}>
                                <Feather
                                    name='x'
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.headerCenter}>
                            <Text style={styles.headerTitle}>Thêm sản phẩm</Text>
                        </View>
                        <View style={styles.headerRight}>
                            <TouchableWithoutFeedback onPress={() => hasAttributes ? goToNextStep() : createProduct()}>
                                <Feather
                                    name={hasAttributes ? 'arrow-right' : 'check'}
                                    size={30}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                    <ScrollView 
                        contentContainerStyle={styles.body}
                        showsVerticalScrollIndicator={false}
                    >
                        {imgArr.length > 0 ?
                            <View style={styles.deleteImgWrapper}>
                                <TouchableOpacity 
                                    style={styles.deleteImgBtn}
                                    onPress={() => switchDeleteImg()}
                                >
                                    <Text style={styles.deleteImgText}>Xóa ảnh</Text>
                                </TouchableOpacity>
                            </View> : null
                        }
                            <View style={styles.productImageWrapper}>
                                <TouchableWithoutFeedback onPress={() => openModalChooseImages()}>
                                    <View style={styles.addImage}>
                                        <MaterialIcon
                                            name='add-photo-alternate'
                                            size={30}
                                            color='gray'
                                        />
                                    </View>
                                </TouchableWithoutFeedback>
                                {/* MODAL CHOOSE IMAGE */}
                                    <ModalChooseImages
                                        modalChooseImagesVisible={modalChooseImagesVisible}
                                        setModalChooseImagesVisible={setModalChooseImagesVisible}
                                        imgArr={imgArr}
                                        setImgArr={setImgArr}
                                    />
                                {/* MODAL CHOOSE IMAGE */}
                                <ScrollView
                                    contentContainerStyle={styles.productImageScroll}
                                    showsHorizontalScrollIndicator={false}
                                    horizontal={true}
                                >
                                    {imgArr.map((value, key) => { 
                                        return(
                                            <View 
                                                style={styles.productImageElement}
                                                key={key}
                                            >
                                                <TouchableWithoutFeedback onPress={() => openModalShowFullImage(value)}>
                                                    <Image
                                                        style={styles.productImage}
                                                        source={{uri: value.path}}
                                                    />
                                                </TouchableWithoutFeedback>
                                                {switchDelImg ?
                                                    <TouchableWithoutFeedback onPress={() => deleteImg(key)}>
                                                        <View style={styles.deleteImg}>
                                                            <View style={styles.redline}></View>
                                                        </View>
                                                    </TouchableWithoutFeedback> : null
                                                }
                                            </View> 
                                        )
                                    })}
                                </ScrollView>
                                {/* MODAL SHOW FULL IMAGE */}
                                    <ModalShowFullImage
                                        modalShowFullImageVisible={modalShowFullImageVisible}
                                        setModalShowFullImageVisible={setModalShowFullImageVisible}                                           
                                        imgArr={imgArr}
                                        setImgArr={setImgArr}
                                        imgShow={imgShow}
                                    />
                                {/* MODAL SHOW FULL IMAGE */}
                        </View>

                        <View style={styles.borderWrapper}>
                            <View style={styles.row}>
                                <Text style={styles.title}>Tên sản phẩm <Text style={styles.require}>*</Text></Text>
                                <Input
                                    placeholder='Nhập tên sản phẩm'
                                    name='productName'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    setValue={setProductName}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.row}>
                                <Text style={styles.title}>Mã sản phẩm</Text>
                                <Input
                                    placeholder='Nhập mã sản phẩm'
                                    name='productCode'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    setValue={setProductCode}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.row}>
                                <Text style={styles.title}>Đơn vị tính</Text>
                                <Input
                                    placeholder='Nhập đơn vị tính'
                                    name='unit'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    setValue={setUnit}
                                    editable={true}
                                />
                            </View>

                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={{width: (width/1.1)*2/3}}>
                                    <Text style={styles.title}>Khối lượng</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalWeight()}>
                                        <View style={styles.weightBorder}>
                                            <NumberFormat
                                                value={Number(weight)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL WEIGHT */}
                                        <ModalEvenNumber
                                            modalTitle='Khối lượng'
                                            modalEvenNumberVisible={modalWeightVisible}
                                            setModalEvenNumberVisible={setModalWeightVisible}
                                            evenNumber={weight}
                                            setEvenNumber={setWeight}
                                        />
                                    {/* MODAL WEIGHT */}
                                </View>
                                <View style={{width: (width/1.1)*1/3 - 10, marginLeft: 10, alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                    <TouchableWithoutFeedback onPress={() => openModalListWeightUnit()}>
                                        <View style={styles.weightUnitPicker}>
                                            <Text style={styles.weightUnitTitle}>{weightUnit.title}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    
                                </View>
                            </View>
                            {/* MODAL CHOOSE WEIGHT UNIT */}
                                <ModalScrollList
                                    listTitle='Đơn vị khối lượng'
                                    modalScrollListVisible={modalWeightUnitVisible}
                                    setModalScrollListVisible={setModalWeightUnitVisible}
                                    list={weightUnitList}
                                    setChooseItem={setWeightUnit}
                                />
                            {/* MODAL CHOOSE WEIGHT UNIT */}
                        </View>

                        <View style={[styles.borderWrapper, {marginBottom: 20}]}>
                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Tồn kho ban đầu</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalInventoryAmount()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(inventoryAmount)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL INVENTORY AMOUNT */}
                                        <ModalEvenNumber
                                            modalTitle='Tồn kho ban đầu'
                                            modalEvenNumberVisible={modalInventoryAmountVisible}
                                            setModalEvenNumberVisible={setModalInventoryAmountVisible}
                                            evenNumber={inventoryAmount}
                                            setEvenNumber={setInventoryAmount}
                                        />
                                    {/* MODAL INVENTORY AMOUNT */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Giá nhập</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalImportPrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(importPrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL IMPORT PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá nhập'
                                            modalEvenNumberVisible={modalImportPriceVisible}
                                            setModalEvenNumberVisible={setModalImportPriceVisible}
                                            evenNumber={importPrice}
                                            setEvenNumber={setImportPrice}
                                        />
                                    {/* MODAL IMPORT PRICE */}
                                </View>
                            </View>

                            <View style={[styles.row, {flexDirection: 'row'}]}>
                                <View style={styles.left}>
                                    <Text style={styles.title}>Giá bán lẻ</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalRetailPrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(retailPrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL RETAIL PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá bán lẻ'
                                            modalEvenNumberVisible={modalRetailPriceVisible}
                                            setModalEvenNumberVisible={setModalRetailPriceVisible}
                                            evenNumber={retailPrice}
                                            setEvenNumber={setRetailPrice}
                                        />
                                    {/* MODAL RETAIL PRICE */}
                                </View>
                                <View style={styles.right}>
                                    <Text style={styles.title}>Giá bán buôn</Text>
                                    <TouchableWithoutFeedback onPress={() => openModalWholesalePrice()}>
                                        <View style={styles.numberBorder}>
                                            <NumberFormat
                                                value={Number(wholesalePrice)}
                                                thousandSeparator={','}
                                                displayType={'text'}
                                                renderText={value => <Text style={styles.number}>{value}</Text>}
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {/* MODAL WHOLESALE PRICE */}
                                        <ModalEvenNumber
                                            modalTitle='Giá bán buôn'
                                            modalEvenNumberVisible={modalWholesalePriceVisible}
                                            setModalEvenNumberVisible={setModalWholesalePriceVisible}
                                            evenNumber={wholesalePrice}
                                            setEvenNumber={setWholesalePrice}
                                        />
                                    {/* MODAL WHOLESALE PRICE */}
                                </View>
                            </View>
                            
                            <View style={[styles.row, {flexDirection: 'row', marginTop: 10}]}>
                                <View style={[styles.left, {paddingLeft: 5}]}>
                                    <Text style={styles.taxableTitle}>Áp dụng thuế</Text>
                                </View>
                                <View style={[styles.right, {paddingRight: 15}]}>
                                    <Switch
                                        onValueChange={toggleSwitch}
                                        value={taxable}
                                        style={styles.switch}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.borderWrapper}>
                            <View style={styles.propertiesTitleWrapper}>
                                <View style={styles.rowLeft}>
                                    <Text style={styles.propertiesTitle}>THUỘC TÍNH</Text>
                                </View>
                                {propertiesArr.length < 3 ? 
                                    <TouchableOpacity 
                                        style={styles.rowRight}
                                        onPress={() => openModalAddNewProperty()}
                                    >
                                        <Feather
                                            name='plus-circle'
                                            color='#1C3FAA'
                                            size={25}
                                        />
                                    </TouchableOpacity> : null
                                }
                                
                                {/* MODAL ADD PROPERTIES */}
                                    <ModalAddProperties
                                        modalAddPropertiesVisible={modalAddPropertiesVisible}
                                        setModalAddPropertiesVisible={setModalAddPropertiesVisible}
                                        propertyName={propertyName}
                                        setPropertyName={setPropertyName}
                                        propertyValue={propertyValue}
                                        setPropertyValue={setPropertyValue}
                                        propertiesArr={propertiesArr}
                                        setPropertiesArr={setPropertiesArr}
                                        setHasAttributes={setHasAttributes}
                                    /> 
                                {/* MODAL ADD PROPERTIES */}   
                            </View>

                            {propertiesArr.map((property, keyOfProperty) => {
                                return (
                                    <View style={styles.propertiesShow} key={keyOfProperty}>
                                        <Text style={styles.propertyNameShow}>{property.propertyName}</Text>
                                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                            <View style={styles.rowLeft}>
                                                <Text style={styles.propertyValueShow}>
                                                    {property.propertyValue.map((value, keyOfValue) => {
                                                        if(keyOfValue == property.propertyValue.length - 1) {
                                                            return value
                                                        }
                                                        else {
                                                            return value + ', '
                                                        }
                                                    })}
                                                </Text>
                                            </View>
                                            <View style={styles.rowRight}>
                                                <TouchableWithoutFeedback
                                                    onPress={() => deleteProperty(keyOfProperty)}
                                                >
                                                    <Feather
                                                        name='trash-2'
                                                        size={20}
                                                        color='#000'
                                                        style={styles.deletePropertyIcon}
                                                    />
                                                </TouchableWithoutFeedback>
                                            </View>
                                        </View>
                                    </View>
                                )
                            })}
                        </View>

                        <View style={[styles.borderWrapper, {paddingBottom: 10}]}>
                            <TouchableWithoutFeedback onPress={() => showOnOffMoreInfo()}>
                                <View style={[styles.moreInfoRow, {paddingTop: 5, paddingBottom: 10}]}>
                                    <View style={styles.moreInfoRowLeft}>
                                        <Text style={styles.moreInfo}>Thông tin thêm</Text>
                                        <Text style={styles.moreInfoNote}>Mô tả, phân loại sản phẩm</Text>
                                    </View>
                                    <View style={styles.moreInfoRowRight}>
                                        <Feather
                                            name={switchShowMoreInfo ? 'chevron-up' : 'chevron-down'}
                                            size={20}
                                            color='#808080'
                                        />
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                            {switchShowMoreInfo ? 
                                <View>
                                    <TouchableHighlight 
                                        onPress={() => openModalProductType()}
                                        underlayColor='lavender'
                                    >
                                        <View style={[styles.moreInfoRow, {paddingVertical: 12}]}>
                                            <View style={styles.moreInfoRowLeft}>
                                                <Text style={styles.rowTitle}>Loại sản phẩm</Text>
                                                <Text 
                                                    style={styles.rowText}
                                                    numberOfLines={1}
                                                >
                                                    --
                                                </Text>
                                            </View>
                                            <View style={styles.moreInfoRowRight}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='#808080'
                                                />
                                            </View>
                                            {/* MODAL PRODUCT TYPE */}
                                                <ModalCustomList
                                                    headerTitle='Loại sản phẩm'
                                                    modalCustomListVisible={modalProductTypeVisible}
                                                    setModalCustomListVisible={setModalProductTypeVisible}
                                                />
                                            {/* MODAL PRODUCT TYPE */}
                                        </View>
                                    </TouchableHighlight>

                                    <TouchableHighlight 
                                        onPress={() => openModalProductBrand()}
                                        underlayColor='lavender'
                                    >
                                        <View style={[styles.moreInfoRow, {paddingVertical: 12}]}>
                                            <View style={styles.moreInfoRowLeft}>
                                                <Text style={styles.rowTitle}>Nhãn hiệu</Text>
                                                <Text 
                                                    style={styles.rowText}
                                                    numberOfLines={1}
                                                >
                                                    --
                                                </Text>
                                            </View>
                                            <View style={styles.moreInfoRowRight}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='#808080'
                                                />
                                            </View>
                                            {/* MODAL PRODUCT BRAND */}
                                                <ModalCustomList
                                                     headerTitle='Nhãn hiệu'
                                                     modalCustomListVisible={modalProductBrandVisible}
                                                     setModalCustomListVisible={setModalProductBrandVisible}
                                                />
                                            {/* MODAL PRODUCT BRAND */}
                                        </View>
                                    </TouchableHighlight>

                                    <TouchableHighlight 
                                        onPress={() => openModalDescribeProduct()}
                                        underlayColor='lavender'
                                    >
                                        <View style={[styles.moreInfoRow, {paddingVertical: 12}]}>
                                            <View style={styles.moreInfoRowLeft}>
                                                <Text style={styles.rowTitle}>Mô tả</Text>
                                                <Text 
                                                    style={styles.rowText}
                                                    numberOfLines={1}
                                                >
                                                    --
                                                </Text>
                                            </View>
                                            <View style={styles.moreInfoRowRight}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='#808080'
                                                />
                                            </View>
                                            {/* MODAL DESCRIBE PRODUCT */}
                                                <ModalDescribeProduct
                                                     modalDescribeProductVisible={modalDescribeProductVisible}
                                                     setModalDescribeProductVisible={setModalDescribeProductVisible}
                                                     description={description}
                                                     setDescripion={setDescripion}
                                                />
                                            {/* MODAL DESCRIBE PRODUCT */}
                                        </View>
                                    </TouchableHighlight>
                                </View> : null
                            }    
                        </View>

                        {/* <View style={[styles.borderWrapper, {flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingBottom: 10}]}>
                            <View style={[styles.left, {paddingLeft: 5}]}>
                                <Text style={styles.canSellTitle}>Cho phép bán</Text>
                            </View>
                            <View style={[styles.right, {paddingRight: 10}]}>
                                <Switch
                                    onValueChange={toggleSwitch}
                                    value={canSell}
                                    style={styles.switch}
                                />
                            </View>
                        </View> */}

                        <View style={styles.btnGroup}>
                            <TouchableOpacity 
                                style={styles.cancelBtn}
                                onPress={() => closeModalCreateProduct()}
                            >
                                <Text style={styles.cancelTitle}>Hủy</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={styles.createNewBtn}
                                onPress={() => hasAttributes ? goToNextStep() : createProduct()}
                            >
                                <Text style={styles.createNewTitle}>{hasAttributes ? 'Tiếp tục' : 'Lưu'}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    {/* MODAL SHOW PRODUCT VERSION */}
                        <ModalShowProductVersion
                            modalShowProductVersionVisible={modalShowProductVersionVisible}
                            setModalShowProductVersionVisible={setModalShowProductVersionVisible}
                            productObj={productObj}
                            userToken={props.userToken}
                            onRefresh={props.onRefresh}
                        />
                    {/* MODAL SHOW PRODUCT VERSION */}
                </View>
            </Modal> 
        </SafeAreaView>
    );
};

