import { StyleSheet } from 'react-native';
import { width, height } from '../../Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        alignItems: 'center',
        width: 40
    },
    headerRight: {
        alignItems: 'center',
        width: 40
    },
    headerCenter: {
        width: width/1.08 - 80,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 40
    },
    borderWrapper: {
        width: width/1.05,
        borderRadius: 5,
        backgroundColor: '#fff',
        marginBottom: 15,
        paddingTop: 10,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    productImageWrapper: {
        width: width,
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    productImage: {
        width: 190,
        height: 210,
    },
    accountName: {
        marginBottom: 15
    },
    title: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
    },
    require: {
        color: 'red'
    },
    separate: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 30
    },
    line: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        width: 70
    },
    separateTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        marginHorizontal: 10,
        color: 'gray',
    },
    btnGroup: {
        flexDirection: 'row',
        marginTop: 15
    },
    cancelBtn: {
        width: 100,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    cancelTitle: {
        fontSize: 18,
        color: 'gray',
        marginVertical: 10,
    },
    createNewBtn: {
        width: width/1.05 - 120,
        borderRadius: 10,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor: '#1C3FAA',
        marginLeft: 20,
    },
    createNewTitle: {
        fontSize: 18,
        color: '#fff',
    },
    propertiesTitleWrapper: {
        flexDirection: 'row',
        marginBottom: 10
    },
    propertiesTitle: {
        fontWeight: 'bold',
        fontSize: 18,
        color: 'gray',
    },
    rowLeft: {
        width: width/1.05 - 50,
        paddingLeft: 10
    },
    rowRight: {
        alignItems: 'center',
        width: 50,
    }, 
    propertiesShow: {
        marginBottom: 15,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingVertical: 5,
    },
    propertyNameShow: {
        marginBottom: 3,
        fontWeight: 'bold',
        marginLeft: 15,
        fontSize: 17
    },
    propertyValueShow: {
        marginBottom: 5,
        color: 'gray',
        marginLeft: 10,
    },
    row: {
        marginBottom: 15
    },
    left: {
        width: width/2.2 - 5
    },
    right: {
        marginLeft: 10,
        width: width/2.2 - 5
    },
    deletePropertyIcon: {
        marginBottom: 5
    },
    deleteImgWrapper: {
        alignItems: 'flex-end',
        width: width - 30
    },
    deleteImgBtn: {
        width: 80,
        paddingVertical: 3,
        borderRadius: 10,
        backgroundColor: '#FF9999',
        alignItems: 'center'
    },
    deleteImgText: {
        color: '#404040',
        fontSize: 13
    },
    addImage: {
        width: 60,
        height: 60,
        borderRadius: 5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'silver',
        borderWidth: 0.5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    productImageScroll: {
        minWidth: width - 90
    },
    productImage: {
        width: 60,
        height: 60,
        borderRadius: 5, 
    },
    productImageElement: {
        marginLeft: 10,
    },
    deleteImg: {
        position: 'absolute',
        width: 60,
        height: 60,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 100, 150, 0.5)',
    },
    redline: {
        width: 30,
        height: 10,
        borderRadius: 3,
        backgroundColor: 'red'
    },
    numberBorder: {
        height: 45,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
        backgroundColor: '#fff',
        width: width/2.2 - 5,
        justifyContent: 'center'
    },
    weightBorder: {
        height: 45,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
        backgroundColor: '#fff',
        width: (width/1.1)*2/3,
        justifyContent: 'center',
    },
    number: {
        marginLeft: 15,
        color: 'gray'
    },
    moreInfoRow: {
        width: width/1.05 - 30,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    moreInfo: {
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 3
    },
    moreInfoNote: {
        fontSize: 13,
        color: 'gray'
    },
    moreInfoRowLeft: {
        width: width/1.05 - 60
    },
    moreInfoRowRight: {
        width: 30,
        alignItems: 'center'
    },
    rowTitle: {
        fontSize: 13,
        color: 'gray',
        marginBottom: 3
    },
    rowText: {
        fontSize: 15
    },
    switch: {
        transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }]
    },
    taxableTitle: {
        fontSize: 16
    },
    weightUnitPicker: {
        width: (width/1.1)*1/3 - 10,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'whitesmoke',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'silver',
        borderWidth: 0.5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,  
    }
})