import React, { useState } from 'react';
import { View, TextInput, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { styles } from './css'
import Icon from 'react-native-vector-icons/Ionicons';

export default function Input(props) {

    const [inputValue, setInputValue] = useState(props.value ? props.value : '');
    const [focus, setFocus] = useState(false);
    const [showPass, setShowPass] = useState(props.hideShowText ? true : false);

    const style = StyleSheet.create({
        focus: {
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 7,
            },
            shadowOpacity: 0.43,
            shadowRadius: 9.51,
            elevation: 15,
        }
    })

    const onChangeText = (text) => {
        setInputValue(text)
        // props.getValue(props.name, text)
        props.setValue ? props.setValue(text) : null
    } 

    const clearInput = () => {
        setInputValue('')
        props.setValue('')
    }

    const showPassWord = () => {
        setShowPass(!showPass)
    }

    return (
        <View style={[
            styles.borderWrapper, 
            focus ? style.focus : null, 
            {width: props.borderWidth}, 
            props.editable == false ? {backgroundColor: 'lavender'} : {backgroundColor: '#fff'}
        ]}>
            <TextInput 
                style={[
                    styles.textInput, 
                    {width: props.inputWidth}, 
                    props.paddingRight ? {paddingRight: props.paddingRight} : null
                ]}
                placeholder={props.placeholder}
                onChangeText={text => onChangeText(text)}
                value={inputValue}
                underlineColorAndroid="transparent"
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                keyboardType={props.keyboardType ? props.keyboardType : 'default'}
                secureTextEntry={showPass}
                editable={props.editable}
            />
            {inputValue && props.editable == true ? 
                <TouchableWithoutFeedback
                    onPress={() => clearInput()}
                >
                    <Icon
                        name='close-circle'
                        size={20}
                        color='silver'
                        style={[styles.clearInput, {left: props.iconDeleteLeft}]}
                    />
                </TouchableWithoutFeedback> : null   
            }
            {props.type == 'Password' && props.type ? 
                <TouchableWithoutFeedback onPress={() => showPassWord()}>
                    <Icon 
                        name={showPass ? 'ios-eye' : 'ios-eye-off'}
                        size={23}
                        color='silver' 
                        style={styles.btnEye}
                    />
                </TouchableWithoutFeedback> : null
            }
        </View>
        
    );
};

