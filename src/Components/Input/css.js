import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({ 
    container: {},
    borderWrapper: {
        height: 45,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: 'gray',
        alignItems: 'center',
    },
    textInput: {
        
    },
    clearInput: {
        position: 'absolute',
        top: 10
    },
    btnEye: {
        position: 'absolute',
        left: width/1.15 - 20,
        top: 8
    }
})
