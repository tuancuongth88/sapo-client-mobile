import React, { useState } from 'react';
import { View, Text } from 'react-native';
import { width } from '../../Components/Dimensions/Dimensions';
import styles from './css';
import {Picker} from '@react-native-community/picker';

export default function SingleSelect(props) {
    const [selectedValue, setSelectedValue] = useState(props.items[0].label);

    return (
        <View style={styles.container}>
            <Picker
                selectedValue={selectedValue}
                style={{ height: 30, width: width/1.1 }}
                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
            >   
                {props.items.map((item, key) => {
                    return <Picker.Item key={key} label={item.label} value={item.value}/>
                })}
            </Picker>
        </View>
    )
}