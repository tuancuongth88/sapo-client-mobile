import { StyleSheet } from 'react-native';
import { width } from '../../Helpers/Functions';

const styles = StyleSheet.create({ 
    container: {
        backgroundColor: '#fff',
        width: width/1.1
    },

})

export default styles;