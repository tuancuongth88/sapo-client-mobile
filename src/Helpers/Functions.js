import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export const loadingScreen = () => {
    return(
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color='gray' />
        </View>
    )
}

export const fetchToken = async() => {
    try{
        const token = await AsyncStorage.getItem('userToken');
        return token;
    } catch(error) {
        console.log(error)
    }  
}