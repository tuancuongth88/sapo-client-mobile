import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard from '../Screens/Dashboard/Dashboard';
import Notifications from '../Screens/Notifications/Notifications';

const DashboardStack = createStackNavigator();

export default function DashboardNavigation() {
    return (
        <DashboardStack.Navigator
            initialRouteName='Dashboard'
        >
            <DashboardStack.Screen
                name="Dashboard"
                component={Dashboard}
                options={{ headerShown: false }}        
            />
            <DashboardStack.Screen
                name="Notifications"
                component={Notifications}
                options={{ headerShown: false }}        
            />
        </DashboardStack.Navigator>
    );
};
