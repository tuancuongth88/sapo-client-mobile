import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LiveStream from '../Screens/LiveStream/LiveStream';

const LiveStreamStack = createStackNavigator();

export default function LiveStreamNavigation() {
    return (
        <LiveStreamStack.Navigator
            initialRouteName='LiveStream'
        >
            <LiveStreamStack.Screen
                name="LiveStream"
                component={LiveStream}
                options={{ headerShown: false }}        
            />
        </LiveStreamStack.Navigator>
    );
};
