import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../Screens/Login/Login';
import Register from '../Screens/Register/Register';
import ForgotPassword from '../Screens/ForgotPassword/ForgotPassword';

const LoginStack = createStackNavigator();

export default function LoginNavigation() {
    return (
        <LoginStack.Navigator
            initialRouteName='Login'
        >
            <LoginStack.Screen
                name="Login"
                component={Login}
                options={{ headerShown: false }}        
            />
            <LoginStack.Screen
                name="Register"
                component={Register}
                options={{ headerShown: false }}        
            />
            <LoginStack.Screen
                name="ForgotPassword"
                component={ForgotPassword}
                options={{ headerShown: false }}        
            />
        </LoginStack.Navigator>
    );
};
