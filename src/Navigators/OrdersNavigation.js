import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Orders from '../Screens/Orders/Orders';
import OrderList from '../Screens/OrderList/OrderList';
import Transportation from '../Screens/Transportation/Transportation';
import DeliveryManagement from '../Screens/DeliveryManagement/DeliveryManagement';
import OrderDetail from '../Screens/OrderDetail/OrderDetail';
import DeliveryDetail from '../Screens/DeliveryDetail/DeliveryDetail';
import DeliveryPartner from '../Screens/DeliveryPartner/DeliveryPartner';
import TransportOverview from '../Screens/TransportOverview/TransportOverview';

const OrdersStack = createStackNavigator();

export default function OrdersNavigation() {
    return (
        <OrdersStack.Navigator
            initialRouteName='Orders'
        >
            <OrdersStack.Screen
                name="Orders"
                component={Orders}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="OrderList"
                component={OrderList}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="OrderDetail"
                component={OrderDetail}
                options={{ headerShown: false }}    
            />
            <OrdersStack.Screen
                name="Transportation"
                component={Transportation}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="DeliveryManagement"
                component={DeliveryManagement}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="DeliveryDetail"
                component={DeliveryDetail}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="DeliveryPartner"
                component={DeliveryPartner}
                options={{ headerShown: false }}        
            />
            <OrdersStack.Screen
                name="TransportOverview"
                component={TransportOverview}
                options={{ headerShown: false }}        
            />
        </OrdersStack.Navigator>
    );
};
