import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MyAccount from '../Screens/MyAccount/MyAccount';
import AccountInformation from '../Screens/AccountInformation/AccountInformation';
import HelpCenter from '../Screens/HelpCenter/HelpCenter';
import AccountManagement from '../Screens/AccountManagement/AccountManagement';
import ChangePassword from '../Screens/ChangePassword/ChangePassword';
import FacebookAccountManagement from '../Screens/FacebookAccountManagement/FacebookAccountManagement';
import CustomerList from '../Screens/CustomerList/CustomerList';
import CustomerDetail from '../Screens/CustomerDetail/CustomerDetail';
import CustomerInfoDetail from '../Screens/CustomerInfoDetail/CustomerInfoDetail';
import AddressList from '../Screens/AddressList/AddressList';
import ContactPeopleList from '../Screens/ContactPeopleList/ContactPeopleList';

const MyAccountStack = createStackNavigator();

export default function MyAccountNavigation() {
    return (
        <MyAccountStack.Navigator
            initialRouteName='MyAccount'
        >
            <MyAccountStack.Screen
                name="MyAccount"
                component={MyAccount}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="AccountInformation"
                component={AccountInformation}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="HelpCenter"
                component={HelpCenter}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="AccountManagement"
                component={AccountManagement}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="ChangePassword"
                component={ChangePassword}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="FacebookAccountManagement"
                component={FacebookAccountManagement}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="CustomerList"
                component={CustomerList}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="CustomerDetail"
                component={CustomerDetail}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="CustomerInfoDetail"
                component={CustomerInfoDetail}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="AddressList"
                component={AddressList}
                options={{ headerShown: false }}        
            />
            <MyAccountStack.Screen
                name="ContactPeopleList"
                component={ContactPeopleList}
                options={{ headerShown: false }}        
            />
        </MyAccountStack.Navigator>
    );
};
