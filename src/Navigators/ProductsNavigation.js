import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Products from '../Screens/Products/Products';
import ProductList from '../Screens/ProductList/ProductList';
import IncomeStatement from '../Screens/IncomeStatement/IncomeStatement';
import ProductDetail from '../Screens/ProductDetail/ProductDetail';

const ProductsStack = createStackNavigator();

export default function ProductsNavigation() {
    return (
        <ProductsStack.Navigator
            initialRouteName='Products'
        >
            <ProductsStack.Screen
                name="Products"
                component={Products}
                options={{ headerShown: false }}        
            />
            <ProductsStack.Screen
                name="ProductList"
                component={ProductList}
                options={{ headerShown: false }}        
            />
            <ProductsStack.Screen
                name="IncomeStatement"
                component={IncomeStatement}
                options={{ headerShown: false }}        
            />
            <ProductsStack.Screen
                name="ProductDetail"
                component={ProductDetail}
                options={{ headerShown: false }}        
            />
        </ProductsStack.Navigator>
    );
};
