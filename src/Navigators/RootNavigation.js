import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginNavigation from './LoginNavigation';
import BottomNavigation from './BottomNavigation';

const RootStack = createStackNavigator();

export default function RootNavigation() {
    return (
        <NavigationContainer>
            <RootStack.Navigator
                initialRouteName='LoginNavigation'
            >
                <RootStack.Screen
                    name="LoginNavigation"
                    component={LoginNavigation}
                    options={{ headerShown: false }}
                    
                />
                <RootStack.Screen
                    name="BottomNavigation"
                    component={BottomNavigation}
                    options={{ headerShown: false }}
                    
                />
            </RootStack.Navigator>
        </NavigationContainer>
    );
};
