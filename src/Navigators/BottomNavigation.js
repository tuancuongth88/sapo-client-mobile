import 'react-native-gesture-handler';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MainButton from '../Components/MainButton/MainButton';
import ProductsNavigation from './ProductsNavigation';
import LiveStreamNavigation from './LiveStreamNavigation';
import DashboardNavigation from './DashboardNavigation';
import OrdersNavigation from './OrdersNavigation';
import MyAccountNavigation from './MyAccountNavigation';
import Feather from 'react-native-vector-icons/Feather';

const Tab = createBottomTabNavigator();

function BottomNavigation(props) {
    return (
        <Tab.Navigator
            initialRouteName= "ProductsNavigation" 
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name == 'DashboardNavigation') {
                        iconName = focused ? 'activity' : 'activity';
                    } else if (route.name == 'ProductsNavigation') {
                        iconName = focused ? 'box' : 'box';
                    } else if (route.name == 'LiveStreamNavigation') {
                        return <MainButton focused={focused}/>
                    } else if (route.name == 'OrdersNavigation') {
                        iconName = focused ? 'shopping-bag' : 'shopping-bag';
                    } else if (route.name == 'MyAccountNavigation') { 
                        iconName = focused ? 'user' : 'user';
                    }
                    return  <Feather name={iconName} size={size} color={color} />         
                }, 
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
                style: { height: 55 },
                labelStyle: { fontSize: 10, marginBottom: 5 },
                keyboardHidesTabBar: true
            }}
            lazy={false}
        >
            <Tab.Screen name="DashboardNavigation" component={DashboardNavigation} options={{ title: 'Dashboard' }}/>
            <Tab.Screen name="ProductsNavigation" component={ProductsNavigation} options={{ title: 'Sản phẩm' }}/>
            <Tab.Screen name="LiveStreamNavigation" component={LiveStreamNavigation} options={{ title: '' }} />
            <Tab.Screen name="OrdersNavigation" component={OrdersNavigation} options={{ title: 'Đơn hàng' }} />
            <Tab.Screen name="MyAccountNavigation" component={MyAccountNavigation} options={{ title: 'Tài khoản' }} />
        </Tab.Navigator>
    );
}

export default BottomNavigation;