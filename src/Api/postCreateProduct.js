import Config from "react-native-config";

const URL = Config.API_URL + 'api/products';

const createFormData = (files, body) => {
    const data = new FormData();
    
    if(files != null) {
        files.forEach((file) => {
            const fileObj = {
                name: file.name,
                type: file.type,
                uri:
                    Platform.OS == "android" ? file.uri : file.uri.replace("file://", "")
            };
            data.append('file', fileObj)
        })
    }
     
    Object.keys(body).forEach(key => {
        data.append(key, body[key]);
    });
    return data;
};

const postCreateProduct = (
        token, 
        productObj,  
        files ) => (
        
    fetch(URL,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Accept: 'application/json',
                Authorization: token
            },
            body: createFormData(
                files, 
                { 
                    title: productObj.title,
                    sku: productObj.sku,
                    unit: productObj.unit,
                    weight: Number(productObj.weight),
                    weight_unit: productObj.weight_unit,
                    description: productObj.description,
                    retail_price: Number(productObj.retail_price),
                    whole_sale_price: Number(productObj.whole_sale_price),
                    import_price: Number(productObj.import_price),
                    inventory: Number(productObj.inventory),
                    has_attributes: productObj.has_attributes,
                    category_id: productObj.category_id,
                    branch_id: productObj.branch_id,
                    taxable: productObj.taxable
                })
        })
    .then(res => {
        return res;
    })
    .catch(err => console.log(err))
);

export default postCreateProduct;
