import Config from "react-native-config";

const URL = Config.API_URL + 'api/auth/remind-password';

const postRemindPassword = (email) => (
    fetch(URL + '?email=' + email,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
        })
    .then(res => {
        return res
    })
    .catch(err => console.log(err))
);

export default postRemindPassword;
