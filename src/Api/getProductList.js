import Config from "react-native-config";

const URL = Config.API_URL + 'api/products';

const getProductList = (userToken, page) => (
    fetch(URL + '?page=' + page,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: userToken
            },
        })
    .then(res => {
        return res
    })
    .catch(err => console.log(err))
);

export default getProductList;
