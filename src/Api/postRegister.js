import Config from "react-native-config";

const URL = Config.API_URL + 'api/auth/register';

const postRegister = (
    name, 
    phone, 
    email, 
    shop_name,
    province_id,
    password,
    password_confirmation ) => (
        
    fetch(URL + '?name=' + name + '&phone=' + phone + '&email=' + email + '&shop_name=' + shop_name + '&province_id=' + province_id + '&password=' + password + '&password_confirmation=' + password_confirmation,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
        })
    .then(res => {
        return res
    })
    .catch(err => console.log(err))
);

export default postRegister;
