import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';

export default function Orders(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const navigateToOrderListScreen = () => {
        props.navigation.navigate('OrderList')
    }

    const navigateToDeliveryManagementScreen = () => {
        props.navigation.navigate('DeliveryManagement')
    }

    const navigateToTransportationScreen = () => {
        props.navigation.navigate('Transportation')
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <Text style={styles.headerTitle}>Đơn hàng</Text>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <Feather
                                name='gift'
                                size={160}
                                color='#1C3FAA'
                                style={{ marginBottom: 30 }}
                            />

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToOrderListScreen()}    
                            >
                                <Feather
                                    name='file-text'
                                    size={28}
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Danh sách đơn hàng</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToDeliveryManagementScreen()}    
                            >
                                <Feather
                                    name='archive'
                                    size={28}
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Quản lý giao hàng</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToTransportationScreen()}    
                            >
                                <Feather
                                    name='truck'
                                    size={28}
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Vận chuyển</Text>
                            </TouchableOpacity>

                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

