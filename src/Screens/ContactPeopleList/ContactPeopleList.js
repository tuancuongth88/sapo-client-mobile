import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import ModalContactPeopleDetail from '../../Components/Modal/ModalContactPeopleDetail/ModalContactPeopleDetail';
import ModalAddContactPeople from '../../Components/Modal/ModalAddContactPeople/ModalAddContactPeople';

export default function ContactPeopleList(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalAddContactPeopleVisible, setModalAddContactPeopleVisible] = useState(false);
    const [modalContactPeopleDetailVisible, setModalContactPeopleDetailVisible] = useState(false);
    const [contactPeopleList, setContactPeopleList] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [chosenContactPeople, setChosenContactPeople] = useState({});

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalAddContactPeople = () => {
        setModalAddContactPeopleVisible(true)
    }

    const openModalContactPeopleDetail = (item) => {
        setChosenContactPeople(item)
        setModalContactPeopleDetailVisible(true) 
    }

    const renderItem = ({item}) => {
        return (
            <TouchableHighlight
                onPress={() => openModalContactPeopleDetail(item)}
                underlayColor='lavender'
            >
                <View style={styles.element}>
                    <View style={styles.left}>
                        <View style={styles.dot}></View>
                    </View>
                    <View style={styles.right}>
                        <Text 
                            style={styles.contactPeopleName}
                            numberOfLines={1}
                        >
                            {item.contactPeopleName}
                        </Text>
                        <Text 
                            style={styles.phone}
                            numberOfLines={1}
                        >
                            {item.phone}
                        </Text>
                        <Text 
                            style={styles.email}
                            numberOfLines={1}
                        >
                            {item.email}
                        </Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

    const onRefresh = () => {
        
    }

    const data = [
        {contactPeopleName: 'Thắng', email: 'abcdegfh@gmail.com', city: 'HN', phone: '0984563322'},
        {contactPeopleName: 'Long', email: '19894jaksdjhdb@gmail.com', phone: '0984563322'}
    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Người liên hệ</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => openModalAddContactPeople()}
                                >
                                    <Feather
                                        name='plus'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                                {/* MODAL ADD CONTACT PEOPLE */}
                                    <ModalAddContactPeople
                                        modalAddContactPeopleVisible={modalAddContactPeopleVisible}
                                        setModalAddContactPeopleVisible={setModalAddContactPeopleVisible}
                                    />
                                {/* MODAL ADD CONTACT PEOPLE */}
                            </View>
                        </View>

                        <View style={styles.body}>
                            <FlatList
                                contentContainerStyle={styles.addressList}
                                // ref={(ref) => { this.flatListRef = ref; }}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={data}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                            />
                        </View> 

                        {/* MODAL CONTACT PEOPLE DETAIL */}
                            <ModalContactPeopleDetail
                                modalContactPeopleDetailVisible={modalContactPeopleDetailVisible}
                                setModalContactPeopleDetailVisible={setModalContactPeopleDetailVisible}
                                contactPeople={chosenContactPeople}
                            />
                        {/* MODAL CONTACT PEOPLE DETAIL */} 
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

