import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';

export default function Products(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const navigateToProductListScreen = () => {
        props.navigation.navigate('ProductList')
    }

    const navigateToIncomeStatementScreen = () => {
        props.navigation.navigate('IncomeStatement')
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <Text style={styles.headerTitle}>Sản phẩm</Text>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <Feather
                                name='box'
                                size={180}
                                color='#1C3FAA'
                                style={{ marginBottom: 30 }}
                            />

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToProductListScreen()}    
                            >
                                <Feather
                                    name='list'
                                    size={28}
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Danh sách sản phẩm</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToIncomeStatementScreen()}    
                            >
                                <Feather
                                    name='clipboard'
                                    size={28}
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Báo cáo doanh thu</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>  
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

