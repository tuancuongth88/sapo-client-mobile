import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const SIZE = 140;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    body: {
        alignItems: 'center',
        paddingTop: 20
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        justifyContent: 'center'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    oldPassword: {
        marginBottom: 15
    },
    newPassword: {
        marginBottom: 15
    },
    confirmNewPassword: {
        marginBottom: 30
    },
    inputTitle: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 3
    },
    changePasswordBtn: {
        width: width/1.1,
        backgroundColor: '#1C3FAA',
        paddingVertical: 10,
        alignItems: 'center',
        marginBottom: 30,
        borderRadius: 10
    },
    changePasswordTitle: {
        color: '#fff',
        fontSize: 18
    }
})