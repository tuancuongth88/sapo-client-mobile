import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, ScrollView } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Components/Input/Input';
import { width, height } from '../../Components/Dimensions/Dimensions';

export default function ChangePassword(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const getValue = (name, value) => {
        
    }

    const changePassword = () => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <ScrollView 
                        contentContainerStyle={styles.container}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Thay đổi mật khẩu</Text>
                            </View>
                            <View style={styles.headerRight}>

                            </View>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.oldPassword}>
                                <Text style={styles.inputTitle}>Mật khẩu cũ</Text>
                                <Input
                                    placeholder='Điền mật khẩu cũ'
                                    name='oldPassword'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.newPassword}>
                                <Text style={styles.inputTitle}>Mật khẩu mới</Text>
                                <Input
                                    placeholder='Điền mật khẩu mới'
                                    name='newPassword'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.confirmNewPassword}>
                                <Text style={styles.inputTitle}>Nhập lại mật khẩu mới</Text>
                                <Input
                                    placeholder='Điền lại mật khẩu mới'
                                    name='confirmNewPassword'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                />
                            </View>

                            <TouchableWithoutFeedback
                                onPress={() => changePassword()}
                            >
                                <View style={styles.changePasswordBtn}>
                                    <Text style={styles.changePasswordTitle}>Đổi mật khẩu</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

