import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, ImageBackground, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import ModalAddNote from '../../Components/Modal/ModalAddNote/ModalAddNote';
import ModalOrderOption from '../../Components/Modal/ModalOrderOption/ModalOrderOption';
import ModalProductVersionDetail from '../../Components/Modal/ModalProductVersionDetail/ModalProductVersionDetail';

export default function OrderDetail(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalAddNoteVisible, setModalAddNoteVisible] = useState(false);
    const [note, setNote] = useState('');
    const [option, setOption] = useState('');
    const [modalOrderOptionVisible, setModalOrderOptionVisible] = useState(false);
    const [imgArr, setImgArr] = useState([]);
    const [modalProductVersionDetailVisible, setModalProductVersionDetailVisible] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalAddNote = () => {
        setModalAddNoteVisible(true)
    }

    const openModalOption = () => {
        setModalOrderOptionVisible(true)
    }

    const goToDeliveryDetailScreen = () => {
        props.navigation.navigate('DeliveryDetail')
    }

    const openModalProductVersionDetail = () => {
        setModalProductVersionDetailVisible(true)
    }

    const goToCustomerDetailScreen = () => {
        props.navigation.navigate('CustomerDetail')
    }

    const exportByBtn = () => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Chi tiết đơn hàng</Text>
                            </View>
                            <View style={styles.headerRight}>

                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={styles.orderStatusWrapper}>
                                <View style={styles.row}>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.title}>Đặt hàng</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.title}>Duyệt</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.title}>Đóng gói</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.title}>Xuất kho</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.title}>Hoàn thành</Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.rowElement}>
                                        <View style={styles.circleActive}>
                                            <Feather
                                                name='check'
                                                size={10}
                                                color='#fff'
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <View style={styles.circleActive}>
                                            <Feather
                                                name='check'
                                                size={10}
                                                color='#fff'
                                            />               
                                        </View>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <View style={styles.circleActive}>
                                            <Feather
                                                name='check'
                                                size={10}
                                                color='#fff'
                                            />               
                                        </View>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <View style={styles.circleDeactive}>
                                            <Feather
                                                name='check'
                                                size={10}
                                                color='#fff'
                                            />               
                                        </View>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <View style={styles.circleDeactive}>
                                            <Feather
                                                name='check'
                                                size={10}
                                                color='#fff'
                                            />               
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.orderStatusTime}>10:48</Text>
                                        <Text style={styles.orderStatusDate}>28/10/2020</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.orderStatusTime}>10:48</Text>
                                        <Text style={styles.orderStatusDate}>28/10/2020</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.orderStatusTime}>10:48</Text>
                                        <Text style={styles.orderStatusDate}>28/10/2020</Text>
                                    </View>
                                    <View style={styles.rowElement}>
                                        <Text style={styles.orderStatusTime}>10:48</Text>
                                        <Text style={styles.orderStatusDate}>28/10/2020</Text>
                                    </View>
                                    {/* <View style={styles.rowElement}>
                                        <Text style={styles.orderStatusTime}>10:48</Text>
                                        <Text style={styles.orderStatusDate}>28/10/2020</Text>
                                    </View> */}
                                </View>
                            </View>
                            
                            <ImageBackground
                                style={styles.img}
                                source={require('../../Assets/order_backgroud.jpg')}
                            >   
                                <View style={styles.inside}>
                                    <Text style={styles.paymentFee}>100,000</Text>
                                    <Text style={styles.sellBy}>Bán bởi:  nguyen chien thang</Text>
                                    <Text style={styles.branch}>Chi nhánh:  Chi nhánh mặc định</Text>
                                    <Text style={styles.source}>Nguồn:  Web</Text>
                                    <View style={styles.insideRow}>
                                        <View style={styles.dotDeactive}></View>
                                        <Text style={styles.insideRowTextDeactive}>Thanh toán toàn bộ</Text>
                                    </View>
                                    <View style={styles.insideRow}>
                                        <View style={styles.dotActive}></View>
                                        <Text style={styles.insideRowTextActive}>Chờ lấy hàng</Text>
                                    </View>
                                </View>
                            </ImageBackground>

                            <TouchableHighlight
                                onPress={() => goToCustomerDetailScreen()}
                                underlayColor='lavender'
                            >
                                <View style={styles.customerWrapper}>
                                    <View style={styles.customerInside}>
                                        <Image
                                            style={styles.avatar}
                                            source={require('../../Assets/avatar.png')}
                                        />
                                        <View style={styles.customerInfo}>
                                            <Text style={styles.customerName}>Thắng - <Text style={styles.customerPhone}>0912637892</Text></Text>
                                            <Text style={styles.customerAddress}>19a đường Bưởi, Phưởng Bưởi, Quận Tây Hồ, Hà Nội</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <View style={[styles.borderWrapper, {paddingVertical: 10, marginBottom: 10}]}>
                                <View style={styles.borderInside}>
                                    <Text style={styles.productHeaderTitle}>Sản phẩm (1)</Text>
                                </View>
                                <TouchableHighlight
                                    onPress={() => openModalProductVersionDetail()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.productInfoWrapper}>
                                        <View style={styles.borderInside}>
                                            <Image
                                                style={styles.productImg}
                                                source={require('../../Assets/no_image.png')}
                                            />
                                            <View style={styles.productInfo}>
                                                <Text 
                                                    style={styles.productName}
                                                    numberOfLines={1}    
                                                >
                                                    áo phông - xxl - tím ádasdasdadadasdassadasdadss
                                                </Text>
                                                <Text 
                                                    style={styles.productCode}
                                                    numberOfLines={1} 
                                                >
                                                    SKU: AP001-xx-t
                                                </Text>
                                                <Text 
                                                    style={styles.productPrice}
                                                    numberOfLines={1} 
                                                >
                                                    100,000
                                                </Text>
                                            </View>
                                            <View style={styles.total}>
                                                <Text 
                                                    style={styles.totalPrice}
                                                    numberOfLines={1}
                                                >
                                                    100,000,00000
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>

                            <View style={[styles.borderWrapper, {paddingTop: 15, paddingBottom: 10, marginBottom: 10}]}>
                                <View style={styles.borderInside}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftTitle}>Tổng tiền hàng</Text>
                                        <Text style={styles.leftTitle}>Thuế</Text>
                                        <Text style={styles.leftTitle}>Chiết khấu</Text>
                                        <Text style={styles.leftTitle}>Phí giao hàng</Text>
                                        <Text style={[styles.leftTitle, {fontWeight: 'bold'}]}>Khách hàng phải trả</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            100,000
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={[styles.rightNumber, {fontWeight: 'bold'}]}
                                            numberOfLines={1}
                                        >
                                            100,000
                                        </Text>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.borderWrapper, {paddingVertical: 10, marginBottom: 10}]}>
                                <View style={styles.top}>
                                    <View style={styles.borderInside}>
                                        <Feather
                                            name='check-circle'
                                            size={20}
                                            color='blue'
                                        />
                                        <Text style={styles.paymentHeaderTitle}>Thanh lý toàn bộ</Text>
                                    </View>
                                </View>
                                <View style={styles.bottom}>
                                    <View style={styles.borderInside}>
                                        <View style={[styles.left, {paddingLeft: 40, paddingTop: 10}]}>
                                            <Text style={styles.text}>Tiền mặt</Text>
                                            <Text style={styles.note}>28/10/2020, 10h:48</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text style={styles.text}>100,000</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.borderWrapper, {paddingTop: 10, marginBottom: 10}]}>
                                <View style={styles.top}>
                                    <View style={styles.borderInside}>
                                        <Feather
                                            name='truck'
                                            size={20}
                                            color='blue'
                                        />
                                        <Text style={styles.paymentHeaderTitle}>Giao hàng</Text>
                                    </View>
                                </View>
                                <TouchableHighlight
                                    onPress={() => goToDeliveryDetailScreen()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.bottom}>
                                        <View style={styles.borderInside}>
                                            <View style={[styles.left, {paddingLeft: 40, paddingVertical: 5}]}>
                                                <Text style={styles.text}>FUN0001</Text>
                                                <Text style={styles.note}>Sơn</Text>
                                            </View>
                                            <View style={styles.rightRow}>
                                                <Text style={styles.deliveryStatus}>Chờ lấy hàng</Text>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            
                            <TouchableHighlight
                                onPress={() => openModalAddNote()}
                                underlayColor='lavender'
                            >
                                <View style={[styles.borderWrapper, {paddingVertical: 10}]}>
                                    <View style={styles.borderInside}>
                                        <View style={styles.leftNote}>
                                            {note == '' ? 
                                                <Text style={[styles.text, {color: 'blue'}]}>Thêm ghi chú</Text>
                                                    :
                                                <View>
                                                    <Text style={styles.noteTitle}>Ghi chú</Text>
                                                    <Text style={styles.addNote}>{note}</Text>
                                                </View>
                                            }
                                        </View>
                                        <View style={styles.rightNote}>
                                            <Feather
                                                name='file-text'
                                                size={20}
                                                color='blue'
                                            />
                                        </View>
                                    </View>
                                </View>
                            </TouchableHighlight>
                            {/* MODAL ADD NOTE */}
                                <ModalAddNote
                                    modalAddNoteVisible={modalAddNoteVisible}
                                    setModalAddNoteVisible={setModalAddNoteVisible}
                                    note={note}
                                    setNote={setNote}
                                />
                            {/* MODAL ADD NOTE */}
                        </ScrollView>
                        <View style={styles.bottom}>
                            <View style={styles.exportBtnWrapper}>
                                <View style={styles.exportBtnGroup}>
                                    <TouchableOpacity
                                        style={styles.exportBtn}
                                        onPress={() => exportByBtn()}
                                    >
                                        <Text style={styles.exportBtnTitle}>
                                            {option == '' ? 'Xuất kho' : option} 
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.optionBtn}
                                        onPress={() => openModalOption()}
                                    >
                                        <Feather
                                            name='more-horizontal'
                                            size={25}
                                            color='gray'
                                        />
                                    </TouchableOpacity>
                                    {/* MODAL ORDER OPTION */}
                                        <ModalOrderOption
                                            modalOrderOptionVisible={modalOrderOptionVisible}
                                            setModalOrderOptionVisible={setModalOrderOptionVisible}
                                            option={option}
                                            setOption={setOption}
                                        />
                                    {/* MODAL ORDER OPTION */}
                                </View>
                            </View>
                        </View>
                        {/* MODAL PRODUCT VERSION DETAIL */}
                            <ModalProductVersionDetail
                                modalProductVersionDetailVisible={modalProductVersionDetailVisible}
                                setModalProductVersionDetailVisible={setModalProductVersionDetailVisible}
                                imgArr={imgArr}
                                setImgArr={setImgArr}
                            />
                        {/* MODAL PRODUCT VERSION DETAIL */}
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

