import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const SIZE = 15;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#EAEAF7',
        flex: 1
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        justifyContent: 'center'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        paddingBottom: 30
    },
    row: {
        flexDirection: 'row',
        width: width/1.06,
        alignItems: 'center'
    },
    rowElement: {
        width: (width/1.06)/5,
        paddingVertical: 5,
        alignItems: 'center'
    },
    circleActive: {
        width: SIZE ,
        height: SIZE,
        borderRadius: SIZE,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1C3FAA',
    },
    circleDeactive: {
        width: SIZE,
        height: SIZE,
        borderRadius: SIZE,
        borderWidth: 1.5,
        borderColor: 'gray'
    },
    orderStatusTime: {
        fontSize: 11
    },
    orderStatusDate: {
        fontSize: 11
    },
    title: {
        fontSize: 13
    },
    orderStatusWrapper: {
        paddingVertical: 5,
        width: width,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    img: {
        width: width,
        borderColor: 'gray',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        alignItems: 'center',
        paddingVertical: 10
    },
    inside: {
        width: width/1.1
    },
    paymentFee: {
        fontSize: 40,
        fontWeight: 'bold',
        marginBottom: 5
    },
    insideRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    dotActive: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'green'
    },
    insideRowTextActive: {
        fontSize: 13,
        marginLeft: 10,
        color: 'green'
    },
    dotDeactive: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'gray'
    },
    insideRowTextDeactive: {
        fontSize: 13,
        marginLeft: 10,
        color: 'gray'
    },
    source: {
        marginBottom: 10
    },
    customerWrapper: {
        width: width,
        backgroundColor: '#fff',
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderColor: 'gray',
        alignItems: 'center',
        marginBottom: 10
    },
    customerInside: {
        flexDirection: 'row',
        width: width/1.1,
        alignItems: 'center',
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    customerInfo: {
        width: width/1.1 - 70,
        marginLeft: 20,
    },
    customerName: {
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 5
    },
    customerPhone: {
        color: 'blue', 
    },
    customerAddress: {
        fontSize: 15
    },
    productHeaderTitle: {
        fontSize: 16,
        marginBottom: 10
    },
    productInfoWrapper: {
        alignItems: 'center',
        width: width,
        paddingVertical: 10
    },
    productImg: {
        width: 60,
        height: 60
    },
    productInfo: {
        width: width/1.1 - 155,
        height: 60,
        marginLeft: 15,
        paddingRight: 25
    },
    total: {
        width: 80,
        height: 60,
        alignItems: 'flex-end',
    },
    productCode: {
        color: 'gray',
        fontSize: 13
    },
    productPrice: {
        color: 'gray',
        fontSize: 13
    },
    borderWrapper: {
        width: width,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    borderInside: {
        width: width/1.1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    left: {
        width: width/2.2,
    },
    right: {
        width: width/2.2,
        alignItems: 'flex-end'
    },
    leftTitle: {
        fontSize: 15,
        marginBottom: 5
    },
    rightNumber: {
        fontSize: 15,
        marginBottom: 5
    },
    paymentHeaderTitle: {
        marginLeft: 20,
        fontSize: 15
    },
    top: {
        width: width,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        alignItems: 'center',
        paddingBottom: 10
    },
    text: {
        fontSize: 15
    },
    note: {
        fontSize: 13,
        color: 'gray',
    },
    rightRow: {
        width: width/2.2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    deliveryStatus: {
        fontSize: 16, 
        marginRight: 5,
        color: '#1C3FAA'
    },
    bottom: {
        width: width,
        alignItems: 'center'
    },
    noteTitle: {
        fontSize: 13,
        color: 'gray'
    },
    addNote: {
        fontSize: 15
    },
    leftNote: {
        width: width/1.1 - 50,
    },
    rightNote: {
        width: 50,
        alignItems: 'flex-end'
    },
    exportBtnWrapper: {
        width: width,
        borderTopColor: 'gray',
        borderTopWidth: 0.5,
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
        paddingTop: 10,
        paddingBottom: 20
    },
    exportBtnGroup: {
        width: width/1.1,
        flexDirection: 'row'
    },
    exportBtn: {
        borderRadius: 10,
        backgroundColor: '#1C3FAA',
        justifyContent: 'center',
        alignItems: 'center',
        width: width/1.1 - 60,
        height: 50
    },
    exportBtnTitle: {
        fontSize: 17,
        color: '#fff'
    },
    optionBtn: {
        width: 50,
        borderRadius: 10,
        backgroundColor: 'whitesmoke',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        height: 50
    },
})