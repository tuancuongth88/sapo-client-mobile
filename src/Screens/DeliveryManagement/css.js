import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        width: 30,
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        alignItems: 'center',
        width: 30,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    iconPlusWrapper: {
        width: 35,
        alignItems: 'center'
    },
    iconSearchWrapper: {
        width: 35,
        alignItems: 'flex-end'
    },
    headerSearchLeft: {
        width: 40,
    },
    headerSearchCenter: {
        width: width/1.08 - 70
    },
    headerSearchRight: {
        width: 30,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    deliveryList: {
        width: width,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 110
    }, 
    body: {
        backgroundColor: '#EAEAF7',
    },
    elementWrapper: {
        width: width/1.05, 
        backgroundColor: '#fff',
        marginTop: 5,
        marginBottom: 10,
        alignItems: 'center',
        borderRadius: 10,
        paddingVertical: 10
    },
    elementHeader: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    elementBody: {
        alignItems: 'center'
    }, 
    deliveryCodeWrapper: {
        width: (width/1.12)*(2/3),
        paddingRight: 15
    },
    deliveryCode: {
        fontWeight: 'bold',
        fontSize: 17,
        marginBottom: 3
    },
    deliveryStatusWrapper: {
        width: (width/1.12)*(1/3),
        alignItems: 'flex-end',
    },
    statusBorder: {
        borderRadius: 10,
        backgroundColor: 'green',
        paddingVertical: 3,
        width: (width/1.12)*(1/3) - 20,
        alignItems: 'center',
        marginBottom: 5
    },
    statusTitle: {
        fontSize: 13,
        color: '#fff'
    },
    customerAndShipWrapper: {
        width: width/1.12,
    },
    customerName: {
        color: 'gray',
        marginRight: 15,
        marginLeft: 10
    },
    customerWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    shipWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    time: {
        color: 'gray'
    },
    phone: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#1C3FAA'
    },
    deliveryFreeGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width,
        marginBottom: 15
    },
    left: {
        width: width/2,
        alignItems: 'center',
        borderColor: 'gray',
        borderRightWidth: 0.5,
    },
    right: {
        width: width/2,
        alignItems: 'center'
    },
    title: {
        fontSize: 13,
        color: 'gray',
        marginBottom: 5
    },
    fee: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    shipper: {
        marginRight: 15,
        marginLeft: 10,
        fontWeight: 'bold'
    },
    listDeliveryStatusWrapper: {
        backgroundColor: '#fff',
        height: 40
    },
    listDeliveryStatus: {
        height: 40,
    },
    statusElement: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    statusElementText: {
        fontSize: 15,
        color: '#000'
    },
    statusElementActive: {
        paddingHorizontal: 20,
        borderColor: '#1C3FAA',
        borderBottomWidth: 2,
        paddingVertical: 10,
    },
    statusElementTextActive: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#1C3FAA'
    }
})