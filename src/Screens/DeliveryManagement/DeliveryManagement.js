import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, TouchableHighlight, ScrollView } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import { width, height } from '../../Components/Dimensions/Dimensions';
import SearchBar from '../../Components/SearchBar/SearchBar';

export default function DeliveryManagement(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [deliveryList, setDeliveryList] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [switchSearch, setSwitchSearch] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [tabs, setTabs] = useState([
        {name: 'Tất cả', active: true},
        {name: 'Chờ lấy hàng', active: false},
        {name: 'Đang giao hàng', active: false},
        {name: 'Hủy giao - chờ nhận', active: false},
        {name: 'Hủy giao - đã nhận', active: false},
    ])

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const navigateToOrderDetailScreen = (item) => {
        props.navigation.navigate('DeliveryDetail', {
            item: item
        })
    }

    const swtichToSearch = () => {
        setSwitchSearch(true)
    }

    const switchToShowHeader = () => {
        setSwitchSearch(false)
    }

    const renderItem = ({item}) => {
        return(
            <TouchableWithoutFeedback onPress={() => navigateToOrderDetailScreen(item)}>
                <View style={styles.elementWrapper}>
                    <View style={styles.elementHeader}>
                        <View style={styles.deliveryCodeWrapper}>
                            <Text 
                                style={styles.deliveryCode}
                                numberOfLines={1}
                            >   
                                {item.code}
                            </Text>
                            <Text style={styles.time}>{item.time}</Text>
                        </View>
                        <View style={styles.deliveryStatusWrapper}>
                            <View style={styles.statusBorder}>
                                <Text style={styles.statusTitle}>Chờ lấy hàng</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.elementBody}>
                        <View style={styles.deliveryFreeGroup}>
                            <View style={styles.left}>
                                <Text style={styles.title}>Thu hộ</Text>
                                <Text style={styles.fee}>0</Text>
                            </View>
                            <View style={styles.right}>
                                <Text style={styles.title}>Phí trả shipper</Text>
                                <Text style={styles.fee}>{item.shipFee}</Text>
                            </View>
                        </View>
                        <View style={styles.customerAndShipWrapper}>
                            <View style={styles.shipWrapper}>
                                <Feather
                                    name='truck'
                                    size={15}
                                    color='gray'
                                />
                                <Text 
                                    style={styles.shipper}
                                    numberOfLines={1}
                                >
                                    {item.shipName}
                                </Text>
                            </View>
                            <View style={styles.customerWrapper}>
                                <Icon
                                    name='person-circle-outline'
                                    size={15}
                                    color='gray'
                                />
                                <Text 
                                    style={styles.customerName}
                                    numberOfLines={1}
                                >   
                                    {item.customer} - 
                                </Text>
                                <TouchableHighlight 
                                    underlayColor='lavender' 
                                    onPress={() => makeAPhoneCall()}
                                >
                                    <Text 
                                        style={styles.phone}
                                        numberOfLines={1}
                                    >
                                        {item.phone}
                                    </Text>
                                </TouchableHighlight >
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    const makeAPhoneCall = () => {

    }

    const onRefresh = () => {
        
    }

    const data = [
        {shipFee: '100000000', code: 'FUN0001', customer: 'Thắng', shipName: 'Sơn', time: '10:48', phone: '0917726521'},
        {shipFee: '250000', code: 'QKK001', customer: 'Trang', shipName: 'Hải', time: '22:38', phone: '0917726541'}, 
        {shipFee: '600000', code: 'QB001', customer: 'Anh Đào', shipName: 'Thảo', time: '17:15', phone: '0917726521'}, 
        {shipFee: '350000', code: 'BL001', customer: 'Thành', shipName: 'Trường', time: '7:45', phone: '0917726521'}, 
        {shipFee: '10000000', code: 'TV001', customer: 'Thiện', shipName: 'Tuấn Anh', time: '9:15', phone: '0917726521'}, 
        {shipFee: '20000', code: 'GT001', customer: 'Nguyên', shipName: 'Quốc Anh', time: '21:20', phone: '0917726521'}, 
    ]

    const activeTab = (activeKey) => {
        var tmp = [];
        tabs.map((tab, key) => {
            if(activeKey == key) {
                tmp.push({
                    name: tab.name,
                    active: true
                })
            }
            else {
                tmp.push({
                    name: tab.name,
                    active: false
                })
            }
        });
        setTabs(tmp);
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        {switchSearch ?
                            <View style={styles.header}>  
                                <View style={styles.headerSearchLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerSearchCenter}>
                                    <SearchBar
                                        placeholder='Tìm kiếm'
                                        backgroundColor='#EAEAF7'
                                        width={width/1.08 - 80}
                                        left={width/1.08 - 110}
                                        setKeyword={setKeyword}
                                    />
                                </View>
                                <View style={styles.headerSearchRight}>
                                    <TouchableWithoutFeedback onPress={() => switchToShowHeader()}>
                                        <Text style={{fontSize: 16}}>Hủy</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                                :
                            <View style={styles.header}>  
                                <View style={styles.headerLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerCenter}>
                                    <Text style={styles.headerTitle}>Quản lý giao hàng</Text>
                                </View>
                                <View style={styles.headerRight}>
                                    <View style={styles.iconSearchWrapper}>
                                        <TouchableWithoutFeedback
                                            onPress={() => swtichToSearch()}
                                        >
                                            <Feather
                                                name='search'
                                                size={25}
                                                color='#404040'
                                            />
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            </View>
                        }

                        <View style={styles.body}>
                            <View style={styles.listDeliveryStatusWrapper}>
                                <ScrollView
                                    contentContainerStyle={styles.listDeliveryStatus}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                >
                                    {tabs.map((tab, key) => {
                                        return (
                                            <TouchableWithoutFeedback 
                                                onPress={() => activeTab(key)}
                                                key={key}
                                            >
                                                <View style={tab.active ? styles.statusElementActive : styles.statusElement}>
                                                    <Text style={tab.active ? styles.statusElementTextActive : styles.statusElementText}>{tab.name}</Text>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        )
                                    })}
                                    
                                </ScrollView>
                            </View>
                            <FlatList
                                contentContainerStyle={styles.deliveryList}
                                // ref={(ref) => { this.flatListRef = ref; }}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={data}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                            />
                        </View>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

