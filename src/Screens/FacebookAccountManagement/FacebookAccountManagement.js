import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, ScrollView } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';

export default function FacebookAccountManagement(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [openLoginFbApp, setOpenLoginFbApp] = useState(false)

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalLoginFbApp = () => {
        setOpenLoginFbApp(true)
    }

    const openModalLoginFbUserAndPass = () => {

    }

    const openModalAddFullToken = () => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Tài khoản Facebook</Text>
                            </View>
                            <View style={styles.headerRight}>
                                
                            </View>
                        </View>
                        
                        <ScrollView 
                            contentContainerStyle={styles.headerGroupBtn}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                        >
                            <TouchableWithoutFeedback
                                onPress={() => openModalLoginFbApp()}
                            >
                                <View style={styles.headerBtnWrapper}>
                                    <Text style={styles.headerBtnTitle}>Login with Facebook App</Text>
                                </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback
                                onPress={() => openModalLoginFbUserAndPass()}
                            >
                                <View style={styles.headerBtnWrapper}>
                                    <Text style={styles.headerBtnTitle}>Login with Facebook User/Pass</Text>
                                </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback
                                onPress={() => openModalAddFullToken()}
                            >
                                <View style={styles.headerBtnWrapper}>
                                    <Text style={styles.headerBtnTitle}>Add Full Token</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </ScrollView>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

