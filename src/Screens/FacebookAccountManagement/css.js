import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        justifyContent: 'center'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    headerBtnWrapper: {
        marginLeft: 20,
        marginTop: 10,
        backgroundColor: '#1C3FAA',
        borderRadius: 10
    },
    headerBtnTitle: {
        marginVertical: 10,
        marginHorizontal: 15,
        color: '#fff',
        fontWeight: 'bold'
    },
    headerGroupBtn: {
        paddingRight: 20,
        marginBottom: 10
    },
    body: {
        
    }
})