import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width/1.08,
        flexDirection: 'row',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#1C3FAA',
        marginLeft: 10,
        marginBottom: 5
    },
    headerLeft: {
        width: (width/1.08)/2,
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginBottom: 10,
        marginTop: 10
    },
    headerRight: {
        width: (width/1.08)/2,
        alignItems: 'flex-end',
        marginTop: 30
    }
})