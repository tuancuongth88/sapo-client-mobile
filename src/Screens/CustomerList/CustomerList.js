import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import SearchBar from '../../Components/SearchBar/SearchBar';
import { width, height } from '../../Components/Dimensions/Dimensions';
import ModalCreateCustomer from '../../Components/Modal/ModalCreateCustomer/ModalCreateCustomer';

export default function CustomerList(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalCreateNewCustomerVisible, setModalCreateNewCustomerVisible] = useState(false);
    const [customerList, setCustomerList] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [switchSearch, setSwitchSearch] = useState(false);
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const navigateToCustomerDetailScreen = () => {
        props.navigation.navigate('CustomerDetail')
    }

    const openModalCreateNewCustomer = () => {
        setModalCreateNewCustomerVisible(true)
    }

    const swtichToSearch = () => {
        setSwitchSearch(true)
    }

    const switchToShowHeader = () => {
        setSwitchSearch(false)
    }

    const renderItem = ({item}) => {
        return(
            <TouchableWithoutFeedback onPress={() => navigateToCustomerDetailScreen()}>
                <View style={styles.elementWrapper}>
                    <View style={styles.topRow}>
                        <View style={styles.topRowLeft}>
                            <Text style={styles.customerName}>{item.name}</Text>
                        </View>
                        <View style={styles.topRowRight}>
                            <Text style={styles.status}>{item.status}</Text>
                        </View>
                    </View>
                    <View style={styles.bottomRow}>
                        <Text style={styles.phone}>{item.phone}</Text>
                        <Text style={styles.address}>{item.address}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    const onRefresh = () => {
        
    }

    const data = [
        {name: 'Thắng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Nam', phone: '096471122', address: '19 a đường Bười, Tây Hồ, hà nội', status: 'Đang giao dịch'},
        {name: 'Phương', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Sơn', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Tùng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Tùng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Tùng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Tùng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'},
        {name: 'Tùng', phone: '096471122', address: '19 đê la thành, giảng võ ba đình hà nội', status: 'Đang giao dịch'}
    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        {switchSearch ? 
                            <View style={styles.header}>  
                                <View style={styles.headerSearchLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerSearchCenter}>
                                    <SearchBar
                                        placeholder='Tìm kiếm'
                                        backgroundColor='#EAEAF7'
                                        width={width/1.08 - 80}
                                        left={width/1.08 - 110}
                                        setKeyword={setKeyword}
                                    />
                                </View>
                                <View style={styles.headerSearchRight}>
                                    <TouchableWithoutFeedback onPress={() => switchToShowHeader()}>
                                        <Text style={{fontSize: 16}}>Hủy</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                                :
                            <View style={styles.header}>  
                                <View style={styles.headerLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerCenter}>
                                    <Text style={styles.headerTitle}>Khách hàng</Text>
                                </View>
                                <View style={styles.headerRight}>
                                    <View style={styles.iconPlusWrapper}>
                                        <TouchableOpacity
                                            onPress={() => openModalCreateNewCustomer()}
                                        >
                                            <Feather
                                                name='plus'
                                                size={30}
                                                color='#404040'
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.iconSearchWrapper}>
                                        <TouchableWithoutFeedback
                                            onPress={() => swtichToSearch()}
                                        >
                                            <Feather
                                                name='search'
                                                size={25}
                                                color='#404040'
                                            />
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            </View>
                        }

                        <View style={styles.body}>
                            <FlatList
                                contentContainerStyle={styles.customerList}
                                // ref={(ref) => { this.flatListRef = ref; }}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={data}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                            />
                        </View>
                        {/* MODAL CREATE CUSTOMER */}
                            <ModalCreateCustomer
                                modalCreateNewCustomerVisible={modalCreateNewCustomerVisible}
                                setModalCreateNewCustomerVisible={setModalCreateNewCustomerVisible}
                            />
                        {/* MODAL CREATE CUSTOMER */}
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

