import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        paddingRight: 10,
        width: 70,
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        alignItems: 'center',
        width: 70,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 140,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    iconPlusWrapper: {
        width: 35,
        alignItems: 'center'
    },
    iconSearchWrapper: {
        width: 35,
        alignItems: 'flex-end'
    },
    headerSearchLeft: {
        width: 40,
    },
    headerSearchCenter: {
        width: width/1.08 - 70
    },
    headerSearchRight: {
        width: 30,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    customerList: {
        width: width,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 120
    }, 
    body: {
        backgroundColor: '#EAEAF7',
    },
    elementWrapper: {
        width: width, 
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingVertical: 10,
        borderColor: 'silver',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5
    },
    topRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    topRowLeft: {
        width: (width/1.05)/2,
        paddingLeft: 10
    },
    topRowRight: {
        width: (width/1.05)/2,
        alignItems: 'flex-end'
    },
    customerName: {
        fontWeight: 'bold',
        fontSize: 16
    },
    status: {
        color: 'tomato',
        fontSize: 18
    },
    phone: {
        color: 'gray',
        fontSize: 13
    },
    address: {
        color: 'gray',
        fontSize: 13  
    },
    bottomRow: {
        width: width/1.05,
        paddingLeft: 10
    }
})