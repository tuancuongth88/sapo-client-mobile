import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        paddingRight: 10,
        width: 70,
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        alignItems: 'center',
        width: 70,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 140,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    iconPlusWrapper: {
        width: 35,
        alignItems: 'center'
    },
    iconSearchWrapper: {
        width: 35,
        alignItems: 'flex-end'
    },
    headerSearchLeft: {
        width: 40,
    },
    headerSearchCenter: {
        width: width/1.08 - 70
    },
    headerSearchRight: {
        width: 30,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    orderList: {
        width: width,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 110
    }, 
    body: {
        backgroundColor: '#EAEAF7',
    },
    elementWrapper: {
        width: width/1.05, 
        backgroundColor: '#fff',
        marginTop: 5,
        marginBottom: 10,
        alignItems: 'center',
        borderRadius: 10,
        paddingVertical: 10
    },
    elementHeader: {
        flexDirection: 'row',
        marginBottom: 15,
    },
    elementBody: {
        flexDirection: 'row',
    }, 
    orderNameWrapper: {
        width: (width/1.12)*(2/3),
        paddingRight: 15
    },
    orderName: {
        fontWeight: 'bold',
        fontSize: 17,
        marginBottom: 3
    },
    orderStatusWrapper: {
        width: (width/1.12)*(1/3),
        alignItems: 'flex-end',
    },
    statusBorder: {
        borderRadius: 10,
        backgroundColor: 'orange',
        paddingVertical: 3,
        width: (width/1.12)*(1/3) - 20,
        alignItems: 'center',
        marginBottom: 5
    },
    statusTitle: {
        fontSize: 13,
        color: '#fff'
    },
    orderCode: {
        color: 'tomato',
        fontSize: 13
    },
    customerWrapper: {
        width: (width/1.12)*(3/5),
    },
    customerName: {
        color: 'gray',
        marginRight: 15,
        marginLeft: 5
    },
    customerAddress: {
        color: 'gray',
        marginRight: 20,
        marginLeft: 5
    },
    customerNote: {
        marginRight: 15,
        marginLeft: 5
    },
    priceWrapper: {
        width: (width/1.12)*(2/5),
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    price: {
        fontSize: 17
    },
    customerNameWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    customerAddressWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    customerNoteWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    time: {
        color: 'gray'
    },
    phone: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#1C3FAA'
    }
})