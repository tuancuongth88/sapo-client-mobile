import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, TouchableOpacity, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import SearchBar from '../../Components/SearchBar/SearchBar';
import NumberFormat from 'react-number-format';
import Icon from 'react-native-vector-icons/Ionicons';
import { width, height } from '../../Components/Dimensions/Dimensions';
import ModalCreateOrder from '../../Components/Modal/ModalCreateOrder/ModalCreateOrder';

export default function OrderList(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalCreateNewOrderVisible, setModalCreateNewOrderVisible] = useState(false);
    const [orderList, setOrderList] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [switchSearch, setSwitchSearch] = useState(false);
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const navigateToOrderDetailScreen = () => {
        props.navigation.navigate('OrderDetail')
    }

    const openModalCreateNewOder = () => {
        setModalCreateNewOrderVisible(true)
    }

    const swtichToSearch = () => {
        setSwitchSearch(true)
    }

    const switchToShowHeader = () => {
        setSwitchSearch(false)
    }

    const renderItem = ({item}) => {
        return(
            <TouchableWithoutFeedback onPress={() => navigateToOrderDetailScreen()}>
                <View style={styles.elementWrapper}>
                    <View style={styles.elementHeader}>
                        <View style={styles.orderNameWrapper}>
                            <Text 
                                style={styles.orderName}
                                numberOfLines={1}
                            >   
                                {item.name}
                            </Text>
                            <Text 
                                style={styles.orderCode}
                                numberOfLines={1}
                            >
                                {item.code}
                            </Text>
                        </View>
                        <View style={styles.orderStatusWrapper}>
                            <View style={styles.statusBorder}>
                                <Text style={styles.statusTitle}>Đang giao dịch</Text>
                            </View>
                            <Text style={styles.time}>{item.time}</Text>
                        </View>
                    </View>
                    <View style={styles.elementBody}>
                        <View style={styles.customerWrapper}>
                            <View style={styles.customerNameWrapper}>
                                <Icon
                                    name='person-circle-outline'
                                    size={15}
                                    color='gray'
                                />
                                <Text 
                                    style={styles.customerName}
                                    numberOfLines={1}
                                >   
                                    {item.customer} - 
                                </Text>
                                <TouchableHighlight 
                                    underlayColor='lavender' 
                                    onPress={() => makeAPhoneCall()}
                                >
                                    <Text style={styles.phone}>{item.phone}</Text>
                                </TouchableHighlight >
                            </View>
                            <View style={styles.customerAddressWrapper}>
                                <Icon
                                    name='location'
                                    size={15}
                                    color='gray'
                                />
                                <Text 
                                    style={styles.customerAddress}
                                    numberOfLines={1}
                                >
                                    {item.address}
                                </Text>
                            </View>
                            <View style={styles.customerNoteWrapper}>
                                <Feather
                                    name='file-text'
                                    size={15}
                                    color='gray'
                                />
                                <Text 
                                    style={styles.customerNote}
                                    numberOfLines={1}
                                >
                                    <Text style={{fontWeight: 'bold'}}>Ghi chú:</Text> Agagagsaffdgdfgdfgdfg
                                </Text>
                            </View>
                        </View>
                        <View style={styles.priceWrapper}>
                            <NumberFormat
                                value={item.price}
                                thousandSeparator={','}
                                displayType={'text'}
                                renderText={value => 
                                    <Text 
                                        style={styles.price}
                                        numberOfLines={1}
                                    >
                                        {value}
                                    </Text>}
                            />
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    const makeAPhoneCall = () => {

    }

    const onRefresh = () => {
        
    }

    const data = [
        {name: 'đơn hàng áo phông ádasdasdasdadasdadaasdsad', price: '100000000', code: 'AP001123213123123', customer: 'Thắng', address: '19a đường bưởi dfsdfsdfdsfdsfsdfsdfsfsdfsdfsfdf', time: '10:48', phone: '0917726521'},
        {name: 'đơn hàng quần bò', price: '250000', code: 'QKK001', customer: 'Trang', address: '190 đê la thành', time: '22:38', phone: '0917726541'}, 
        {name: 'quần vải + áo sơ mi denim bò', price: '600000', code: 'QB001', customer: 'Anh Đào', address: '27 ngõ 16 quan nhân', time: '17:15', phone: '0917726521'}, 
        {name: 'bàn là philips', price: '350000', code: 'BL001', customer: 'Thành', address: 'số 6 ngách 35 ngõ 91 nguyễn chí thanh', time: '7:45', phone: '0917726521'}, 
        {name: 'tivi plasma LG 50 inch', price: '10000000', code: 'TV001', customer: 'Thiện', address: '19 ngõ 1 đê la thành', time: '9:15', phone: '0917726521'}, 
        {name: 'găng tay rửa bát loại 1', price: '20000', code: 'GT001', customer: 'Nguyên', address: '780 đường láng', time: '21:20', phone: '0917726521'}, 
    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        {switchSearch ? 
                            <View style={styles.header}>
                                <View style={styles.headerSearchLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerSearchCenter}>
                                    <SearchBar
                                        placeholder='Tìm kiếm'
                                        backgroundColor='#EAEAF7'
                                        width={width/1.08 - 80}
                                        left={width/1.08 - 110}
                                        setKeyword={setKeyword}
                                    />
                                </View>
                                <View style={styles.headerSearchRight}>
                                    <TouchableWithoutFeedback onPress={() => switchToShowHeader()}>
                                        <Text style={{fontSize: 16}}>Hủy</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                               :
                            <View style={styles.header}>  
                               <View style={styles.headerLeft}>
                                   <TouchableWithoutFeedback
                                       onPress={() => goBackPreviousScreen()}
                                   >
                                       <Feather
                                           name='arrow-left'
                                           size={30}
                                           color='#404040'
                                       />
                                   </TouchableWithoutFeedback>
                               </View>
                               <View style={styles.headerCenter}>
                                   <Text style={styles.headerTitle}>Đơn hàng</Text>
                               </View>
                               <View style={styles.headerRight}>
                                   <View style={styles.iconPlusWrapper}>
                                       <TouchableOpacity
                                           onPress={() => openModalCreateNewOder()}
                                       >
                                           <Feather
                                               name='plus'
                                               size={30}
                                               color='#404040'
                                           />
                                       </TouchableOpacity>
                                   </View>
                                   <View style={styles.iconSearchWrapper}>
                                       <TouchableWithoutFeedback
                                           onPress={() => swtichToSearch()}
                                       >
                                           <Feather
                                               name='search'
                                               size={25}
                                               color='#404040'
                                           />
                                       </TouchableWithoutFeedback>
                                   </View>
                               </View>
                           </View>
                        }

                        <View style={styles.body}>
                            <FlatList
                                contentContainerStyle={styles.orderList}
                                // ref={(ref) => { this.flatListRef = ref; }}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={data}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                            />
                        </View>
                        {/* MODAL CREATE ORDER */}
                            <ModalCreateOrder
                                modalCreateNewOrderVisible={modalCreateNewOrderVisible}
                                setModalCreateNewOrderVisible={setModalCreateNewOrderVisible}
                            />
                        {/* MODAL CREATE ORDER */}
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

