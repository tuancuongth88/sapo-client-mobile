import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const SIZE = 15;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#EAEAF7',
        flex: 1
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        justifyContent: 'center'
    },
    headerLeft: {
        alignItems: 'center',
        width: 30
    },
    headerRight: {
        alignItems: 'center',
        width: 30
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040',
    },
    iconGoPreviousScreen: {

    },
    body: {
        paddingBottom: 30
    },
    row: {
        flexDirection: 'row',
        width: width/1.1,
        alignItems: 'center'
    },
    rowElement: {
        width: (width/1.06)/5,
        paddingVertical: 5,
        alignItems: 'center'
    },
    title: {
        fontSize: 13
    },
    img: {
        width: width,
        borderColor: 'gray',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        alignItems: 'center',
        paddingVertical: 10,
    },
    inside: {
        width: width/1.1,
        paddingBottom: 20
    },
    insideRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5
    },
    source: {
        marginBottom: 10
    },
    customerWrapper: {
        width: width,
        backgroundColor: '#fff',
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        borderColor: 'gray',
        alignItems: 'center',
        marginBottom: 10
    },
    customerInside: {
        flexDirection: 'row',
        width: width/1.1,
        alignItems: 'center',
        marginBottom: 10
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    customerInfo: {
        width: width/1.1 - 70,
        marginLeft: 20,
    },
    customerName: {
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 5
    },
    customerPhone: {
        color: 'blue', 
    },
    customerAddress: {
        fontSize: 15
    },
    productHeaderTitle: {
        fontSize: 16,
        marginBottom: 10
    },
    productInfoWrapper: {
        alignItems: 'center',
        width: width,
        paddingVertical: 10
    },
    productImg: {
        width: 60,
        height: 60
    },
    productInfo: {
        width: width/1.1 - 155,
        height: 60,
        marginLeft: 15,
        paddingRight: 25
    },
    total: {
        width: 80,
        height: 60,
        alignItems: 'flex-end',
    },
    productCode: {
        color: 'gray',
        fontSize: 13
    },
    productPrice: {
        color: 'gray',
        fontSize: 13
    },
    borderWrapper: {
        width: width,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    borderInside: {
        width: width/1.1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    left: {
        width: width/2.2,
    },
    right: {
        width: width/2.2,
        alignItems: 'flex-end'
    },
    leftTitle: {
        fontSize: 15,
        marginBottom: 5
    },
    rightNumber: {
        fontSize: 15,
        marginBottom: 5
    },
    top: {
        width: width,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        alignItems: 'center',
        paddingBottom: 10
    },
    text: {
        fontSize: 15
    },
    note: {
        fontSize: 13,
        color: 'gray',
    },
    rightRow: {
        width: width/2.2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    bottom: {
        width: width,
        alignItems: 'center'
    },
    noteTitle: {
        fontSize: 13,
        color: 'gray'
    },
    addNote: {
        fontSize: 15
    },
    leftNote: {
        width: width/1.1 - 50,
    },
    rightNote: {
        width: 50,
        alignItems: 'flex-end'
    },
    shipper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    shipperName: {
        marginLeft: 10,
        fontSize: 16,
        fontWeight: 'bold'
    },
    deliveryTitle: {
        fontSize: 18,
        marginLeft: 40
    },
    orderName: {
        color: 'blue',
        marginLeft: 40
    },
    deleveryStatus: {
        marginLeft: 45,
        flexDirection: 'row',
        alignItems: 'center',
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'tomato'
    },
    deleveryStatusTitle: {
        color: 'tomato',
        marginLeft: 10
    },
    copy: {
        fontSize: 16,
        color: 'blue',
        marginTop: 30
    },
    separate: {
        width: width/1.1,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        marginBottom: 10
    },
    appointment: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    appointmentTitle: {
        fontWeight: 'bold',
        marginLeft: 10,
        fontSize: 17
    },
})