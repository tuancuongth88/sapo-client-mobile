import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, ImageBackground, Image, TouchableHighlight, TouchableOpacity } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import ModalAddNote from '../../Components/Modal/ModalAddNote/ModalAddNote';
import { width } from '../../Components/Dimensions/Dimensions';

export default function DeliveryDetail(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalAddNoteVisible, setModalAddNoteVisible] = useState(false);
    const [note, setNote] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalAddNote = () => {
        setModalAddNoteVisible(true)
    }

    const goToProductDetailScreen = () => {
        props.navigation.navigate('ProductDetail')
    }

    const goToOrderDetailScreen = () => {
        props.navigation.navigate('OrderDetail')
    }

    const printDelivery = () => {

    }

    const copyBillCode = () => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>FUN00001</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableOpacity onPress={() => printDelivery()}>
                                    <Icon
                                        name='print'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <ImageBackground
                                style={styles.img}
                                source={require('../../Assets/delivery_backgroud.jpg')}
                            >   
                                <View style={[styles.inside, {flexDirection: 'row'}]}>
                                    <View style={styles.left}>
                                        <View style={styles.shipper}>
                                            <Icon
                                                name='person-circle-outline'
                                                size={30}
                                            />
                                            <Text style={styles.shipperName}>Sơn</Text>
                                        </View>
                                        <Text style={styles.deliveryTitle}>FUN00001</Text>
                                        <TouchableWithoutFeedback onpress={() => goToOrderDetailScreen()}>
                                            <Text style={styles.orderName}>Đơn hàng SON0001</Text>
                                        </TouchableWithoutFeedback>
                                        <View style={styles.deleveryStatus}>
                                            <View style={styles.dot}></View>
                                            <Text style={styles.deleveryStatusTitle}>Hủy đóng gói</Text>
                                        </View>
                                        <View style={styles.deleveryStatus}>
                                            <View style={styles.dot}></View>
                                            <Text style={styles.deleveryStatusTitle}>Chưa đối soát</Text>
                                        </View>
                                    </View>
                                    <View style={styles.right}>
                                        <TouchableWithoutFeedback onPress={() => copyBillCode()}>
                                            <Text style={styles.copy}>SAO CHÉP</Text>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            </ImageBackground>

                            <View style={[styles.borderWrapper, {paddingVertical: 10, marginBottom: 10}]}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftTitle}>Thu hộ COD</Text>
                                        <Text style={styles.leftTitle}>Phí trả đối tác vận chuyển</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={[styles.rightNumber, {fontWeight: 'bold'}]}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={[styles.rightNumber, {fontWeight: 'bold'}]}
                                            numberOfLines={1}
                                        >
                                            100,000
                                        </Text>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.customerWrapper}>
                                <View style={styles.customerInside}>
                                    <Image
                                        style={styles.avatar}
                                        source={require('../../Assets/avatar.png')}
                                    />
                                    <View style={styles.customerInfo}>
                                        <Text style={styles.customerName}>Thắng - <Text style={styles.customerPhone}>0912637892</Text></Text>
                                        <Text style={styles.customerAddress}>19a đường Bưởi, Phưởng Bưởi, Quận Tây Hồ, Hà Nội</Text>
                                    </View>
                                </View>
                                <View style={{width: width/1.1}}>
                                    <View style={styles.separate}></View>
                                    <View style={styles.appointment}>
                                        <Feather
                                            name='calendar'
                                            size={25}
                                            color='gray'
                                        />
                                        <Text style={styles.appointmentTitle}>Hẹn giao hàng: --</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.borderWrapper, {paddingVertical: 10, marginBottom: 10}]}>
                                <View style={styles.borderInside}>
                                    <Text style={styles.productHeaderTitle}>Sản phẩm (1)</Text>
                                </View>
                                <TouchableHighlight
                                    onPress={() => goToProductDetailScreen()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.productInfoWrapper}>
                                        <View style={styles.borderInside}>
                                            <Image
                                                style={styles.productImg}
                                                source={require('../../Assets/no_image.png')}
                                            />
                                            <View style={styles.productInfo}>
                                                <Text 
                                                    style={styles.productName}
                                                    numberOfLines={1}    
                                                >
                                                    áo phông - xxl - tím ádasdasdadadasdassadasdadss
                                                </Text>
                                                <Text 
                                                    style={styles.productCode}
                                                    numberOfLines={1} 
                                                >
                                                    SKU: AP001-xx-t
                                                </Text>
                                                <Text 
                                                    style={styles.productPrice}
                                                    numberOfLines={1} 
                                                >
                                                    100,000
                                                </Text>
                                            </View>
                                            <View style={styles.total}>
                                                <Text 
                                                    style={styles.totalPrice}
                                                    numberOfLines={1}
                                                >
                                                    100,000,00000
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>

                            <View style={[styles.borderWrapper, {paddingTop: 15, paddingBottom: 10, marginBottom: 10}]}>
                                <View style={styles.borderInside}>
                                    <View style={styles.left}>
                                        <Text style={styles.leftTitle}>Tổng tiền hàng</Text>
                                        <Text style={styles.leftTitle}>Thuế</Text>
                                        <Text style={styles.leftTitle}>Chiết khấu</Text>
                                        <Text style={styles.leftTitle}>Phí giao hàng</Text>
                                        <Text style={[styles.leftTitle, {fontWeight: 'bold'}]}>Khách hàng phải trả</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            100,000
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={styles.rightNumber}
                                            numberOfLines={1}
                                        >
                                            0
                                        </Text>
                                        <Text 
                                            style={[styles.rightNumber, {fontWeight: 'bold'}]}
                                            numberOfLines={1}
                                        >
                                            100,000
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            
                            <TouchableHighlight
                                onPress={() => openModalAddNote()}
                                underlayColor='lavender'
                            >
                                <View style={[styles.borderWrapper, {paddingVertical: 10}]}>
                                    <View style={styles.borderInside}>
                                        <View style={styles.leftNote}>
                                            {note == '' ? 
                                                <Text style={[styles.text, {color: 'blue'}]}>Thêm ghi chú</Text>
                                                    :
                                                <View>
                                                    <Text style={styles.noteTitle}>Ghi chú</Text>
                                                    <Text style={styles.addNote}>{note}</Text>
                                                </View>
                                            }
                                        </View>
                                        <View style={styles.rightNote}>
                                            <Feather
                                                name='file-text'
                                                size={20}
                                                color='blue'
                                            />
                                        </View>
                                    </View>
                                </View>
                            </TouchableHighlight>
                            {/* MODAL ADD NOTE */}
                                <ModalAddNote
                                    modalAddNoteVisible={modalAddNoteVisible}
                                    setModalAddNoteVisible={setModalAddNoteVisible}
                                    note={note}
                                    setNote={setNote}
                                />
                            {/* MODAL ADD NOTE */}
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

