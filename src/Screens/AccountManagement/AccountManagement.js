import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, ScrollView } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';

export default function AccountManagement(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const navigateToFbAccManagementScreen = () => {
        props.navigation.navigate('FacebookAccountManagement')
    }

    const navigateToInstaAccManagementScreen = () => {
        // props.navigation.navigate('InstagramAccountManagement')
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Quản lý tài khoản</Text>
                            </View>
                            <View style={styles.headerRight}>

                            </View>
                        </View> 
                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={styles.bodyLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => navigateToFbAccManagementScreen()}
                                >
                                    <View style={styles.fbWrapper}>
                                        <View style={styles.fbIconWrapper}>
                                            <Feather
                                                name='facebook'
                                                size={100}
                                                color='#fff'
                                                style={{textAlign: 'center'}}
                                            />
                                        </View>
                                        <View style={styles.fbTitleWrapper}>
                                            <Text style={styles.fbTitle}>Quản lý tài khoản facebook</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                            
                            <View style={styles.bodyRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => navigateToInstaAccManagementScreen()}
                                >
                                    <View style={styles.instaWrapper}>
                                        <View style={styles.instaIconWrapper}>
                                            <Feather
                                                name='instagram'
                                                size={75}
                                                color='#fff'
                                                style={{textAlign: 'center'}}
                                            />
                                        </View>
                                        <View style={styles.instaTitleWrapper}>
                                            <Text style={styles.instaTitle}>Quản lý tài khoản instagram</Text>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

