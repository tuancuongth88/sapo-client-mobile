import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 10,
        // borderBottomColor: 'gray',
        // borderBottomWidth: 0.5,
        // backgroundColor: '#fff',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 7,
        // },
        // shadowOpacity: 0.43,
        // shadowRadius: 9.51,
        // elevation: 5,
        justifyContent: 'center'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    body: {
        width: width/1.1,
        flexDirection: 'row'
    },
    bodyLeft: {
        width: (width/1.1) / 2,
        alignItems: 'center'
    },
    bodyRight: {
        width: (width/1.1) / 2,
        alignItems: 'center'
    },
    fbWrapper: {
        width: (width/1.1) / 2 - 30,
        height: 170,
        borderRadius: 10,
        flexDirection: 'row',
        backgroundColor: '#1C3FAA',
        marginBottom: 20,
        marginTop: 15
    },
    fbIconWrapper: {
        width: ((width/1.1) / 2 - 30) / 2,
        justifyContent: 'center',
    },
    fbTitleWrapper: {
        width: ((width/1.1) / 2 - 30) / 2,
        justifyContent: 'center',
    },
    fbTitle: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 15,
        lineHeight: 25
    },
    instaWrapper: {
        width: (width/1.1) / 2 - 30,
        height: 170,
        borderRadius: 10,
        backgroundColor: '#bc2a8d',
        marginTop: 90,
        marginBottom: 20
    },
    instaIconWrapper: {
        height: 100,
        justifyContent: 'center',
    },
    instaTitleWrapper: {
        height: 70,
        alignItems: 'center',
    },
    instaTitle: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 15,
        lineHeight: 25
    }
})