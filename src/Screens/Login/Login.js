import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, ScrollView, TouchableOpacity, Image, Alert } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Input from '../../Components/Input/Input';
import { width, height } from '../../Components/Dimensions/Dimensions';
import postLogin from '../../Api/postLogin';
import AsyncStorage from '@react-native-community/async-storage';

export default function Login(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goToForgotPasswordScreen = () => {
        props.navigation.navigate('ForgotPassword')
    }

    const goToRegisterScreen = () => {
        props.navigation.navigate('Register')
    }

    const login = async() => {
        if(email == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập email');
        }
        else if(password == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập mật khẩu');
        }
        else {
            const res = await postLogin(email, password);
            if(res.status == 200) {
                const resLogin = await res.json();
                if(resLogin.code == 200) {
                    await AsyncStorage.setItem('userToken', resLogin.data.access_token);
                    props.navigation.navigate('BottomNavigation')
                }
                else if(resLogin.code == 401) {
                    Alert.alert('Lỗi !!!', resLogin.message);
                }
            }
            else if(res.status == 500) {
                Alert.alert('Lỗi !!!', 'Yêu cầu không thể thực hiện. Vui lòng đăng nhập lại !!!');
            }
        } 
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <ScrollView 
                        contentContainerStyle={styles.container}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.logo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../Assets/logo.png')}
                            />
                        </View>

                        <View style={styles.phoneOrEmailInput}>
                            <Input
                                placeholder='Email'
                                name='id'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                keyboardType='email-address'
                                setValue={setEmail}
                                editable={true}
                            />
                        </View>

                        <View style={styles.passwordInput}>
                            <Input
                                placeholder='Mật khẩu'
                                name='password'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 45}
                                paddingRight={60}
                                type='Password'
                                setValue={setPassword}
                                editable={true}
                            />
                        </View>

                        <TouchableOpacity
                            style={styles.loginBtn}
                            onPress={() => login()}
                        >
                            <Text style={styles.loginText}>Đăng nhập</Text>
                        </TouchableOpacity>

                        <TouchableWithoutFeedback
                            onPress={() => goToForgotPasswordScreen()}
                        >
                            <Text style={styles.forgotPassword}>Quên mật khẩu !!!</Text>
                        </TouchableWithoutFeedback>

                        <View style={styles.orWrapper}>
                            <View style={styles.line}></View>
                            <Text style={styles.orText}>HOẶC</Text>
                            <View style={styles.line}></View>
                        </View>
                            
                        <TouchableOpacity
                            onPress={() => goToRegisterScreen()}
                            style={styles.createAccount}
                        >
                            <Text style={styles.register}>Tạo tài khoản</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

