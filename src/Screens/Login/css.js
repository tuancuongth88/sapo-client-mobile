import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    logo: {
        width: width/1.5,
        height: 120,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: height/8,
        marginBottom: 50,
    },
    logoImage: {
        width: width/1.5,
        height: 120,
    },
    phoneOrEmailInput: {
        marginBottom: 10
    },
    passwordInput: {
        marginBottom: 30
    },
    loginBtn: {
        width: width/1.1,
        backgroundColor: '#1C3FAA',
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 10,
        marginBottom: 15
    },
    loginText: {
        color: '#fff',
        fontSize: 18
    },
    forgotPassword: {
        marginBottom: 70,
        fontSize: 15,
        color: '#1C3FAA',
        fontWeight: 'bold'
    },
    register: {
        fontSize: 15,
        color: '#1C3FAA',
        fontWeight: 'bold'
    },
    orWrapper: {
        width: width/2,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    line: {
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        width: (width/2 - 20)/2
    },
    orText: {
        marginHorizontal: 5,
        fontSize: 13,
    },
    createAccount: {
        width: width/1.1,
        paddingVertical: 5,
        alignItems: 'center',
        backgroundColor: 'lavender',
        borderRadius: 5
    }
})