import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    header: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    body: {
        backgroundColor: '#EAEAF7',
        flex: 1
    },
    logoWrapper: {
        height: 150,
        width: width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        height:70,
        width: 150
    },
    borderWrapper: {
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        backgroundColor: '#fff',
        width: width
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        width: 50,
        alignItems: 'center'
    },
    borderInside: {
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        width: width - 50
    },
    lastBorderInside: {
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        width: width - 50
    },
    left: {
        width: width - 90
    },
    right: {
        width: 40,
        alignItems: 'center'
    },
    title: {
        fontSize: 16
    }
})