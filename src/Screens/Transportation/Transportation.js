import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Image, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ModalDeliveryCostEstimate from '../../Components/Modal/ModalDeliveryCostEstimate/ModalDeliveryCostEstimate';

export default function Transportation(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalDeliveryCostEstimateVisible, setModalDeliveryCostEstimateVisible] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const goToTransportOverviewScreen = () => {
        props.navigation.navigate('TransportOverview')
    }

    const goToDeliveryPartnerScreen = () => {
        props.navigation.navigate('DeliveryPartner')
    }

    const openModalDeliveryCostEstimate = () => {
        setModalDeliveryCostEstimateVisible(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Vận chuyển</Text>
                            </View>
                            <View style={styles.headerRight}>
                                
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >   
                            <View style={styles.logoWrapper}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../Assets/logo.png')}
                                />
                            </View>
                            <View style={styles.borderWrapper}>
                                <TouchableHighlight
                                    onPress={() => goToTransportOverviewScreen()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.icon}>
                                            <FontAwesome
                                                name='truck'
                                                size={20}
                                                color='gray'
                                            />
                                        </View>
                                        <View style={styles.borderInside}>
                                            <View style={styles.left}>
                                                <Text style={styles.title}>Tổng quan vận chuyển</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    onPress={() => openModalDeliveryCostEstimate()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.icon}>
                                            <FontAwesome5
                                                name='search-dollar'
                                                size={20}
                                                color='gray'
                                            />
                                        </View>
                                        <View style={styles.borderInside}>
                                            <View style={styles.left}>
                                                <Text style={styles.title}>Tra cứu cước phí</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                                {/* MODAL COST ESTIMATE */}
                                    <ModalDeliveryCostEstimate
                                        modalDeliveryCostEstimateVisible={modalDeliveryCostEstimateVisible}
                                        setModalDeliveryCostEstimateVisible={setModalDeliveryCostEstimateVisible}
                                    />
                                {/* MODAL COST ESTIMATE */}

                                <TouchableHighlight
                                    onPress={() => goToDeliveryPartnerScreen()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.icon}>
                                            <Icon
                                                name='person'
                                                size={20}
                                                color='gray'
                                            />
                                        </View>
                                        <View style={styles.lastBorderInside}>
                                            <View style={styles.left}>
                                                <Text style={styles.title}>Đối tác</Text>
                                            </View>
                                            <View style={styles.right}>
                                                <Feather
                                                    name='chevron-right'
                                                    size={20}
                                                    color='gray'
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

