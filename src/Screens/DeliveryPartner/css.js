import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    header: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        width: 30,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    body: {
        
    },
    searchBar: {
        width: width/1.08 - 80,
        height: 40,
        borderRadius: 5,
        backgroundColor: '#EAEAF7',
        paddingLeft: 10
    },
    borderTitle: {
        width: width,
        backgroundColor: '#EAEAF7',
        paddingVertical: 10
    },
    title: {
        fontSize: 15,
        marginLeft: 10
    },
    logo: {
        width: 60,
        height: 60
    },
    row: {
        paddingVertical: 10,
        width: width,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    left: {
        width: 80,
        alignItems: 'center'
    },
    center: {
        width: width - 190,
        paddingLeft: 5
    },
    right: {
        width: 110,
    },
    topTitle: {
        fontSize: 17,
        fontWeight: 'bold',
    },
    codeTitle: {
        color: 'gray',
        marginBottom: 10
    },
    connectionTitle: {
        color: 'blue',
        fontSize: 16
    }
})