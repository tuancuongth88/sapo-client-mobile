import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableHighlight, TouchableWithoutFeedback, Image, FlatList } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import { width, height } from '../../Components/Dimensions/Dimensions';
import SearchBar from '../../Components/SearchBar/SearchBar';

export default function DelieveryPartner(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [switchSearch, setSwitchSearch] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const changeSearch = () => {
        setSwitchSearch(!switchSearch)
    }

    const openModalDeliveryGHN = () => {

    }

    const openModalDeliveryAhamove = () => {

    }

    const openModalDeliveryVTP = () => {

    }

    const renderItem = ({item}) => {
        return (
            <View></View>
        )
    }

    const onRefresh = () => {
        
    }

    const data = [

    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                {switchSearch ? 
                                    <View style={styles.searchBarWrapper}>
                                        <SearchBar
                                            placeholder='Tìm kiếm'
                                            backgroundColor='#EAEAF7'
                                            width={width/1.08 - 80}
                                            left={width/1.08 - 110}
                                            setKeyword={setKeyword}
                                        />
                                    </View>
                                        :
                                    <Text style={styles.headerTitle}>Đối tác</Text>
                                } 
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback onPress={() => changeSearch()}>
                                    {switchSearch ? 
                                        <Text style={{fontSize: 17}}>Hủy</Text>
                                          :
                                        <Feather
                                            name='search'
                                            size={25}
                                            color='#404040'
                                        />
                                    }
                                </TouchableWithoutFeedback>
                            </View>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.borderTitle}>
                                <Text style={styles.title}>Đối tác tích hợp</Text>
                            </View>
                            <View style={styles.integrationList}>
                                <TouchableHighlight
                                    onPress={() => openModalDeliveryGHN()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Image
                                                style={styles.logo}
                                                source={require('../../Assets/logoGHN.png')}
                                            />
                                        </View>
                                        <View style={styles.center}>
                                            <Text style={styles.topTitle}>Giao hàng nhanh</Text>
                                            <Text style={styles.codeTitle}>DSPGHN</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text style={styles.connectionTitle}>Chưa kết nối</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    onPress={() => openModalDeliveryAhamove()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Image
                                                style={styles.logo}
                                                source={require('../../Assets/ahamove.jpg')}
                                            />
                                        </View>
                                        <View style={styles.center}>
                                            <Text style={styles.topTitle}>Ahamove</Text>
                                            <Text style={styles.codeTitle}>AHAMOVE</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text style={styles.connectionTitle}>Chưa kết nối</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>

                                <TouchableHighlight
                                    onPress={() => openModalDeliveryVTP()}
                                    underlayColor='lavender'
                                >
                                    <View style={styles.row}>
                                        <View style={styles.left}>
                                            <Image
                                                style={styles.logo}
                                                source={require('../../Assets/viettelpost.jpg')}
                                            />
                                        </View>
                                        <View style={styles.center}>
                                            <Text style={styles.topTitle}>Viettel Post</Text>
                                            <Text style={styles.codeTitle}>DSPVTP</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text style={styles.connectionTitle}>Chưa kết nối</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={styles.borderTitle}>
                                <Text style={styles.title}>Đối tác tự liên hệ</Text>
                            </View>
                            <View style={styles.contactList}>
                                <FlatList
                                    style={styles.listContact}
                                    refreshing={refresh}
                                    onRefresh={onRefresh}
                                    data={data}
                                    renderItem={renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator ={false}
                                />  
                            </View>
                        </View>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

