import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040',
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    body: {
        backgroundColor: '#EAEAF7',
    },
    addressList: {
        width: width,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 120
    },
    element: {
        width: width,
        backgroundColor: '#fff',
        paddingVertical: 10,
        paddingHorizontal: 15,
        marginBottom: 15,
        borderColor: 'gray',
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5
    },
    customerName: {
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 10
    },
    phone: {
        fontSize: 15,
        color: 'gray',
    },
    address: {
        fontSize: 15,
        color: 'gray'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    left: {
        width: (width - 30) / 2
    },
    right: {
        width: (width - 30) / 2,
        alignItems: 'flex-end'
    }
})