import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import ModalAddressDetail from '../../Components/Modal/ModalAddressDetail/ModalAddressDetail';
import ModalAddAddress from '../../Components/Modal/ModalAddAddress/ModalAddAddress';

export default function AddressList(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalAddAddressVisible, setModalAddAddressVisible] = useState(false);
    const [modalAddressDetailVisible, setModalAddressDetailVisible] = useState(false);
    const [addressList, setAddressList] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [chosenAddress, setChosenAddress] = useState({});

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalAddAddress = () => {
        setModalAddAddressVisible(true)
    }

    const openModalAddressDetail = (item) => {
        setChosenAddress(item)
        setModalAddressDetailVisible(true) 
    }

    const renderItem = ({item}) => {
        return (
            <TouchableHighlight
                onPress={() => openModalAddressDetail(item)}
                underlayColor='lavender'
            >
                <View style={styles.element}>
                    <Text 
                        style={styles.customerName}
                        numberOfLines={1}
                    >
                        {item.customerName}
                    </Text>
                    <Text 
                        style={styles.phone}
                        numberOfLines={1}
                    >
                        {item.phone}
                    </Text>
                    <Text 
                        style={styles.address}
                        numberOfLines={1}
                    >
                        {item.address}
                    </Text>
                    <Text 
                        style={styles.address}
                        numberOfLines={1}
                    >
                        {item.ward}
                    </Text>
                    <Text 
                        style={styles.address}
                        numberOfLines={1}
                    >
                        {item.district}
                    </Text>
                    <View style={styles.row}>
                        <View style={styles.left}>
                            <Text 
                                style={styles.address}
                                numberOfLines={1}
                            >
                                {item.city}
                            </Text>
                        </View>
                        <View style={styles.right}>
                            <Icon
                                name='location-sharp'
                                size={20}
                                color='silver'
                            />
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

    const onRefresh = () => {
        
    }

    const data = [
        {customerName: 'Thắng', address: '19a đường bưởi', district: 'quận Tây Hồ', ward: 'Phường Bưởi', city: 'HN', phone: '0984563322'},
        {customerName: 'Long', address: '198 đê la thành', district: 'quận Đống Đa', ward: 'Phường Ô Chợ Dừa', city: 'HN', phone: '0984563322'}
    ]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Địa chỉ</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => openModalAddAddress()}
                                >
                                    <Feather
                                        name='plus'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                                {/* MODAL ADD ADDRESS */}
                                    <ModalAddAddress
                                        modalAddAddressVisible={modalAddAddressVisible}
                                        setModalAddAddressVisible={setModalAddAddressVisible}
                                    />
                                {/* MODAL ADD ADDRESS */}
                            </View>
                        </View>

                        <View style={styles.body}>
                            <FlatList
                                contentContainerStyle={styles.addressList}
                                // ref={(ref) => { this.flatListRef = ref; }}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={data}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                            />
                        </View> 

                        {/* MODAL ADDRESS DETAIL */}
                            <ModalAddressDetail
                                modalAddressDetailVisible={modalAddressDetailVisible}
                                setModalAddressDetailVisible={setModalAddressDetailVisible}
                                address={chosenAddress}
                            />
                        {/* MODAL ADDRESS DETAIL */} 
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

