import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    logo: {
        width: width/1.5,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 30,
    },
    logoImage: {
        width: width/1.5,
        height: 80,
    },
    input: {
        marginBottom: 10
    },
    gobackLoginScreen: {
        fontSize: 15,
        color: '#1C3FAA',
        fontWeight: 'bold'
    },
    registerBtn: {
        width: width/1.1,
        backgroundColor: '#1C3FAA',
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 10,
        marginBottom: 15,
        marginTop: 30
    },
    registerText: {
        color: '#fff',
        fontSize: 18
    },
})