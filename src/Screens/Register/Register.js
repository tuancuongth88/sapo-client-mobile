import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Image, Alert } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Input from '../../Components/Input/Input';
import { width, height } from '../../Components/Dimensions/Dimensions';
import postRegister from '../../Api/postRegister';
import AsyncStorage from '@react-native-community/async-storage'; 

export default function Register(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [shopName, setShopName] = useState('');
    const [provinceId, setProvinceId] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const gobackLoginScreen = () => {
        props.navigation.goBack();
    }

    const register = async() => {
        if(name == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập họ tên')
        }
        else if(phone == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập số điện thoại')
        }
        else if(email == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập email')
        }
        else if(shopName == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập tên cửa hàng')
        }
        else if(provinceId == '') {
            Alert.alert('Thông báo', 'Vui lòng chọn khu vực')
        }
        else if(password == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập mật khẩu')
        }
        else if(passwordConfirmation == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập mật khẩu xác nhận')
        }
        else {
            const res = await postRegister(
                name,
                phone,
                email,
                shopName,
                provinceId,
                password,
                passwordConfirmation
            )
            if(res.status == 200) {
                const resRegister = await res.json()
                if(resRegister.code == 200) {
                    Alert.alert(
                        'Thông báo',
                        'Đăng ký thành công',
                        [
                            {text: 'OK', onPress: async() => {
                                await AsyncStorage.setItem('userToken', resLogin.data.access_token);
                                props.navigation.navigate('BottomNavigation')
                            }}
                        ],
                    )
                }
                else if(resRegister.code == 422) {
                    Alert.alert('Lỗi !!!', resRegister.message)
                }
            }
            else if(res.status == 500) {
                Alert.alert('Lỗi !!!', 'Yêu cầu không thể thực hiện. Vui lòng đăng nhập lại !!!');
            }
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <ScrollView 
                        contentContainerStyle={styles.container}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.logo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../Assets/logo.png')}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Họ tên'
                                name='name'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setName}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Số điện thoại'
                                name='phone'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                keyboardType='phone-pad'
                                setValue={setPhone}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Email'
                                name='id'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setEmail}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Tên cửa hàng'
                                name='shopName'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setShopName}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Khu vực'
                                name='provinceId'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setProvinceId}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Mật khẩu'
                                name='password'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setPassword}
                                editable={true}
                            />
                        </View>

                        <View style={styles.input}>
                            <Input
                                placeholder='Xác nhận mật khẩu'
                                name='password_confirmation'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                setValue={setPasswordConfirmation}
                                editable={true}
                            />
                        </View>

                        <TouchableOpacity
                            style={styles.registerBtn}
                            onPress={() => register()}
                        >
                            <Text style={styles.registerText}>Đăng ký</Text>
                        </TouchableOpacity>

                        <TouchableWithoutFeedback
                            onPress={() => gobackLoginScreen()}
                        >
                            <Text style={styles.gobackLoginScreen}>Quay lại đăng nhập</Text>
                        </TouchableWithoutFeedback>
                    </ScrollView> 
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

