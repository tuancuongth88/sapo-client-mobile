import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    logo: {
        width: width/1.5,
        height: 120,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: height/8,
        marginBottom: 70,
    },
    logoImage: {
        width: width/1.5,
        height: 120,
    },
    phoneOrEmailInput: {
        marginBottom: 30
    },
    gobackLoginScreen: {
        fontSize: 15,
        color: '#1C3FAA',
        fontWeight: 'bold'
    },
    restoreBtn: {
        width: width/1.1,
        backgroundColor: '#1C3FAA',
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 10,
        marginBottom: 15
    },
    restoreText: {
        color: '#fff',
        fontSize: 18
    },
})