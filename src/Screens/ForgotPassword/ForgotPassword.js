import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Image, Alert } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Input from '../../Components/Input/Input';
import { width, height } from '../../Components/Dimensions/Dimensions';
import postRemindPassword from '../../Api/postRemindPassword';

export default function ForgotPassword(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [email, setEmail] = useState('');

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const gobackLoginScreen = () => {
        props.navigation.goBack();
    }

    const restorePassword = async() => {
        if(email == '') {
            Alert.alert('Thông báo', 'Vui lòng nhập email');
        }
        else {
            const res = await postRemindPassword(email);
            if(res.status == 200) {
                const resRestorePassword = await res.json();
                if(resRestorePassword.code == 200) {
                    Alert.alert('Thành công', resLogin.message);
                }
                else if(resRestorePassword.code == 401) {
                    Alert.alert('Lỗi !!!', resLogin.message);
                }
            }
            else if(res.status == 500) {
                 Alert.alert('Lỗi !!!', 'Yêu cầu không thể thực hiện. Vui lòng đăng nhập lại !!!');
            }
        }
       
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <ScrollView 
                        contentContainerStyle={styles.container}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.logo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../Assets/logo.png')}
                            />
                        </View>

                        <View style={styles.phoneOrEmailInput}>
                            <Input
                                placeholder='Email'
                                name='id'
                                borderWidth={width/1.1}
                                inputWidth={width/1.1 - 30}
                                iconDeleteLeft={width/1.15 - 25}
                                keyboardType='email-address'
                                setValue={setEmail}
                                editable={true}
                            />
                        </View>

                        <TouchableOpacity
                            style={styles.restoreBtn}
                            onPress={() => restorePassword()}
                        >
                            <Text style={styles.restoreText}>Khôi phục</Text>
                        </TouchableOpacity>

                        <TouchableWithoutFeedback
                            onPress={() => gobackLoginScreen()}
                        >
                            <Text style={styles.gobackLoginScreen}>Quay lại đăng nhập</Text>
                        </TouchableWithoutFeedback>
                    </ScrollView> 
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

