import React, { useState, useEffect, useRef } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, FlatList, Image, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen, fetchToken } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import SearchBar from '../../Components/SearchBar/SearchBar';
import NumberFormat from 'react-number-format';
import { width, height } from '../../Components/Dimensions/Dimensions';
import ModalCreateProduct from '../../Components/Modal/ModalCreateProduct/ModalCreateProduct';
import getProductList from '../../Api/getProductList';
import AsyncStorage from '@react-native-community/async-storage';

export default function ProductList(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [productList, setProductList] = useState([]);
    const [refresh, setRefresh] = useState(false)
    const [modalCreateNewProductVisible, setModalCreateNewProductVisible] = useState(false);
    const [switchSearch, setSwitchSearch] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [userToken, setUserToken] = useState('');
    const [page, setPage] = useState(1);
    const [itemLoading, setItemLoading] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const flatListRef = useRef()
    // const isFirstRender = useRef(true);

    useEffect(() => {
        NetInfo.addEventListener(async(state) => {
            setIsInternetReachable(state.isInternetReachable);
            if(state.isInternetReachable) {
                const token = await fetchToken()
                setUserToken(token)
                getProductListFunc(token, productList, page)
                setItemLoading(true)
                setLoadMore(true)
                setRefresh(false)
                // isFirstRender.current = false
            }
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const getProductListFunc = async(token, productList, page) => { 
        const res = await getProductList(token, page);
        if(res.status == 200) {
            const resGetProductList = await res.json(); 
            if(resGetProductList.code == 200) {
                const arrTmp = [...productList,...resGetProductList.data.result]
                if(arrTmp.length == productList.length) {
                    setItemLoading(false)
                    setLoadMore(false)
                }
                else if(arrTmp.length > productList.length) {
                    setProductList([...arrTmp])
                }
            }
            else if(resGetProductList.code == 401) {
                Alert.alert('Thông báo', 'Phiên làm việc của bạn đã hết hạn. Vui lòng đăng nhập lại !!!');
                AsyncStorage
                .clear()
                .then(() => props.navigation.navigate('LoginNavigation'))
            }
            else if(resGetProductList.code == 403) {
                Alert.alert('Lỗi !!!', resGetProductList.message);
            }
            else if(resGetProductList.code == 422) {
                Alert.alert('Lỗi !!!', resGetProductList.message);
            }
            else if(resGetProductList.code == 500) {
                Alert.alert('Lỗi !!!', resGetProductList.message);
                console.log(resGetProductList.message_content)
            }
        }
        else if(res.status == 500) {
            Alert.alert('Lỗi !!!', 'Yêu cầu không thể thực hiện. Vui lòng đăng nhập lại !!!');
        }
    }
    
    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const navigateToProductDetailScreen = (product) => {
        props.navigation.navigate('ProductDetail', {
            product: product
        })
    }

    const openModalCreateNewProduct = () => {
        setModalCreateNewProductVisible(true)
    }

    const swtichToSearch = () => {
        setSwitchSearch(true)
    }

    const switchToShowHeader = () => {
        setSwitchSearch(false)
    }

    const renderItem = ({item}) => {
        return (
            <TouchableWithoutFeedback
                onPress={() => navigateToProductDetailScreen(item)}
            >
                <View style={styles.elementWrapper}>
                    <Image
                        source={require('../../Assets/starry_sky.jpg')}
                        style={styles.photo}
                    />
                    <Text 
                        style={styles.productName}
                        numberOfLines={1}
                    >
                        {item.title.toUpperCase()}
                    </Text>
                    <View style={styles.priceWrapper}>
                        <NumberFormat
                            value={item.retail_price}
                            thousandSeparator={','}
                            prefix={'đ '}
                            displayType={'text'}
                            renderText={value => <Text style={styles.money}>{value}</Text>}
                        />
                    </View>
                    <View style={styles.productCode}>
                        <Text style={styles.code}>{item.sku}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    const renderFooter = () => {
        return( 
            <View style={styles.itemLoader}>
                <ActivityIndicator size='large'/>
            </View>
        )
    }

    const handleLoadMore = () => {
        setPage(page + 1)
        const nextPage = page + 1
        getProductListFunc(userToken, productList, nextPage)
    }

    const onRefresh = () => {
        getProductListFunc(userToken, [], 1)
        setProductList([])
        setPage(1)
        setLoadMore(true)
        setItemLoading(true)    
    }

    // useEffect(() => { 
    //     if(!isFirstRender.current) { console.log(page)
    //         getProductListFunc(userToken, page)
    //     }
    // }, [page]);

    const scrollToTop = () => {
        if(productList.length > 0) {
            flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
        } 
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        {switchSearch ?
                            <View style={styles.header}>
                                <View style={styles.headerSearchLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerSearchCenter}>
                                    <SearchBar
                                        placeholder='Tìm kiếm'
                                        backgroundColor='#EAEAF7'
                                        width={width/1.08 - 80}
                                        left={width/1.08 - 110}
                                        setKeyword={setKeyword}
                                    />
                                </View>
                                <View style={styles.headerSearchRight}>
                                    <TouchableWithoutFeedback onPress={() => switchToShowHeader()}>
                                        <Text style={{fontSize: 16}}>Hủy</Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                                :
                            <View style={styles.header}>  
                                <View style={styles.headerLeft}>
                                    <TouchableWithoutFeedback
                                        onPress={() => goBackPreviousScreen()}
                                    >
                                        <Feather
                                            name='arrow-left'
                                            size={30}
                                            color='#404040'
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={styles.headerCenter}>
                                    <Text style={styles.headerTitle}>Sản phẩm</Text>
                                </View>
                                <View style={styles.headerRight}>
                                    <View style={styles.iconPlusWrapper}>
                                        <TouchableOpacity
                                            onPress={() => openModalCreateNewProduct()}
                                        >
                                            <Feather
                                                name='plus'
                                                size={30}
                                                color='#404040'
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.iconSearchWrapper}>
                                        <TouchableWithoutFeedback
                                            onPress={() => swtichToSearch()}
                                        >
                                            <Feather
                                                name='search'
                                                size={25}
                                                color='#404040'
                                            />
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            </View>
                        }

                        <View style={styles.body}>
                            <FlatList
                                contentContainerStyle={styles.productList}
                                ref={flatListRef}
                                refreshing={refresh}
                                onRefresh={onRefresh}
                                data={productList}
                                renderItem={renderItem}
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                                showsVerticalScrollIndicator ={false}
                                onEndReached={loadMore ? handleLoadMore : null}
                                onEndReachedThreshold={0.1}
                                ListFooterComponent={itemLoading ? renderFooter : null}
                            />
                            <TouchableOpacity
                                style={styles.scrollToTop}
                                onPress={() => scrollToTop()}
                            >
                                <Fontisto
                                    name='angle-dobule-up'
                                    size={35}
                                    color='#fff'
                                />
                            </TouchableOpacity>
                        </View>
                        {/* MODAL CREATE NEW PRODUCT */}
                            <ModalCreateProduct
                                modalCreateNewProductVisible={modalCreateNewProductVisible}
                                setModalCreateNewProductVisible={setModalCreateNewProductVisible}
                                userToken={userToken}
                                onRefresh={onRefresh}
                            />
                        {/* MODAL CREATE NEW PRODUCT */}
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

