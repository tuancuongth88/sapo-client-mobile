import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const scrollToTopSize = 50;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerLeft: {
        paddingRight: 10,
        width: 70,
        height: 40,
        justifyContent: 'center'
    },
    headerRight: {
        alignItems: 'center',
        width: 70,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center'
    },
    headerCenter: {
        width: width/1.08 - 140,
        alignItems: 'center',
        height: 40,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    iconPlusWrapper: {
        width: 35,
        alignItems: 'center'
    },
    iconSearchWrapper: {
        width: 35,
        alignItems: 'flex-end'
    },
    headerSearchLeft: {
        width: 40,
    },
    headerSearchCenter: {
        width: width/1.08 - 70
    },
    headerSearchRight: {
        width: 30,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    productList: {
        width: width,
        paddingBottom: 120
    },
    elementWrapper: {
        width: width/2 - 10,
        height: 250,
        borderWidth: 0.5,
        marginHorizontal: 5,
        marginVertical: 10,
        borderColor: 'gray',
        alignItems: 'center',
    },
    photo: {
        width: width/2 - 10,
        height: 140,
        marginBottom: 10,
    },
    productName: {
        marginHorizontal: 10,
        fontWeight: 'bold',
        fontSize: 17,
        marginBottom: 20,
        color: '#1C3FAA'
    },
    priceWrapper: {
        width: width/2 - 10,
        marginBottom: 5
    },
    money: {
        textAlign: 'center',
        color: 'tomato',
        fontSize: 16,
        fontFamily: 'sans-serif-medium',
    },
    productCode: {
        width: width/2 - 10,
    },
    code: {
        textAlign: 'center',
        fontSize: 15,
        color: 'gray'
    },
    scrollToTop: {
        position: 'absolute',
        width: scrollToTopSize,
        height: scrollToTopSize,
        borderRadius: 10,
        backgroundColor: '#fdab64',
        justifyContent: 'center',
        alignItems: 'center',
        top: height/1.40,
        left: width/1.15,
        opacity: 0.6
    },
})