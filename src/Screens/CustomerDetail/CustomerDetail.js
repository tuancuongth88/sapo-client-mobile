import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalEditCustomer from '../../Components/Modal/ModalEditCustomer/ModalEditCustomer';

export default function CustomerDetail(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalEditCustomerVisible, setModalEditCustomerVisible] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalEditCustomer = () => {
        setModalEditCustomerVisible(true)
    }

    const goToDetailInfoScreen = () => {
        props.navigation.navigate('CustomerInfoDetail')
    }

    const goToAddressListScreen = () => {
        props.navigation.navigate('AddressList')
    }

    const goToContactPeopleListScreen = () => {
        props.navigation.navigate('ContactPeopleList')
    }

    const goToOrderListScreen = () => {
        props.navigation.navigate('OrderList')
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                        style={styles.iconGoPreviousScreen}
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Chi tiết khách hàng</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => openModalEditCustomer()}
                                >
                                    <MaterialCommunityIcon
                                        name='pencil-outline'
                                        size={25}
                                        color='#404040'
                                        style={styles.iconEdit}
                                    />
                                </TouchableWithoutFeedback>
                                {/* MODAL EDIT CUSTOMER */}
                                    <ModalEditCustomer
                                        modalEditCustomerVisible={modalEditCustomerVisible}
                                        setModalEditCustomerVisible={setModalEditCustomerVisible}
                                    />
                                {/* MODAL EDIT CUSTOMER */}
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <TouchableHighlight
                                onPress={() => goToDetailInfoScreen()}
                                underlayColor='lavender'
                            >
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowTitle}>Thông tin chi tiết</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={22}
                                            color='gray'
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={() => goToAddressListScreen()}
                                underlayColor='lavender'
                            >
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowTitle}>Địa chỉ</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={22}
                                            color='gray'
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={() => goToContactPeopleListScreen()}
                                underlayColor='lavender'
                            >
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowTitle}>Người liên hệ</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={22}
                                            color='gray'
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight
                                onPress={() => goToOrderListScreen()}
                                underlayColor='lavender'
                            >
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowTitle}>Lịch sử mua hàng</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Feather
                                            name='chevron-right'
                                            size={22}
                                            color='gray'
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>
                        </ScrollView>  
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

