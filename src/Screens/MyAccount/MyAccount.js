import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, Image, TouchableWithoutFeedback, TouchableOpacity, RefreshControl } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

export default function MyAccount(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => {
          setTimeout(resolve, timeout);
        });
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(2000).then(() => setRefreshing(false));
    }, []);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const navigateToAccountInfoScreen = () => {
        props.navigation.navigate('AccountInformation')
    }

    const navigateToAccountManagementScreen = () => {
        props.navigation.navigate('AccountManagement')
    }

    const navigateToHelpCenterScreen = () => {
        props.navigation.navigate('HelpCenter')
    }

    const navigateToCustomerListScreen = () => {
        props.navigation.navigate('CustomerList')
    }

    const logOut = () => {
        AsyncStorage
        .clear()
        .then(() => {
            props.navigation.navigate('LoginNavigation')
        })
    }

    const navigateToChangePasswordScreen = () => {
        props.navigation.navigate('ChangePassword')
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <ScrollView 
                        contentContainerStyle={styles.container}
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    >
                        <View style={styles.header}>
                            <Text style={styles.headerTitle}>Tài khoản</Text>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.avatarAndName}>
                                <TouchableWithoutFeedback
                                    onPress={() => navigateToAccountInfoScreen()}
                                >
                                    <Image 
                                        style={styles.avatarHeader}
                                        source={require('../../Assets/avatar.png')}
                                    />
                                </TouchableWithoutFeedback>
                                <View style={styles.accountName}>
                                    <Text style={styles.name}>nguyen chien thang</Text>
                                    <Text style={styles.email}>chienthanga2.ftu@gmail.com</Text>
                                </View>
                            </View>
                                
                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToAccountManagementScreen()}    
                            >
                                <Feather
                                    name='edit'
                                    size={35}
                                    color='#000'
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Quản lý tài khoản liên kết</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToChangePasswordScreen()}    
                            >
                                <Feather
                                    name='lock'
                                    size={35}
                                    color='#000'
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Đổi mật khẩu</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToCustomerListScreen()}    
                            >
                                <Feather
                                    name='users'
                                    size={35}
                                    color='#000'
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Khách hàng</Text>
                            </TouchableOpacity>
                            
                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => navigateToHelpCenterScreen()}    
                            >
                                <Feather
                                    name='alert-circle'
                                    size={35}
                                    color='#000'
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Giúp đỡ</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                style={styles.accountConfigElement}
                                onPress={() => logOut()}    
                            >
                                <Feather
                                    name='log-out'
                                    size={35}
                                    color='#000'
                                    style={styles.accountConfigElementIcon}
                                />
                                <Text style={styles.accountConfigElementTitle}>Đăng xuất</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>  
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

