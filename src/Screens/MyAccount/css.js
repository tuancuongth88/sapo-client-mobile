import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    header: {
        width: width/1.08,
        height: 60,
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    avatarAndName: {
        width: width/1.08,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 50
    },
    avatarHeader: {
        width: 40, 
        height: 40
    },
    accountName: {
        marginLeft: 10
    },
    name: {
        fontWeight: 'bold',
        fontSize: 16
    },
    email: {
        color: 'gray',
        fontSize: 13
    },
    accountConfigElement: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width/1.08,
        height: 60,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 7
        },
        shadowRadius: 7,
        shadowOpacity: .5,
        elevation: 12,
        marginBottom: 25
    },
    accountConfigElementIcon: {
        marginLeft: 20
    },
    accountConfigElementTitle: {
        marginLeft: 15,
        fontSize: 18
    }
})