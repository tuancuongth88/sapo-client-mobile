import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040',
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    body: {
        
    },
    border: {
        width: width,
        backgroundColor: '#fff',
    },
    row: {
        width: width,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderColor: 'gray',
        borderBottomWidth: 0.5
    },
    left: {
        width: width/2,
        paddingLeft: 20
    },
    right: {
        width: width/2,
        paddingRight: 15,
        alignItems: 'flex-end'
    },
    rowTitle: {
        fontSize: 16,
        color: '#404040'
    },
    bottom: {
        width: width,
        backgroundColor: '#fff',
        paddingTop: 10,
        paddingBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#1C3FAA',
        borderTopWidth: 0.5
    },
    bottomElement: {
        width: width/5,
    }
})