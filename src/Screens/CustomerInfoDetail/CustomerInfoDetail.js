import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Linking, Alert } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalEditCustomer from '../../Components/Modal/ModalEditCustomer/ModalEditCustomer';
import Geolocation from '@react-native-community/geolocation';

export default function CustomerInfoDetail(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalEditCustomerVisible, setModalEditCustomerVisible] = useState(false);
    const [location, setLocation] = useState(null);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
            // Geolocation.getCurrentPosition(info => setLocation(info));
            Geolocation.getCurrentPosition((success)=>{setLocation(success)}, (e)=>{console.log(e)}, {timeout: 2000});
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalEditCustomer = () => {
        setModalEditCustomerVisible(true)
    }

    const deleteCustomer = () => {
        Alert.alert(
            'Xóa khách hàng',
            'Bạn có chắc chắn muốn xóa khách hàng này?',
            [
                {text: 'Thoát', style: 'cancel'},
                {text: 'Xóa', onPress: () => {}}
            ],
            {cancelable: true}
        )
    }

    const messageToCustomer = () => {
        Linking.openURL('sms:' + '0985561142')
    }

    const callCustomer = () => {
        Linking.openURL('tel:' + '0985561142')
    }

    const mailToCustomer = () => {
        Linking.openURL('mailto:' + 'vanminh@gmail.com')
    }

    const openGGMap = () => {
        Linking
            .openURL('http://maps.google.com/maps?daddr=' + location.coords.latitude + ',' + location.coords.longitude)
            .catch(err => console.error('An error occurred', err));
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                        style={styles.iconGoPreviousScreen}
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Thông tin chi tiết</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => openModalEditCustomer()}
                                >
                                    <MaterialCommunityIcon
                                        name='pencil-outline'
                                        size={25}
                                        color='#404040'
                                        style={styles.iconEdit}
                                    />
                                </TouchableWithoutFeedback>
                                {/* MODAL EDIT CUSTOMER */}
                                    <ModalEditCustomer
                                        modalEditCustomerVisible={modalEditCustomerVisible}
                                        setModalEditCustomerVisible={setModalEditCustomerVisible}
                                    />
                                {/* MODAL EDIT CUSTOMER */}
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={[styles.border, {marginBottom: 30}]}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Tên khách hàng</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            Thắng
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Mã khách hàng</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            CUS00001
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Nhóm khách hàng</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            Bán lẻ
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Giới tính</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            Nam
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Ngày sinh</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            28/10/1987
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Thẻ</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            
                                        </Text>
                                    </View>
                                </View>
                            </View>   

                            <View style={[styles.border, {marginBottom: 30, borderColor: 'gray', borderTopWidth: 0.5}]}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Số điện thoại</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            0913564482
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Số fax</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Website</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Email</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            chienvan@gmail.com
                                        </Text>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.border, {borderColor: 'gray', borderTopWidth: 0.5}]}>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Chiết khẩu</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            0%
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.left}>
                                        <Text style={styles.rowLeftText}>Phương thức thanh toán</Text>
                                    </View>
                                    <View style={styles.right}>
                                        <Text 
                                            style={styles.rowRightText}
                                            numberOfLines={1}
                                        >
                                            
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>  

                        <View style={styles.bottom}>
                            <View style={[styles.bottomElement, {paddingLeft: 25}]}>
                                <TouchableOpacity onPress={() => mailToCustomer()}>
                                    <Icon
                                        name='mail-open-outline'
                                        size={30}
                                        color='#1C3FAA'
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.bottomElement, {alignItems: 'center'}]}>
                                <TouchableOpacity onPress={() => openGGMap()}>
                                    <Icon
                                        name='location-outline'
                                        size={30}
                                        color='#1C3FAA'
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.bottomElement, {alignItems: 'center'}]}>
                                <TouchableOpacity onPress={() => messageToCustomer()}>
                                    <Icon
                                        name='chatbox-ellipses-outline'
                                        size={30}
                                        color='#1C3FAA'
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.bottomElement, {alignItems: 'center'}]}>
                                <TouchableOpacity onPress={() => callCustomer()}>
                                    <Icon
                                        name='call-outline'
                                        size={30}
                                        color='#1C3FAA'
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.bottomElement, {paddingRight: 30, alignItems: 'flex-end'}]}>
                                <TouchableOpacity onPress={() => deleteCustomer()}>
                                    <Icon
                                        name='remove-circle-outline'
                                        size={30}
                                        color='red'
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

