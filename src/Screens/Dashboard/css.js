import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginBottom: 50
    },
    header: {
        width: width/1.08,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    headerLeft: {
        width: (width/1.08)/2,
    },
    headerRight: {
        width: (width/1.08)/2 - 10,
        alignItems: 'flex-end'
    },
    body: {
        alignItems: 'center',
        paddingVertical: 30,
    },
    reportElement: {
        borderWidth: 0.5,
        width: width/1.15,
        borderColor: 'gray',
        borderRadius: 10,
        height: 100,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    reportElementLeft: {
        width: (width/1.15)/2 - 20,
        height: 100
    },
    reportElementRight: {
        width: (width/1.15)/2 - 20,
        alignItems: 'flex-end',
        height: 100
    },
    itemSaleIcon: {
        marginTop: 10,
    },
    reportTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'gray'
    },
    reportStatus: {
        width: 55,
        height: 25,
        backgroundColor: '#91C714',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: 10
    },
    odd: {
        color: '#fff'
    },
    arrowUp: {
        marginLeft: 2
    },
    reportTitleAmount: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#404040'
    }
})