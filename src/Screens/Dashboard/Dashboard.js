import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, RefreshControl } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Icon from 'react-native-vector-icons/Ionicons';

export default function Dashboard(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [refreshing, setRefreshing] = React.useState(false);

    const wait = (timeout) => {
        return new Promise(resolve => {
          setTimeout(resolve, timeout);
        });
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(2000).then(() => setRefreshing(false));
    }, []);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const navigateToNotificationsScreen = () => {
        props.navigation.navigate('Notifications')
    }
    
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <Text style={styles.headerTitle}>Dashboard</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => navigateToNotificationsScreen()}
                                >
                                    <Icon
                                        name='notifications-outline'
                                        size={30}
                                        color='gray'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                        </View>

                        <ScrollView
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            }
                        >
                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='cart-outline'
                                        size={40}
                                        color='#1C3FAA'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>Item Sales</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>33%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>4.510</Text>
                                </View>
                            </View>

                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='card-outline'
                                        size={40}
                                        color='tomato'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>New Orders</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>2%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>3.521</Text>
                                </View>
                            </View>

                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='tv-outline'
                                        size={40}
                                        color='orange'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>Total Products</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>12%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>2.145</Text>
                                </View>
                            </View>

                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='person-outline'
                                        size={40}
                                        color='#91C714'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>Unique Visitor</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>22%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>152.000</Text>
                                </View>
                            </View>

                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='person-outline'
                                        size={40}
                                        color='#91C714'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>Unique Visitor</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>22%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>152.000</Text>
                                </View>
                            </View>

                            <View style={styles.reportElement}>
                                <View style={styles.reportElementLeft}>
                                    <Icon
                                        name='person-outline'
                                        size={40}
                                        color='#91C714'
                                        style={styles.itemSaleIcon}
                                    />
                                    <Text style={styles.reportTitle}>Unique Visitor</Text>
                                </View>
                                <View style={styles.reportElementRight}>
                                    <View style={styles.reportStatus}>
                                        <Text style={styles.odd}>22%</Text>
                                        <Icon
                                            name='chevron-up'
                                            size={18}
                                            color='#fff'
                                            style={styles.arrowUp}
                                        />
                                    </View>
                                    <Text style={styles.reportTitleAmount}>152.000</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View> 
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

