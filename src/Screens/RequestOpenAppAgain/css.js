import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    }
});

export { styles };