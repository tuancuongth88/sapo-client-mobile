import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    header: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040'
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    body: {
        backgroundColor: '#EAEAF7',
        minHeight: height,
        width: width,
        alignItems: 'center',
        paddingBottom: 100
    },
    bodyInside: {
        width: width/1.05
    },
    borderTitle: {
        paddingVertical: 10,
        width: width/1.05,
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    border: {
        backgroundColor: '#fff',
        borderRadius: 10,
        width: width/1.05,
        borderColor: 'gray',
        borderWidth: 0.5,
        padding: 10
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    element: {
        width: (width/1.05 - 40)/3,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20,
        borderRadius: 10,
        borderColor: 'gray',
        borderWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginLeft: 10,
    },
    firstElement: {
        width: (width/1.05 - 40)/3,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20,
        borderRadius: 10,
        borderColor: 'gray',
        borderWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    elementSpecial: {
        width: (width/1.05 - 40)/3,
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 5,
        borderRadius: 10,
        borderColor: 'gray',
        borderWidth: 0.5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginLeft: 10,
    },
    logo: {
        marginBottom: 5
    },
    amount: {
        marginBottom: 5,
        fontSize: 22,
        fontWeight: 'bold'
    },
    note: {
        color: 'gray',
        fontSize: 11
    },
    borderNote: {
        color: 'gray',
        fontSize: 13
    },
    leftTitle: {
        width: (width/1.05)/2,
    },
    rightTitle: {
        width: (width/1.05)/2,
        alignItems: 'flex-end'
    },
    filterPartner: {
        color: 'blue'
    },
    leftRow: {
        width: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightRow: {
        width: width/1.05 - 100,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        marginLeft: 10
    },
    blue: {
        width: 50,
        height: 50,
        borderRadius: 10,
        backgroundColor: 'lightblue'
    },
    top: {
        marginBottom: 5,
        fontSize: 18,
        fontWeight: 'bold'
    },
    bottom: {
        fontSize: 13,
        color: 'gray'
    },
    rowNumber: {
        width: (width/1.05 - 100)/2,
        alignItems: 'flex-end',
    },
    rowTitle: {
        width: (width/1.05 - 100)/2,
    },
    tomato: {
        width: 50,
        height: 50,
        borderRadius: 10,
        backgroundColor: '#FFCCFF'
    },
    purple: {
        width: 50,
        height: 50,
        borderRadius: 10,
        backgroundColor: '#E5CCFF'
    }
})