import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalScrollList from '../../Components/Modal/ModalScrollList/ModalScrollList';
import ModalCODDefine from '../../Components/Modal/ModalCODDefine/ModalCODDefine';

export default function TransportOverview(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalDeliveryPartnerVisible, setModalDeliveryPartnerVisible] = useState(false);
    const [partnerList, setPartnerList] = useState([]);
    const [modalCODDefineVisible, setModalCODDefineVisible] = useState(false);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const goToDeliveryManagementScreen = () => {
        props.navigation.navigate('DeliveryManagement')
    }

    const openModalFilterDeliveryPartner = () => {
        setModalDeliveryPartnerVisible(true)
    }

    const openModalCODDefine = () => {
        setModalCODDefineVisible(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Tổng quan vận chuyển</Text>
                            </View>
                            <View style={styles.headerRight}>
                                
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >   
                            <View style={styles.bodyInside}>
                                <View style={styles.borderTitle}>
                                    <Text style={styles.title}>Tình trạng giao hàng</Text>
                                </View>
                                <View style={styles.border}>
                                    <View style={styles.row}>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.firstElement}>
                                                <MaterialCommunityIcon
                                                    name='dropbox'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Chưa đóng gói</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.element}>
                                                <MaterialCommunityIcon
                                                    name='cube'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Chờ lấy hàng</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.element}>
                                                <MaterialCommunityIcon
                                                    name='truck-fast'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Đang giao hàng</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>

                                    <View style={styles.row}>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.firstElement}>
                                                <MaterialCommunityIcon
                                                    name='bus-clock'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Chờ giao lại</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.elementSpecial}>
                                                <MaterialCommunityIcon
                                                    name='account-clock'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Hủy giao</Text>
                                                <Text style={styles.note}>Chờ nhận</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                        <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                            <View style={styles.elementSpecial}>
                                                <MaterialCommunityIcon
                                                    name='briefcase-clock'
                                                    size={30}
                                                    color='#1C3FAA'
                                                    style={styles.logo}
                                                />
                                                <Text style={styles.amount}>0</Text>
                                                <Text style={styles.label}>Hủy giao</Text>
                                                <Text style={styles.note}>Đã nhận</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                    <Text style={styles.borderNote}>* Dữ liệu cập nhật theo 30 ngày gần nhất</Text>
                                </View>

                                <View style={styles.borderTitle}>
                                    <View style={styles.leftTitle}>
                                        <TouchableWithoutFeedback onPress={() => openModalCODDefine()}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.title}>THU HỘ COD</Text>
                                                <Icon
                                                    name='information-circle'
                                                    size={20}
                                                    color='#1C3FAA'
                                                    style={{marginLeft: 5}}
                                                />
                                            </View>
                                        </TouchableWithoutFeedback>
                                        {/* MODAL COD DEFINE */}
                                            <ModalCODDefine
                                                modalCODDefineVisible={modalCODDefineVisible}
                                                setModalCODDefineVisible={setModalCODDefineVisible}
                                            />
                                        {/* MODAL COD DEFINE */}
                                    </View>
                                    <View style={styles.rightTitle}>
                                        <TouchableWithoutFeedback onPress={() => openModalFilterDeliveryPartner()}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text style={styles.filterPartner}>Tất cả đối tác</Text>
                                                <Feather
                                                    name='chevron-down'
                                                    size={20}
                                                    color='blue'
                                                    style={{marginLeft: 5}}
                                                />
                                            </View>
                                        </TouchableWithoutFeedback>
                                        {/* MODAL FILTER PARTNER */}
                                            <ModalScrollList
                                                modalScrollListVisible={modalDeliveryPartnerVisible}
                                                setModalScrollListVisible={setModalDeliveryPartnerVisible}
                                                list={partnerList}
                                                listTitle='Đối tác vận chuyển'
                                            />
                                        {/* MODAL FILTER PARTNER */}
                                    </View>
                                </View>
                                <View style={styles.border}>
                                    <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                        <View style={styles.row}>
                                            <View style={styles.leftRow}>
                                                <View style={styles.blue}></View>
                                            </View>
                                            <View style={styles.rightRow}>
                                                <View style={styles.rowTitle}>
                                                    <Text style={styles.top}>Đang thu hộ</Text>
                                                    <Text style={styles.bottom}>Vận đơn: 0</Text>
                                                </View>
                                                <View style={styles.rowNumber}>
                                                    <Text style={styles.top}>0</Text>
                                                    <Text style={styles.bottom}>Tổng phí: 0</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>

                                    <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                        <View style={styles.row}>
                                            <View style={styles.leftRow}>
                                                <View style={styles.tomato}></View>
                                            </View>
                                            <View style={styles.rightRow}>
                                                <View style={styles.rowTitle}>
                                                    <Text style={styles.top}>Chờ đối soát</Text>
                                                    <Text style={styles.bottom}>Vận đơn: 0</Text>
                                                </View>
                                                <View style={styles.rowNumber}>
                                                    <Text style={styles.top}>0</Text>
                                                    <Text style={styles.bottom}>Tổng phí: 0</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>

                                    <TouchableWithoutFeedback onPress={() => goToDeliveryManagementScreen()}>
                                        <View style={styles.row}>
                                            <View style={styles.leftRow}>
                                                <View style={styles.purple}></View>
                                            </View>
                                            <View style={styles.rightRow}>
                                                <View style={styles.rowTitle}>
                                                    <Text style={styles.top}>Đã đối soát</Text>
                                                    <Text style={styles.bottom}>Vận đơn: 0</Text>
                                                </View>
                                                <View style={styles.rowNumber}>
                                                    <Text style={styles.top}>0</Text>
                                                    <Text style={styles.bottom}>Tổng phí: 0</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <Text style={styles.borderNote}>* Dữ liệu cập nhật theo 30 ngày gần nhất</Text>
                                </View>

                                <View style={styles.borderTitle}>
                                    <Text style={styles.title}>Chỉ số vận chuyển</Text>
                                </View>
                                <View style={styles.border}>
                                    
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

