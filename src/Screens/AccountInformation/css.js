import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

const avatarSIZE = 180;

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    body: {
        alignItems: 'center',
    },
    header: {
        width: width/1.08,
        height: 30,
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 15
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#000',
        marginLeft: 15,
        marginTop: 15,
        color: '#404040'
    },
    iconGoPreviousScreen: {
        marginTop: 15
    },
    avatarWrapper: {
        width: avatarSIZE,
        height: avatarSIZE,
        borderRadius: avatarSIZE,
        borderWidth: 1,
        borderColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 10
    },
    avatar: {
        width: avatarSIZE - 8,
        height: avatarSIZE - 8,
        borderRadius: avatarSIZE - 8,
    },
    accountName: {
        marginBottom: 15
    },
    email: {
        marginBottom: 15
    },
    phone: {
        marginBottom: 30
    },
    inputTitle: {
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 3
    },
    changeInfoBtn: {
        width: width/1.1,
        backgroundColor: '#1C3FAA',
        paddingVertical: 10,
        alignItems: 'center',
        marginBottom: 70,
        borderRadius: 10
    },
    changeInfoTitle: {
        color: '#fff',
        fontSize: 18
    }
})