import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, ScrollView, Image } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import Input from '../../Components/Input/Input';
import { width, height } from '../../Components/Dimensions/Dimensions';
import ModalChooseImages from '../../Components/Modal/ModalChooseImages/ModalChooseImages';

export default function AccountInformation(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [modalChooseImagesVisible, setModalChooseImagesVisible] = useState(false);
    const [avatar, setAvatar] = useState(null);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const openModalChooseImages = () => {
        setModalChooseImagesVisible(true)
    }

    const getValue = (name, value) => {
        
    }

    const changeAccountInfo = () => {

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ?
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <TouchableWithoutFeedback
                                onPress={() => goBackPreviousScreen()}
                            >
                                <Feather
                                    name='arrow-left'
                                    size={30}
                                    style={styles.iconGoPreviousScreen}
                                    color='#404040'
                                />
                            </TouchableWithoutFeedback>
                            <Text style={styles.headerTitle}>Thông tin người dùng</Text>
                        </View>
                        
                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            <TouchableWithoutFeedback
                                onPress={() => openModalChooseImages()}
                            >
                                <View style={styles.avatarWrapper}>
                                    <Image
                                        source={avatar == null ? require('../../Assets/avatar.png') : {uri: avatar.path}}
                                        style={styles.avatar}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                            {/* MODAL CHOOSE IMAGE */}
                                <ModalChooseImages
                                    modalChooseImagesVisible={modalChooseImagesVisible}
                                    setModalChooseImagesVisible={setModalChooseImagesVisible}
                                    avatar={avatar}
                                    setAvatar={setAvatar}
                                />
                            {/* MODAL CHOOSE IMAGE */}

                            <View style={styles.accountName}>
                                <Text style={styles.inputTitle}>Tên tài khoản</Text>
                                <Input
                                    placeholder='Tên tài khoản'
                                    name='name'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.email}>
                                <Text style={styles.inputTitle}>Email</Text>
                                <Input
                                    placeholder='Email'
                                    name='email'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                />
                            </View>

                            <View style={styles.phone}>
                                <Text style={styles.inputTitle}>Số điện thoại</Text>
                                <Input
                                    placeholder='Số điện thoại'
                                    name='phone'
                                    borderWidth={width/1.1}
                                    inputWidth={width/1.1 - 30}
                                    iconDeleteLeft={width/1.15 - 25}
                                    editable={true}
                                    keyboardType='phone-pad'
                                />
                            </View>

                            <TouchableWithoutFeedback
                                onPress={() => changeAccountInfo()}
                            >
                                <View style={styles.changeInfoBtn}>
                                    <Text style={styles.changeInfoTitle}>Cập nhật</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </ScrollView>
                    </View> 
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

