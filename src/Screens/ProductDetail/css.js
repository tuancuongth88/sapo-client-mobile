import { StyleSheet } from 'react-native';
import { width, height } from '../../Components/Dimensions/Dimensions';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#EAEAF7'
    },
    header: {
        width: width,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 5,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    headerTitle: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#404040',
    },
    headerLeft: {
        width: 30,
        alignItems: 'center',
    },
    headerCenter: {
        width: width/1.08 - 60,
        alignItems: 'center',
    },
    headerRight: {
        width: 30,
        alignItems: 'center'
    },
    body: {
        alignItems: 'center',
        paddingBottom: 20,
        paddingTop: 10
    },
    productImageWrapper: {
        width: width,
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    addImage: {
        width: 60,
        height: 60,
        borderRadius: 5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'silver',
        borderWidth: 0.5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
    },
    productImageScroll: {
        minWidth: width - 90
    },
    productImage: {
        width: 60,
        height: 60,
        borderRadius: 5, 
    },
    productImageElement: {
        marginLeft: 10,
    },
    deleteImg: {
        position: 'absolute',
        width: 60,
        height: 60,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 100, 150, 0.5)',
    },
    redline: {
        width: 30,
        height: 10,
        borderRadius: 3,
        backgroundColor: 'red'
    },
    productNameWrapper: {
        width: width - 30,
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginBottom: 15
    },
    productName: {
        fontSize: 16
    },
    moreInfoWrapper: {
        alignItems: 'center',
        width: width - 30,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginBottom: 15,
        paddingVertical: 10
    },
    moreInfoTitleRow: {
        width: width - 30,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        paddingBottom: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    moreInfoTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        marginBottom: 3
    },
    moreInfoTitleNote: {
        fontSize: 13,
        color: 'gray'
    },
    moreInfoTitleRowLeft: {
        width: width - 80
    },
    moreInfoTitleRowRight: {
        width: 30,
        alignItems: 'center'
    },
    moreInfoElementRow: {
        width: width - 50,
        paddingVertical: 10,
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    moreInfoElementRowLeft: {
        width: width - 80,
        paddingRight: 15
    },
    moreInfoElementRowRight: {
        width: 30,
        alignItems: 'center'
    },
    moreInfoElementTitle: {
        fontSize: 13,
        color: 'gray',
        marginBottom: 5
    },
    moreInfoElementText: {
        fontSize: 16
    },
    productVersionWrapper: {
        alignItems: 'center',
        width: width - 30,
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginBottom: 15
    },
    productVersionTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    productVersionTitleLeft: {
        width: width - 90,
        paddingTop: 15,
        paddingBottom: 10,
        paddingLeft: 10
    },
    productVersionTitleRight: {
        width: 60,
        alignItems: 'center'
    },
    productVersionTitle: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    productVersionRow: {
        borderColor: 'gray',
        borderBottomWidth: 0.5,
        width: width - 30,
        paddingVertical: 15,
        flexDirection: 'row',
    },
    versionRowLeft: {
        width: 80,
        paddingHorizontal: 10
    },
    versionRowCenter: {
        width: (width - 110) / 2,
        paddingLeft: 5,
        paddingRight: 15
    },
    versionRowRight: {
        width: (width - 110) / 2,
        alignItems: 'flex-end',
        paddingRight: 15
    },
    productVersionImg: {
        width: 60,
        height: 60,
        borderRadius: 5
    },
    versionName: {
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 5
    },
    versionCode: {
        color: 'gray'
    },
    versionPrice: {
        fontSize: 15
    },
    versionInventory: {
        color: 'gray'
    },
    versionCanSell: {
        color: 'gray'
    },
    statusBorder: {
        width: width - 30,
        borderRadius: 5,
        paddingVertical: 10,
        paddingLeft: 15,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 5,
        marginBottom: 15
    },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'green'
    },
    status: {
        marginLeft: 15,
        fontSize: 16
    },
    deleteBtn: {
        width: width/2.5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'tomato',
        paddingVertical: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    deleteTitle: {
        color: 'tomato',
        fontSize: 16
    },
    deleteImgWrapper: {
        alignItems: 'flex-end',
        width: width - 30
    },
    deleteImgBtn: {
        width: 80,
        paddingVertical: 3,
        borderRadius: 10,
        backgroundColor: '#FF9999',
        alignItems: 'center'
    },
    deleteImgText: {
        color: '#404040',
        fontSize: 13
    }
})