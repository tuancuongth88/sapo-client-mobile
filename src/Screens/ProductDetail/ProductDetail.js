import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, TouchableWithoutFeedback, Image, TouchableOpacity, Alert, TouchableHighlight } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { styles } from './css'
import InternetConnecNotification from '../InternetConnecNotification/InternetConnecNotification';
import { loadingScreen } from '../../Helpers/Functions'; 
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ModalDescribeProduct from '../../Components/Modal/ModalDescribeProduct/ModalDescribeProduct';
import ModalAddMoreProductVersion from '../../Components/Modal/ModalAddMoreProductVersion/ModalAddMoreProductVersion';
import ModalProductVersionDetail from '../../Components/Modal/ModalProductVersionDetail/ModalProductVersionDetail';
import ModalEditProduct from '../../Components/Modal/ModalEditProduct/ModalEditProduct';
import ModalChooseImages from '../../Components/Modal/ModalChooseImages/ModalChooseImages';
import ModalShowFullImage from '../../Components/Modal/ModalShowFullImage/ModalShowFullImage';

export default function ProductDetail(props) {

    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [loading, setLoading] = useState(true);
    const [imgArr, setImgArr] = useState([]);
    const [switchShowMoreInfo, setSwitchShowMoreInfo] = useState(true);
    const [modalEditProductVisible, setModalEditProductVisible] = useState(false);
    const [modalAddMoreProductVersionVisible, setModalAddMoreProductVersionVisible] = useState(false);
    const [modalDescribeProductVisible, setModalDescribeProductVisible] = useState(false);
    const [modalProductVersionDetailVisible, setModalProductVersionDetailVisible] = useState(false);
    const [modalChooseImagesVisible, setModalChooseImagesVisible] = useState(false);
    const [modalShowFullImageVisible, setModalShowFullImageVisible] = useState(false);
    const [switchDelImg, setSwitchDelImg] = useState(false);
    const [imgShow, setImgShow] = useState({});
    const [arrImgEmpty, setArrImgEmpty] = useState(false);
    const [listProductVersion, setListProductVersion] = useState([
        {id: 0, name: '30-đen ádasdasdsadasdasdasdsadsadfdsfsdfsdfsdfsdfsdfsdfsdf', code: 'SKU: PVN01', price: '100,000', inventory: 100, canSell: 100},
        {id: 1, name: '30-xanh', code: 'SKU: PVN02', price: '100,000', inventory: 100, canSell: 100},
        {id: 2, name: '30-nâu', code: 'SKU: PVN03', price: '100,000', inventory: 100, canSell: 100},
        {id: 3, name: '31-vàng', code: 'SKU: PVN04', price: '100,000', inventory: 100, canSell: 100},
        {id: 4, name: '31-đen', code: 'SKU: PVN05', price: '100,000', inventory: 100, canSell: 100},
        {id: 5, name: '31-xanh', code: 'SKU: PVN06', price: '100,000', inventory: 100, canSell: 100},
        {id: 6, name: '32-đen', code: 'SKU: PVN07', price: '100,000', inventory: 100, canSell: 100}
    ]);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const goBackPreviousScreen = () => {
        props.navigation.goBack()
    }

    const showOnOffMoreInfo = () => {
        setSwitchShowMoreInfo(!switchShowMoreInfo);
    }

    const openModalEditProduct = () => {
        setModalEditProductVisible(true)
    }

    const openModalAddMoreProductVersion = () => {
        setModalAddMoreProductVersionVisible(true)
    }

    const openModalDescribeProduct = () => {
        setModalDescribeProductVisible(true)
    }

    const openModalProductVersionDetail = () => {
        setModalProductVersionDetailVisible(true)
    }

    const openModalChooseImages = () => {
        setModalChooseImagesVisible(true)
    }

    const openModalShowFullImage = (img) => {
        setImgShow(img)
        setModalShowFullImageVisible(true)
    }

    const switchDeleteImg = () => {
        setSwitchDelImg(!switchDelImg)
    }

    const deleteImg = (imgKey) => {
        var arrTmp = imgArr;
        imgArr.map((value, key) => {
            if(key == imgKey) {
                arrTmp.splice(imgKey, 1)
            }
        });
        setImgArr([...arrTmp])
        if(arrTmp.length == 0) {
            setSwitchDelImg(false)
            setArrImgEmpty(true)
        }
    }

    const deleteProduct = () => {
        Alert.alert(
            'Xóa sản phẩm',
            'Bạn có chắc chắn muốn xóa sản phẩm này không?',
            [
                {text: 'Thoát', style: 'cancel'},
                {text: 'Xóa', onPress: () => {}}
            ],
            {cancelable: true}
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            {loading ? loadingScreen() :
                isInternetReachable ? 
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.headerLeft}>
                                <TouchableWithoutFeedback
                                    onPress={() => goBackPreviousScreen()}
                                >
                                    <Feather
                                        name='arrow-left'
                                        size={30}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={styles.headerTitle}>Chi tiết sản phẩm</Text>
                            </View>
                            <View style={styles.headerRight}>
                                <TouchableWithoutFeedback
                                    onPress={() => openModalEditProduct()}
                                >
                                    <MaterialCommunityIcon
                                        name='pencil-outline'
                                        size={25}
                                        color='#404040'
                                    />
                                </TouchableWithoutFeedback>
                                {/* MODAL EDIT PRODUCT */}
                                    <ModalEditProduct
                                        modalEditProductVisible={modalEditProductVisible}
                                        setModalEditProductVisible={setModalEditProductVisible}
                                    />
                                {/* MODAL EDIT PRODUCT */}
                            </View>
                        </View>

                        <ScrollView 
                            contentContainerStyle={styles.body}
                            showsVerticalScrollIndicator={false}
                        >
                            {imgArr.length > 0 ?
                                <View style={styles.deleteImgWrapper}>
                                    <TouchableOpacity 
                                        style={styles.deleteImgBtn}
                                        onPress={() => switchDeleteImg()}
                                    >
                                        <Text style={styles.deleteImgText}>Xóa ảnh</Text>
                                    </TouchableOpacity>
                                </View> : null
                            }
                            <View style={styles.productImageWrapper}>
                                <TouchableWithoutFeedback onPress={() => openModalChooseImages()}>
                                    <View style={styles.addImage}>
                                        <MaterialIcon
                                            name='add-photo-alternate'
                                            size={30}
                                            color='gray'
                                        />
                                    </View>
                                </TouchableWithoutFeedback>
                                {/* MODAL CHOOSE IMAGE */}
                                    <ModalChooseImages
                                        modalChooseImagesVisible={modalChooseImagesVisible}
                                        setModalChooseImagesVisible={setModalChooseImagesVisible}
                                        imgArr={imgArr}
                                        setImgArr={setImgArr}
                                    />
                                {/* MODAL CHOOSE IMAGE */}
                                <ScrollView
                                    contentContainerStyle={styles.productImageScroll}
                                    showsHorizontalScrollIndicator={false}
                                    horizontal={true}
                                >
                                    {imgArr.map((value, key) => { 
                                        return(
                                            <View 
                                                style={styles.productImageElement}
                                                key={key}
                                            >
                                                <TouchableWithoutFeedback onPress={() => openModalShowFullImage(value)}>
                                                    <Image
                                                        style={styles.productImage}
                                                        source={{uri: value.path}}
                                                    />
                                                </TouchableWithoutFeedback>
                                                {switchDelImg ?
                                                    <TouchableWithoutFeedback onPress={() => deleteImg(key)}>
                                                        <View style={styles.deleteImg}>
                                                            <View style={styles.redline}></View>
                                                        </View>
                                                    </TouchableWithoutFeedback> : null
                                                }
                                            </View> 
                                        )
                                    })}
                                </ScrollView>
                                {/* MODAL SHOW FULL IMAGE */}
                                    <ModalShowFullImage
                                        modalShowFullImageVisible={modalShowFullImageVisible}
                                        setModalShowFullImageVisible={setModalShowFullImageVisible}                                           
                                        imgArr={imgArr}
                                        setImgArr={setImgArr}
                                        imgShow={imgShow}
                                    />
                                {/* MODAL SHOW FULL IMAGE */}
                            </View>

                            <View style={styles.productNameWrapper}>
                                <Text style={styles.productName}>Quần sks</Text>
                            </View>

                            <View style={styles.moreInfoWrapper}>
                                <TouchableWithoutFeedback underlayColor='lavender'>
                                    <View style={styles.moreInfoTitleRow}>
                                        <View style={styles.moreInfoTitleRowLeft}>
                                            <Text style={styles.moreInfoTitle}>Thông tin thêm</Text>
                                            <Text style={styles.moreInfoTitleNote}>Mô tả, phân loại sản phẩm</Text>
                                        </View>
                                        <View style={styles.moreInfoTitleRowRight}>
                                            <Feather
                                                name={switchShowMoreInfo ? 'chevron-up' : 'chevron-down'}
                                                size={20}
                                                color='#808080'
                                            />
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                                {switchShowMoreInfo ? 
                                    <View>
                                        <View style={styles.moreInfoElementRow}>
                                            <View style={styles.moreInfoElementRowLeft}>
                                                <Text style={styles.moreInfoElementTitle}>Loại sản phẩm</Text>
                                                <Text 
                                                    style={styles.moreInfoElementText}
                                                    numberOfLines={1}
                                                >
                                                    Quần
                                                </Text>
                                            </View>
                                            <View style={styles.moreInfoElementRowRight}>
                                                
                                            </View>
                                        </View>
                                        <View style={styles.moreInfoElementRow}>
                                            <View style={styles.moreInfoElementRowLeft}>
                                                <Text style={styles.moreInfoElementTitle}>Nhãn hiệu</Text>
                                                <Text 
                                                    style={styles.moreInfoElementText}
                                                    numberOfLines={1}
                                                >
                                                    --
                                                </Text>
                                            </View>
                                            <View style={styles.moreInfoElementRowRight}>
                                                
                                            </View>
                                        </View>
                                        <TouchableHighlight
                                            onPress={() => openModalDescribeProduct()}
                                            underlayColor='lavender'
                                        >
                                            <View style={styles.moreInfoElementRow}>
                                                <View style={styles.moreInfoElementRowLeft}>
                                                    <Text style={styles.moreInfoElementTitle}>Mô tả</Text>
                                                    <Text 
                                                        style={styles.moreInfoElementText}
                                                        numberOfLines={1}
                                                    >
                                                        --
                                                    </Text>
                                                </View>
                                                <View style={styles.moreInfoElementRowRight}>
                                                    <Feather
                                                        name='chevron-right'
                                                        size={20}
                                                        color='#808080'
                                                    />
                                                </View>
                                                {/* MODAL DESCRIBE PRODUCT */}
                                                    <ModalDescribeProduct
                                                        modalDescribeProductVisible={modalDescribeProductVisible}
                                                        setModalDescribeProductVisible={setModalDescribeProductVisible}
                                                    />
                                                {/* MODAL DESCRIBE PRODUCT */}
                                            </View>
                                        </TouchableHighlight>
                                    </View> : null
                                }
                            </View>

                            <View style={styles.productVersionWrapper}>
                                <View style={styles.productVersionTitleWrapper}>
                                    <View style={styles.productVersionTitleLeft}>
                                        <Text style={styles.productVersionTitle}>Phiên bản sản phẩm</Text>
                                        {/* MODAL PRODUCT VERSION */}
                                            <ModalAddMoreProductVersion
                                                modalAddMoreProductVersionVisible={modalAddMoreProductVersionVisible}
                                                setModalAddMoreProductVersionVisible={setModalAddMoreProductVersionVisible}
                                            />
                                        {/* MODAL PRODUCT VERSION */}
                                    </View>
                                    <View style={styles.productVersionTitleRight}>
                                        <TouchableOpacity onPress={() => openModalAddMoreProductVersion()}>
                                            <Feather
                                                name='plus-circle'
                                                size={22}
                                                color='#1C3FAA'
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {listProductVersion.map((version, key) => {
                                    return(
                                        <TouchableHighlight
                                            onPress={() => openModalProductVersionDetail()}
                                            underlayColor='lavender'
                                            key={key}
                                        >                                    
                                            <View style={styles.productVersionRow}>
                                                <View style={styles.versionRowLeft}>
                                                    <Image
                                                        style={styles.productVersionImg}
                                                        source={require('../../Assets/no_image.png')}
                                                    />
                                                </View>
                                                <View style={styles.versionRowCenter}>
                                                    <Text 
                                                        style={styles.versionName}
                                                        numberOfLines={1}
                                                    >
                                                        {version.name}
                                                    </Text>
                                                    <Text 
                                                        style={styles.versionCode}
                                                        numberOfLines={1}
                                                    >
                                                        {version.code}
                                                    </Text>
                                                </View>
                                                <View style={styles.versionRowRight}>
                                                    <Text 
                                                        style={styles.versionPrice}
                                                        numberOfLines={1}
                                                    >
                                                        {version.price}
                                                    </Text>
                                                    <Text 
                                                        style={styles.versionInventory}
                                                        numberOfLines={1}
                                                    >
                                                        Tồn kho: {version.inventory}
                                                    </Text>
                                                    <Text 
                                                        style={styles.versionCanSell}
                                                        numberOfLines={1}
                                                    >
                                                        Có thể bán: {version.canSell}
                                                    </Text>
                                                </View>
                                            </View>
                                        </TouchableHighlight>
                                    )
                                })}
                                {/* MODAL PRODUCT VERSION DETAIL */}
                                    <ModalProductVersionDetail
                                        modalProductVersionDetailVisible={modalProductVersionDetailVisible}
                                        setModalProductVersionDetailVisible={setModalProductVersionDetailVisible}
                                        imgArr={imgArr}
                                        setImgArr={setImgArr}
                                        arrImgEmpty={arrImgEmpty}
                                    />
                                {/* MODAL PRODUCT VERSION DETAIL */}
                            </View>

                            <View style={styles.statusBorder}>
                                <View style={styles.circle}></View>
                                <Text style={styles.status}>Đang giao dịch</Text>
                            </View>
                            
                            <TouchableHighlight                              
                                onPress={() => deleteProduct()}
                                underlayColor='gray'
                            >
                                <View style={styles.deleteBtn}>
                                    <Text style={styles.deleteTitle}>Xóa sản phẩm</Text>
                                </View>
                            </TouchableHighlight>
                        </ScrollView>
                    </View>
                        :
                    <InternetConnecNotification/>
            }
        </SafeAreaView>
    );
};

