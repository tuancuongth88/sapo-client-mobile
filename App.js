/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import { View, StatusBar, Text, StyleSheet, ActivityIndicator } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import OfflineNotice from './src/Components/OfflineNotice/OfflineNotice';
import RootNavigation from './src/Navigators/RootNavigation';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

export default function App() {
    const slides = [
        {
          key: 'one',
          title: 'Intro 1',
          text: 'Description.\nSay something cool',
        //   image: require('./assets/1.jpg'),
          backgroundColor: '#59b2ab',
        },
        {
          key: 'two',
          title: 'Intro 2',
          text: 'Other cool stuff',
        //   image: require('./assets/2.jpg'),
          backgroundColor: '#febe29',
        },
        {
          key: 'three',
          title: 'Last Intro',
          text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        //   image: require('./assets/3.jpg'),
          backgroundColor: '#22bcb5',
        }
    ];
  
    const styles = StyleSheet.create({
        buttonCircle: {
            width: 40,
            height: 40,
            backgroundColor: 'rgba(0, 0, 0, .2)',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
        },
        skip: {
            width: 40,
            height: 30,
            justifyContent: 'center',
            alignItems: 'center'
        },
        skipText: {
            color: 'rgba(255, 255, 255, .9)',
            fontSize: 13,
            fontWeight: 'bold'
        }
    });
    
    const [isInternetReachable, setIsInternetReachable] = useState(false);
    const [showRealApp, setShowRealApp] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        NetInfo.addEventListener(state => {
            setIsInternetReachable(state.isInternetReachable);
        });
        AsyncStorage.getItem('setShowedRealApp').then((value) =>
            { 
                if(value == 'true') {
                    setShowRealApp(true);
                }
                setLoading(false);
            }
        )
    }, []);

    const renderItem = ({ item }) => {
        return (
          <View>
            <Text>{item.title}</Text>
            {/* <Image source={item.image} /> */}
            <Text>{item.text}</Text>
          </View>
        );
    }

    const onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        setShowRealApp(true);
        AsyncStorage.setItem('setShowedRealApp', 'true');
    }

    const onSkip = () => {
        setShowRealApp(true);
        AsyncStorage.setItem('setShowedRealApp', 'true');
    }

    const renderNextButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Icon
              name="ios-arrow-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
            />
          </View>
        );
    };

    const renderDoneButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Icon
              name="ios-checkmark"
              color="rgba(255, 255, 255, .9)"
              size={24}
            />
          </View>
        );
    };

    const renderSkipButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Text style={styles.skipText}>Skip</Text>
            </View>
        )
    }

    return (
        <>
        {loading ? <ActivityIndicator size="large" /> :
            showRealApp ? 
                <View style={{ flex: 1 }}>
                    <StatusBar barStyle="dark-content" backgroundColor='#fff'/>
                    {isInternetReachable == false ? 
                        <View style={{height: 40}}>
                            <OfflineNotice/>
                        </View> : null
                    }
                    <RootNavigation/>
                </View>
                    :
                <AppIntroSlider 
                    renderItem={renderItem} 
                    data={slides} 
                    onDone={onDone}
                    onSkip={onSkip}
                    renderDoneButton={renderDoneButton}
                    renderNextButton={renderNextButton}
                    renderSkipButton={renderSkipButton}
                    showSkipButton={true}
                />
            }  
        </>
    );
};
